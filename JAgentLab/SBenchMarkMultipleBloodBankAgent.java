
/**
 * SBenchMarkMultipleBloodBankAgent.java 
	*  col's implementation of multiple agent minimal result for blood bank problem. 
 * @author Col Clark (your name) 
 * @version 18/5/14 
 */
import java.util.LinkedList;
import src.Model.*; 
import src.Model.BloodBank.*; 

public class SBenchMarkMultipleBloodBankAgent extends BloodBankAgent
{
    // instance variables - replace the example below with your own
    static private LinkedList[] mailbox;
    private BoardComponentList knownBloodBanks = null;
    private BoardComponentList knownDonors = null;
    private BoardComponentList wayPoints = null;
    private int flag;
    private boolean isSet= false;

    /**
     * Constructor for objects of class S654456BloodBankAgent
     */
    public SBenchMarkMultipleBloodBankAgent()  
    { 
          super(); 
          knownBloodBanks = new BoardComponentList(); 
          knownDonors = new  BoardComponentList();
          wayPoints= new  BoardComponentList();
          flag = numberOfAgents();
          mailbox = null;
          mailbox = new LinkedList[numberOfAgents()];
          for ( int i=0; i< numberOfAgents(); i++ ) 
          {
    	     mailbox[i] = new LinkedList<Message>();
          }
    }

    /* 
    * constructs this from the specified parameter 
    * @param bC the BoardAgent object for the super class. 
    */ 
     public SBenchMarkMultipleBloodBankAgent( BoardComponent bC ) 
     { 
          super( bC );
          knownBloodBanks = new BoardComponentList(); 
          knownDonors = new  BoardComponentList();
          wayPoints= new  BoardComponentList(); 
          mailbox = null;
          mailbox = new LinkedList[numberOfAgents()];
          for ( int i=0; i< numberOfAgents(); i++ ) 
          {
    	     mailbox[i] = new LinkedList<Message>();
          }
          
     } 
    
    /* 
    * implements the abstract method from the super class. 
    * This is a method that specifies this agent's behaviour 
    *  in the Blood Bank Problem.
    */ 
    public void agentStrategy() 
    { 
        //System.out.println("My Id "+myIdNumber+" Known BloodBanks " +knownBloodBanks.size()+" Known Donors "+knownDonors.size()+" My Blood Load "+ myBloodLoad);
        //System.out.print(numberOfAgents() + " ");
        //System.out.println(myIdNumber); 
        setAgentSpeedTo( 1.0); 
        if(!isSet)
        {            
            isSet=true;
        }
        Message message = new Message(myIdNumber, visibleDonors, visibleBloodBanks);
        for(int i=0; i<numberOfAgents();i++)
        {
             if(i != myIdNumber)
             {
              mailbox[i].addLast(message);
             }
        }
       if(flag >0)
       {
           BloodBank bloodBank;
           Donor donor;
            for ( int i=0; i< knownBloodBanks.size(); i++ ) 
            {      
                bloodBank = (BloodBank) knownBloodBanks.retrieve(i);    
                knownBloodBanks.remove(bloodBank);
            } // end for i
            for ( int i=0; i< knownDonors.size(); i++ ) 
            {      
                donor = (Donor) knownDonors.retrieve(i);    
                knownDonors.remove(donor);
            } // end for i
       }
       if(mailbox[myIdNumber].size()>0)
       {
          for(int i=0; i<mailbox[myIdNumber].size();i++)
          {
             Message temp = (Message) mailbox[myIdNumber].get(0);
             synchKnownDonors(temp.getDonors());
             synchKnownBloodBanks(temp.getBloodBanks());
             mailbox[myIdNumber].remove(0);
          }
        } 
        updateKnownBloodBanks();
        updateKnownDonors(); 
        updateKnownWayPoints();
        flag--;
        
        if ((myTimeRemaining < distanceToClosestBloodBank()+2) && (myBloodLoad > 0) ) 
        { // not much time left, so make a run for the closest blood bank
           
                setDirectionToClosestKnownBloodBank();
        }else
        {
        
            if(myBloodLoad == MY_CAPACITY && knownBloodBanks.size() >0)
            {
               setDirectionToClosestKnownBloodBank();
            }
            
            if(myBloodLoad < MY_CAPACITY && knownBloodBanks.size() > 0)
            {
                if(myBloodLoad > 0 && knownDonors.size() > 0)
                {
                   if((distanceToClosestDonor() > distanceToClosestBloodBank()))
                   {
                      setDirectionToClosestKnownBloodBank();
                   }
                }else
                {
                  setDirectionToClosestKnownDonor();
                }
            }
            
            if(myBloodLoad == 0 && knownDonors.size() > 0)
            {
              setDirectionToClosestKnownDonor();
            }
        }//end if timeRemaining
                
       
            
    } // end agent strategy 
    
    
    private void synchKnownBloodBanks(BoardComponentList b) 
    { 
        BloodBank bloodBank, oldBloodBank; 
        int index;
        for ( int i=0; i< b.size(); i++ ) 
        { 
            bloodBank = (BloodBank) b.retrieve(i); 
            index = knownBloodBanks.indexOf( bloodBank ); 
                if ( index > -1 ) 
                { 
                    oldBloodBank = (BloodBank) knownBloodBanks.retrieve(index); 
                    if ( oldBloodBank.getBloodLoad() < bloodBank.getBloodLoad() )   
                    {
                       knownBloodBanks.replace( index, bloodBank );
                    }else
                    {
                      //do nothing
                    }
                }
                else 
                    knownBloodBanks.add( bloodBank ); 
        } // end for i 
        for ( int i=0; i< knownBloodBanks.size(); i++ ) 
        {      
            bloodBank = (BloodBank) knownBloodBanks.retrieve(i);    
            if(bloodBank.getSpareCapacity()==0)
            {
               knownBloodBanks.remove(knownBloodBanks.retrieve(i));
            }
        } // end for i
    } // end synch known blood banks 
    
    
    private void synchKnownDonors(BoardComponentList b) 
    { 
        Donor donor, oldDonor ;
        int index;            
        for ( int i=0; i< b.size(); i++ ) 
        { 
            donor = (Donor) b.retrieve(i); 
            index = knownDonors.indexOf( donor ); 
            if ( index > -1 ) 
            { 
                oldDonor = (Donor) knownDonors.retrieve(index); 
                if ( oldDonor.getBloodLoad() > donor.getBloodLoad() )   
                {
                    knownDonors.replace( index, donor );
                }else
                {
                  //do nothing
                }
            }
            else 
                knownDonors.add( donor); 
        } // end for i 
        for ( int i=0; i< knownDonors.size(); i++ ) 
        {      
            donor = (Donor) knownDonors.retrieve(i);    
                if(donor.getBloodLoad()<=1)
                {
                   knownDonors.remove(donor);
                }
        } // end for i 
    } // end synch known donors 
    
    
    private void updateKnownBloodBanks() 
    { 
        BloodBank bloodBank, oldBloodBank; 
        int index; 
        for ( int i=0; i< visibleBloodBanks.size(); i++ ) 
        { 
            bloodBank = (BloodBank) visibleBloodBanks.retrieve(i); 
            index = knownBloodBanks.indexOf( bloodBank ); 
            if ( index > -1 ) 
            { 
                oldBloodBank = (BloodBank) knownBloodBanks.retrieve(index); 
                if ( oldBloodBank.getBloodLoad() < bloodBank.getBloodLoad() )
                {
                    knownBloodBanks.replace( index, bloodBank );
                }else
                {
                  //do nothing
                }
            }
            else
            {
                knownBloodBanks.add( bloodBank );
            }
        }
        for ( int i=0; i< knownBloodBanks.size(); i++ ) 
        {      
            bloodBank = (BloodBank) knownBloodBanks.retrieve(i);    
            if(bloodBank.getSpareCapacity()==0)
            {
               knownBloodBanks.remove(knownBloodBanks.retrieve(i));
            }
        } // end for i  
    } // end update known blood banks 
        
    
    private void updateKnownDonors() 
    { 
        Donor donor, oldDonor ;
        int index; 
        for ( int i=0; i< visibleDonors.size(); i++ ) 
        { 
            donor = (Donor) visibleDonors.retrieve(i); 
            index = knownDonors.indexOf( donor ); 
            if ( index > -1 ) 
            { 
                oldDonor = (Donor) knownDonors.retrieve(index); 
                if ( oldDonor.getBloodLoad() > donor.getBloodLoad() ) 
                {
                    knownDonors.replace( index, donor );  
                }else
                {
                  //do nothing
                }
            }
            else 
            {
                knownDonors.add( donor); 
            }   
        } // end for i 
        for ( int i=0; i< knownDonors.size(); i++ ) 
        {      
            donor = (Donor) knownDonors.retrieve(i);    
                if(donor.getBloodLoad()<=1)
                {
                   knownDonors.remove(donor);
                }
        } // end for i 
    } // end update known donors 
    
    
    private void updateKnownWayPoints() 
    { 
        BoardComponent wP; 
            int index; 
            for ( int i=0; i< wayPoints.size(); i++ ) 
            { 
                wP = wayPoints.retrieve(i); 
                if ( wP.distance(this) <1)
                { 
                    boolean done = wayPoints.remove(wP);
                }
            }
            
    }
     
    
    private BloodBank getClosestKnownBloodBank() 
    { 
       double minDis = 10000.0; 
       double dis;
       BloodBank closestBloodBank = null;
       BloodBank bC = null; 
       for ( int i=0; i< knownBloodBanks.size(); i++ ) 
       { 
            bC = (BloodBank) knownBloodBanks.retrieve(i);
            dis = this.distance( bC ); 
            if ( dis < minDis ) 
            { 
                 minDis = dis; 
                        closestBloodBank = bC; 
            }
        }
      return closestBloodBank;
    } // end get closest known blood bank 
    
    
    
    private Donor getClosestKnownDonor() 
    { 
        double minDis = 10000.0; 
        double dis;
        Donor closestDonor = null;
        Donor bC = null; 
        for ( int i=0; i< knownDonors.size(); i++ ) 
        { 
            bC = (Donor) knownDonors.retrieve(i);
            dis = this.distance( bC ); 
            if ( dis < minDis ) 
            { 
                 minDis = dis; 
                        closestDonor = bC; 
            }
        }
      return closestDonor;     
    } // end get closest donor 
    
    private BoardComponent getClosestKnownWayPoint() 
    { 
      double minDis = 10000.0; 
      double dis;
        BoardComponent closestWayPoint = null;
        BoardComponent bC = null; 
        for ( int i=0; i< wayPoints.size(); i++ ) 
        { 
            bC =  wayPoints.retrieve(i);
                dis = this.distance( bC ); 
                if ( dis < minDis ) 
                { 
                     minDis = dis; 
                            closestWayPoint = bC; 
                }
        }
            return closestWayPoint;     
    } // end get closest donor 

    
    private double distanceToClosestBloodBank() 
    { 
        double distance = 0.0; //Double.MAX_VALUE; 
            if ( knownBloodBanks.size() > 0 ) 
            { 
                BloodBank bloodBank = getClosestKnownBloodBank(); 
                    distance = this.distance( bloodBank ); 
                    //System.out.println("s11-dTcBB-dis= " + ((int) distance) + ", BB is " + bloodBank.toString() ); 
            } 
            return distance; 
    } // end distance to closest blood bank 
    
    private double distanceToClosestDonor() 
    { 
        double distance = 0.0; //Double.MAX_VALUE; 
            //System.out.println("S11-dTCBB-knownBloodBank size= " + knownBloodBanks.size() );
            if ( knownDonors.size() > 0 ) 
            { 
                Donor donor = getClosestKnownDonor(); 
                    distance = this.distance( donor ); 
                    //System.out.println("s11-dTcBB-dis= " + ((int) distance) + ", BB is " + bloodBank.toString() ); 
            } 
            return distance; 
    } // end distance to closest blood bank 
        
    private void setDirection( double angle ) 
    {
        if ( angle > 0 ) 
        { 
            while ( angle > BoardComponent.HALF_TURN_INC )
                {    
                    turnRight();
                        angle-= BoardComponent.TURN_INC; 
                }
        }
        else // must be negative
        { 
            while ( angle < (-BoardComponent.HALF_TURN_INC) )
                { 
                    turnLeft(); 
                        angle += BoardComponent.TURN_INC;
                }
        }
    } // end set direction

    private void setDirectionToClosestKnownBloodBank() 
    { 
        BloodBank bloodBank = getClosestKnownBloodBank(); 
            if ( bloodBank != null ) 
            { 
                double angle = this.getAngleTo( bloodBank ); 
                    setDirection( angle ); 
            }
    } // end set direction to closest blood bank 
    
    private void setDirectionToClosestKnownDonor() 
    { 
        Donor donor = getClosestKnownDonor(); 
            if(donor != null && donor.getBloodLoad() >1)
            {
                double angle = getAngleTo( donor );  
                setDirection ( angle ); 
            }
    } // end set direction to closest blood bank 
    
    private void setDirectionToWayPoint() 
    {
        if ( wayPoints.size() > 0 ) 
        {
            BoardComponent wP = getClosestKnownWayPoint(); 
            if(wP != null)
            {
                double angle = getAngleTo(wP);  
                setDirection ( angle ); 
            }
        }
        else 
        { 
          
        } 
    } // end set direction to wayPoint 
    

       private class Message 
       {	 
    		    // instance variables 
            private int senderId;   
            private BoardComponentList donors; 
            private BoardComponentList bloodBanks; 
            private BoardComponent requestAssistanceAt; 
            private Intention iIntendToAssistAt; 
    		
            public Message( int senderId, BoardComponentList d, BoardComponentList b ) 
            { 
                this.senderId = senderId;  
    		        donors = d; 
    		        bloodBanks = b; 
            } // end constructor 
    	
            public void setIntentions( Intention intention ) 
            { 
                iIntendToAssistAt = intention; 
            } 
    
            public void setRequest( BoardComponent bC ) 
            { 
                requestAssistanceAt = bC; 
            } 
    
            public BoardComponentList getDonors() 
            { 
                return donors; 
            } 
    
            public BoardComponentList getBloodBanks() 
            { 
                return bloodBanks; 
            } 
            
    		public Intention getIntentions() 
    	    { 
    	        return iIntendToAssistAt; 
    	    } 
    	  
    	    public BoardComponent getRequest() 
    	    { 
    	        return requestAssistanceAt; 
    	    } 
    	
    	    public int getSenderId() 
    	    { 
    	        return senderId; 
    	    } 		
        } // end Message class 
				
		private class Intention
		{
			  // instance variables 
	      private BoardComponent target; 
	      private double distanceTo; 
	
	      public Intention( BoardComponent bC, double distance ) 
	      { 
	          target = bC; 
			      distanceTo = distance; 
	      } // end constructor 
		

          public BoardComponent getTarget() 
	      { 
	          return target; 
	      } 
		
	      public double getDistancetoTarget() 
	      { 
	        return distanceTo;
	      } 

		} // end Intention class
} // end S123456BloodBankAgent class
