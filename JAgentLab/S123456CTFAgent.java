/*
* S123456CTFAgent.java extends CTFAgent class and 
*  implements the abstract method agentStrategy().
* @author Phil Sheridan
* @version 29/4/14
*/ 
import src.Model.CTF.*; 
import src.Model.*; 

public class S123456CTFAgent extends CTFAgent
{
 
	 // instance variables 
		private BoardComponentList knownCTFs;
		private BoardComponentList knownDonors; 
	/* 
	* constructs a default object 
	*/ 
	public S123456CTFAgent() 	
	{ 
	  super(); 
	  knownCTFs = new BoardComponentList();
		knownDonors = new BoardComponentList(); 
	}

/* 
* constructs this from the specified parameter 
* @param bC the BoardAgent object for the super class. 
*/ 
 public S123456CTFAgent( BoardComponent bC ) 
	{ 
	    super( bC ); 
	} 
	
	/* 
	* implements the abstract method from the super class. 
	* This is a method that specifies this agent's behaviour 
	*  in the Blood Bank Problem.
	*/ 
	public void agentStrategy() 
	{ 
	    setAgentSpeedTo( 1.0);  
		} // end agent strategy 
}// end class
