package src.View; 

/* 
* DrawableTagController.java 
* @author Phil Sheridan 
* @version 6/2/15 
*/ 

import java.util.LinkedList; 
import java.util.Iterator; 
import java.util.Map; 
import java.util.HashMap; 
import src.Model.*; 
import src.Model.Tag.*; 
 	
public class DrawableTagController extends TagController 
{  
	protected BoardComponentList baseCity; 
	private DrawableBoardComponentList dBCL;			
	private  ControllerCanvas canvas; 	
    private BoardComponentList fieldBorder; 
	
	/* 
	 ** constructs this. 
	 **/ 
	public DrawableTagController( int totalTime, String[] filename, boolean randomise ) 
	{ 
	    super( totalTime, filename, randomise );  
		dBCL = new DrawableBoardComponentList( agentBCList[0] );
		fieldBorder = new BoardComponentList(); 
		BoardComponent bC; 
		// now do the borders 
		bC= new BoardComponent( ((MAX_X/2)+1), (MAX_Y/2), 2, MAX_Y, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now add right border 
		//bC= new BoardComponent( (MAX_X-1), (MAX_Y/2), 2, MAX_Y, true ); 
		//bC.setColour( "red"); 
		//fieldBorder.add( bC ); 
		// now add upper boreder 
		bC= new BoardComponent( (MAX_X-(MAX_X/4)), 2, (MAX_X/2), 2, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now add the lower border 
		bC= new BoardComponent( (MAX_X-(MAX_X/4)), (MAX_Y-2), (MAX_X/2), 2, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now do the same for the blue 
		// now do mid field marker 
		bC= new BoardComponent( (MAX_X/2-1), (MAX_Y/2), 2, MAX_Y, true ); 
		//bC.setColour( "blue"); 
		//fieldBorder.add( bC ); 
		// now add left border 
		bC= new BoardComponent( 2, (MAX_Y/2), 2, MAX_Y, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
		// now add upper boreder 
		bC= new BoardComponent( (MAX_X/4), 2, (MAX_X/2), 2, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
		// now add the lower border 
		bC= new BoardComponent( (MAX_X/4), (MAX_Y-2), (MAX_X/2), 2, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
			   
	} // end constructor 

	/* 
	 ** runs this. 
	 */ 
	public void runDrawableController() 
	{
        System.out.println("DC-rC-start");  
	    canvas = ControllerCanvas.getCanvas();  	       
		for ( int repetition=0; repetition< numberOfRepetitions; repetition++ ) 
		{ 
		    //startNextRepetition(); 
			setShiftFactors( repetition );  
			sizeOfTeam = agentBCList[repetition].size(); 
			for ( int round = 0; round < numberOfRounds; round++ ) 
			{  
			    for ( int game=0; game< numberOfGames; game++ ) 
		        { 
				    canvas.updateCityLabel(); 
					runNextDrawableGame( repetition,  round, game ); 
					canvas.updateCityLabel(); 
					//System.out.println("DCTFC-rDC-after runNextRGame");  
			        canvas.wait( 2000 ); 
				} // end for game   
			} // end for round 
		} // end for repitition  
		setTournamentResultsLabel(); 
		canvas.updateCityLabel(); 
		resultsToFile();       
		//printAllScores(); 
	} // end run drawable controller 
	
	/* 
	 ** runs next drawable game. 
	 **/	
	public void runNextDrawableGame( int repNo, int round, int game ) 
	{ 
	    //System.out.println("DCTFC-rNDG-start"); 
	    int[] contestantId = new int[numberOfTeams];
	    for ( int t=0; t< numberOfTeams; t++ ) 
	    {	        
			contestantId[t] = schedule[round][game][t]; 
			//System.out.println("DTC-rNG-contestantId= " + contestantId[t] ); 
			//System.out.println("t=" + t + ", contestantId= " + contestantId[t] );
			player[t] = teamList[contestantId[t]]; 
			//for ( int j=0; j< player[t].length; j++ ) 
			//  System.out.println("CTFC-rNG-player " + j + " is " + player[t][j].toString());
		} // end for t 
		//System.out.println("CTFC-rNG-before initialise"); 
		initialiseGame( repNo, round, game, contestantId ); 
        timeRemaining = totalTime; 
        setGameLabel( repNo, round, game, contestantId ); 
		canvas.updateCityLabel(); ; 
		boolean finished = false; 
		while ( !finished ) 
		{	
		    playGame(); 
			dBCL.makeInvisible();	
			updateDBCL( round );
			dBCL.makeVisible(); 
			finished = isGameFinished();  
		} // end while	
		//System.out.println("CTFC-rNG-finished while");
		updateCurrentContestants( repNo, round, game, contestantId ); 
		canvas.updateRoundScoreBoard();    
		canvas.updateCityLabel(); 
	    for ( int blink=0; blink< 9; blink++ ) 
		{ 
		    canvas.toggleCityLabel(blink); 
	        canvas.wait( 1000 ); 
	       } // end for blink
       } // end run next drawable game 
		
	private void updateDBCL( int round ) 
	{
	    BoardComponent bC, smallBC; 
	    dBCL.clear(); 
		//for ( int i=0; i< fieldBorder.size(); i++ ) 
		 //   dBCL.add( fieldBorder.retrieve(i) ); 
		
		// now add the agents 
		double[] xY; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{
			// now put the agents in the list
			for ( int i=0; i< sizeOfTeam; i++ ) 
			{   
			    bC = player[t][i]; 
				dBCL.add( new BoardComponent(bC) );  
			} // end for i 			
		} // end for t 
	} // end update dbcl 
	
    static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // keep false when testing, otherwise randomises red and blue contestants
	    int gameDuration = 400; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "TagContestantNames.txt"; 
	    filename[1] = "TagCityNames.txt"; 
		DrawableTagController dC = new DrawableTagController( gameDuration, filename, randomise );  
	    ControllerCanvas.createController( dC ); 
	    dC.runDrawableController(); 
	} // end main 
	
} // end DrawableTagController class 
