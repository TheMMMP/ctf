package src.View; 

/**
 * ControllerCanvas is a class to allow for simple 
	*  graphical drawing on a canvas.
  *It demonstrates simple agent movement.
  * @author Phil Sheridan 
	* @version 12/5/14
*/ 

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import src.Model.*; 
import java.util.*;


public class ControllerCanvas //implements ActionListener
{
   
    private static ControllerCanvas canvasSingleton = null;
	
	/* 
	* creates the canvas on which to display the animated tournament. 
	* @param controller, the Controller object to be displayed
	*/ 
	public static void createController( Controller controller ) 
	{ 
		if(canvasSingleton == null) 
		{
		    String title = controller.getTitle();  
				int width = Controller.MAX_X + (2*Controller.MAX_Y); //FIELD_SIZE;  
		    int height = 3*Controller.MAX_Y; //FIELD_SIZE; 
		    Color bColour = Color.white;
        canvasSingleton = new ControllerCanvas( controller,title, width, height, bColour );
		} // end if                                
	}  // end create controller 

    /**
     * Factory method to get the canvas singleton object.
     */
    public static ControllerCanvas getCanvas()
    {
        canvasSingleton.setVisible(true);
        return canvasSingleton;
	} // end get canvas 

    //  ----- instance part -----

    private JFrame frame;
    private CanvasPane canvas;
    private Graphics2D graphic;
    private Color backgroundColor;
    private Image canvasImage;
    private List<Object> objects;
    private HashMap<Object, ShapeDescription> shapes;	
    private Controller controller; 

   private String[] toggle; 
	//private JLabel cityLabel; 
	private JLabel repetitionLabel; 
	private JLabel roundLabel; 
	private JLabel gameLabel; 
	private JLabel blueContestantLabel; 
  private JLabel redContestantLabel; 
  private JLabel vsLabel; 
  private JLabel resultLabel; 
  private JLabel resultTypeLabel; 
  private JLabel winnerLabel; 
 //private String gameResult; 
	
	//private JTextField blueTF; 
	//private JTextField vsTF; 
	
	//private JLabel winLabel; 

	//private JLabel roundLabel; 
	private JLabel[] rankLabel; 
	private JLabel[] nameLabel; 
	private Map<String, Color>  colourMap; 
	private JTextField[] firstCompTF; 
	private JTextField[] secondCompTF; 
	private JTextField[] thirdCompTF; 
	private JLabel roundL; 
	private JLabel scoreL; 
	private JLabel hallOfFameL; 
	private JTextArea hallOfFameTA; 
	private JLabel individualGameL; 
	private JTextArea individualGameTA; 
	
    /**
     * Create a Canvas.
     * @param controllerController to run
	 *  @param title    title to appear in Canvas Frame
     * @param width    the desired width for the canvas
     * @param height   the desired height for the canvas
     * @param bgColor the desired background color of the canvas
     */
    private ControllerCanvas( Controller controller, String title, int width, int height, Color bgColor)
    {
	    this.controller = controller; 
        frame = new JFrame(); 
		canvas = new CanvasPane();
        Container pane = frame.getContentPane();
		toggle = new String[2]; 
		toggle[0] = "   "; 
		toggle[1] = "    "; 
		JPanel participantPanel = new JPanel(); 
		//participantPanel.setLayout( new BorderLayout() ); 
		participantPanel.setLayout(new GridLayout( 3, 3 )); 
		String blank = "                "; 
		// now do the repetition label
		repetitionLabel = new JLabel( blank, SwingConstants.CENTER);
        repetitionLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( repetitionLabel  ); 
	    // now do the round label 
		roundLabel = new JLabel( blank, SwingConstants.CENTER);
        roundLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( roundLabel  ); 
	    // now do the game label 
		gameLabel = new JLabel( blank, SwingConstants.CENTER);
        gameLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( gameLabel  ); 
		blueContestantLabel = new JLabel( blank, SwingConstants.RIGHT);  
	    blueContestantLabel.setFont(new Font("Arial", Font.BOLD, 24));
		blueContestantLabel.setForeground(Color.BLUE); 	 
	    participantPanel.add( blueContestantLabel  ); 
	    // now do vs label 
		String vsName = " vS "; 
		vsLabel = new JLabel( vsName, SwingConstants.CENTER);
        vsLabel.setFont(new Font("Arial", Font.BOLD, 24));
		vsLabel.setForeground(Color.BLACK); 
		    
	    participantPanel.add( vsLabel  ); 
		redContestantLabel = new JLabel( blank, SwingConstants.LEFT);
        redContestantLabel.setFont(new Font("Arial", Font.BOLD, 24));
	    redContestantLabel.setForeground(Color.RED); 
	    participantPanel.add( redContestantLabel  ); 
	    // now do the result label 
		resultLabel = new JLabel( blank, SwingConstants.CENTER);
        resultLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( resultLabel  ); 
	    // now do the result type label 
		resultTypeLabel = new JLabel( blank, SwingConstants.CENTER);
        resultTypeLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( resultTypeLabel  ); 
	    // now do the winner label 
		winnerLabel = new JLabel( blank, SwingConstants.CENTER);
        winnerLabel.setFont(new Font("Arial", Font.BOLD, 24));  
	    participantPanel.add( winnerLabel  ); 
	
	    individualGameL = new JLabel("Individual Game Results", SwingConstants.CENTER);
        individualGameL.setFont(new Font("Arial", Font.BOLD, 24));
	    individualGameTA = new JTextArea( 10, 40 ); 
	    individualGameTA.setLineWrap( true ); 
		String buf = " \n";  
	    individualGameTA.setText( buf );
        JPanel gamePanel = new JPanel(); 
		gamePanel.setLayout( new BorderLayout() ); 
		gamePanel.add( individualGameL, BorderLayout.NORTH ); 
	    gamePanel.add( individualGameTA, BorderLayout.CENTER ); 
		JPanel displayPanel = new JPanel(); 
	    displayPanel.setLayout( new BorderLayout() ); 
	    displayPanel.add( gamePanel, BorderLayout.SOUTH ); 
		displayPanel.add( participantPanel, BorderLayout.NORTH);
        displayPanel.add( canvas, BorderLayout.CENTER); 
        String roundStr = "Round "; 
		roundL = new JLabel(roundStr, SwingConstants.CENTER);
		int numberOfContestants = controller.getNumberOfContestants(); 
		int numberOfRounds = controller.getNumberOfRounds(); 
		JPanel roundScore = new JPanel(); 
		roundScore.setLayout(new GridLayout((numberOfContestants+1), 5 )); //, 2, 10));
		//now Place the components in the pane
		int red, green, blue;
		rankLabel = new JLabel[numberOfContestants]; 
	    nameLabel = new JLabel[numberOfContestants]; 
		colourMap = new HashMap<String, Color>(); 
		//nameColour = new Color[numberOfContestants]; 
		firstCompTF = new JTextField[numberOfContestants]; 
	    secondCompTF = new JTextField[numberOfContestants]; 
		thirdCompTF = new JTextField[numberOfContestants]; 
		// now place the column header labels in panel 
		roundScore.add( new JLabel( "Rank", SwingConstants.CENTER )); 
		roundScore.add( new JLabel( "Name", SwingConstants.CENTER )); 
		roundScore.add( new JLabel( controller.getScore1(), SwingConstants.CENTER )); 
		roundScore.add( new JLabel( controller.getScore2(), SwingConstants.CENTER )); 
		roundScore.add( new JLabel( controller.getScore3(), SwingConstants.CENTER )); 
		
		// now fill the scoreboard with contestant labels 
		Contestant contestant; 
		// commented out next line 29/4
		Iterator<Contestant> itr = controller.getCurrentListItr(); 
		Color[] tagColour = new Color[numberOfContestants]; 
		
		for ( int i=0; i< numberOfContestants; i++ ) 
		{
		    rankLabel[i] = new JLabel( Integer.toString( i+1 ), SwingConstants.CENTER ); 
		    roundScore.add( rankLabel[i]);
        //  nameLabel[i] = new JLabel(
			contestant = itr.next(); 
		    //	.lastNameToString(), SwingConstants.CENTER); 
			nameLabel[i] = new JLabel( contestant.getLastName(), SwingConstants.CENTER); 
			if ( i== 0 ) 
		    { 
			    red = 255; //(int)(Math.random() * 256);  //red component
                green = 0; //(int)(Math.random() * 256);//green component
                blue = 0; //(int)(Math.random() * 256); //blue component
		    } 
			else 
			{ 
			    red = 0; 
				green = 0; 
				blue = 255; 
			}
			colourMap.put( contestant.getIdNo(),  new Color( red, green, blue) );
			nameLabel[i].setForeground( colourMap.get( contestant.getIdNo() ) ); 
			roundScore.add( nameLabel[i]);
			firstCompTF[i] = new JTextField ( 3 ); 
			roundScore.add( firstCompTF[i] ); 
			secondCompTF[i] = new JTextField( 4 ); 
			roundScore.add( secondCompTF[i] ); 
			thirdCompTF[i] = new JTextField( 7 ); 
			roundScore.add( thirdCompTF[i] );
		} // end for i 
		// now build the scoreboard 
		JPanel scorePanel = new JPanel(); 
	    scorePanel.setPreferredSize(new Dimension(300, 400)); //numberOfContestants));
		scorePanel.setLayout( new BorderLayout() );  
		scoreL = new JLabel("Scoreboard", SwingConstants.CENTER);
		scoreL.setFont(new Font("Arial", Font.BOLD, 24));
		scorePanel.add( scoreL, BorderLayout.NORTH ); 
		scorePanel.add( roundScore, BorderLayout.CENTER ); 
		//JPanel blankPanel = new JPanel(); 
		// temp commented back in the next line
		//blankPanel.setPreferredSize(new Dimension(200, (200-numberOfContestants)));
		//scorePanel.add( blankPanel, BorderLayout.SOUTH ); 
		JPanel roundPanel = new JPanel(); 
		roundPanel.setLayout( new BorderLayout() ); 
		roundPanel.add( scorePanel, BorderLayout.CENTER);

		pane.setLayout(new BorderLayout());
    pane.add( displayPanel, BorderLayout.CENTER); 
		pane.add( roundPanel, BorderLayout.WEST);
		
		// now create the hall of fame components 
		hallOfFameL = new JLabel("Hall Of Fame", SwingConstants.CENTER);
    hallOfFameL.setFont(new Font("Arial", Font.BOLD, 24));
	  hallOfFameTA = new JTextArea( 10, 40 ); 
		hallOfFameTA.setLineWrap( true ); 
		buf = "                                                    "; 
		hallOfFameTA.setText( buf ); 
		hallOfFameTA.append( "Year    Name"); 
		JPanel hallPanel = new JPanel(); 
		hallPanel.setLayout( new BorderLayout() ); 
		hallPanel.add( hallOfFameL, BorderLayout.NORTH ); 
		hallPanel.add( hallOfFameTA, BorderLayout.CENTER ); 
		
		//pane.add( hallPanel, BorderLayout.EAST ); 
		
		
		frame.setTitle( title );
		//scorePanel.setPreferredSize(new Dimension(300, 400)); //numberOfContestants));
		//blankPanel.setPreferredSize(new Dimension(200, (200-numberOfContestants)));
	
		canvas.setPreferredSize(new Dimension(width, height));
        
		backgroundColor = bgColor;
    frame.pack();
		objects = new ArrayList<Object>();
    shapes = new HashMap<Object, ShapeDescription>();
	} // end constructor 
	
	/* 
	* updates the header label to indicate the current round. 
	*/ 
	public void updateCityLabel() 
	{ 
		String[] l = controller.getCurrentRoundStr(); 
		if ( l.length == 9 ) 
		{   
		    //gameResult = l[9]; 
		    repetitionLabel.setText(l[0]); 
			roundLabel.setText(l[1]); 
			gameLabel.setText(l[2]); 
			
			blueContestantLabel.setText( l[3] ); 
			vsLabel.setText(l[4]); 
			redContestantLabel.setText(l[5]); 
			resultLabel.setText(l[6]); 
			resultTypeLabel.setText(l[7]); 
			winnerLabel.setText(l[8]); 
			toggle[0] = l[8];
		}
		//else if ( l.length == 2 ) 
		//    cityLabel.setText(l[0] + " " + l[1]  ); 
	 //else 
	  //   cityLabel.setText(l[0]  ); 
	
	} // end update city label  
	
	public void toggleCityLabel( int tab ) 
	{ 
	     winnerLabel.setText(toggle[tab%2]); 
		  
	} // end toggle city label 
	
	/* 
	* updates the scoreboard panel with the current state of the tournament. 
	*/ 
	public void updateRoundScoreBoard() 
	{ 
		//LinkedList<Contestant> list =  controller.getCurrentRoundList(); 
		LinkedList<Contestant> list =  controller.orderedContestantList; 
	
		int i= 0; 
		for( Contestant c: list ) 
		{  
		    nameLabel[i].setText( c.getLastName() ); 
			nameLabel[i].setForeground( colourMap.get( c.getIdNo() ) ); 
			firstCompTF[i].setText( (" " +c.getFirstComponent()) ); 
		    secondCompTF[i].setText( (" " +c.getSecondComponent()) ); 
		    thirdCompTF[i].setText( (" " +c.getThirdComponent()) ); 
	      i++; 
		} // end for i 
		String buf; 
		LinkedList<String> iGL = controller.getIndividualGameList(); 
		//for ( int k=0; k< iGL.size(); k++ ) 
		//{ 
		    buf = iGL.get(iGL.size()-1); ; 
		    buf += "\n";
				if ( (iGL.size()%6) == 0 ) 
				    individualGameTA.setText( buf );
				else 
				    individualGameTA.append( buf ); 
    //} // end for i
		
	} // end update round score board 
		 
	/**
     * Set the canvas visibility and brings canvas to the front of screen
     * when made visible. This method can also be used to bring an already
     * visible canvas to the front of other windows.
     * @param visible  boolean value representing the desired visibility of
     * the canvas (true or false) 
     */
    public void setVisible(boolean visible)
    {
        if(graphic == null) 
		    {
            // first time: instantiate the offscreen image and fill it with
            // the background color
            Dimension size = canvas.getSize();
			      canvasImage = canvas.createImage(size.width, size.height);
            graphic = (Graphics2D)canvasImage.getGraphics();
            graphic.setColor(backgroundColor);
            graphic.fillRect(0, 0, size.width, size.height);       
        }
        frame.setVisible(visible);
	} // end setVisible 

    /**
     * Draw a given shape onto the canvas.
     * @param  referenceObject  an object to define identity for this shape
     * @param  shapeList, a LinkedList of the shape objects to be drawn on the canvas
     */
	public void draw(Object referenceObject, LinkedList<ShapePacket> shapeList)
	{
        objects.remove(referenceObject);   // just in case it was already there
        objects.add(referenceObject);      // add at the end
        shapes.put(referenceObject, new ShapeDescription( shapeList));
        redraw();
	} // end draw 
 
    /**
     * Erase a given shape's from the screen.
     * @param  referenceObject  the shape object to be erased 
     */
    public void erase(Object referenceObject)
    {
        objects.remove(referenceObject);   // just in case it was already there
        shapes.remove(referenceObject);
        redraw();
	} // end erase 

    /**
     * Set the foreground color of the Canvas.
     * @param  newColor   the new color for the foreground of the Canvas 
     */
    public void setForegroundColor(String colorString)
    {
        if(colorString.equals("red")) 
				{
            graphic.setColor(Color.red);
        }
        else if(colorString.equals("black")) 
				{
            graphic.setColor(Color.black);
        }
        else if(colorString.equals("blue")) 
				{
            graphic.setColor(Color.blue);
        }
        else if(colorString.equals("yellow")) 
				{
            graphic.setColor(Color.yellow);
        }
        else if(colorString.equals("green")) 
				{
            graphic.setColor(Color.green);
        }
        else if(colorString.equals("magenta")) 
				{
            graphic.setColor(Color.magenta);
        }
        else if(colorString.equals("white")) 
				{
            graphic.setColor(Color.white);
        }
        else 
				{
            graphic.setColor(Color.black);
        }
    }
   
	
    /**
     * Wait for a specified number of milliseconds before finishing.
     * This provides an easy way to specify a small delay which can be
     * used when producing animations.
     * @param  milliseconds  the number 
     */
    public void wait(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        } 
        catch (Exception e)
        {
            // ignoring exception at the moment
        }
    }

    /**
     * Redraw ell shapes currently on the Canvas.
     */
    private void redraw()
    {
        erase();
        for(Object shape : objects) 
		    {
            shapes.get(shape).draw(graphic);
        }
        canvas.repaint();
	} // end redraw 
       
    /**
     * Erase the whole canvas. (Does not repaint.)
     */
    private void erase()
    {
        Color original = graphic.getColor();
        graphic.setColor(backgroundColor);
        Dimension size = canvas.getSize();
        graphic.fill(new Rectangle(0, 0, size.width, size.height));
        graphic.setColor(original);
	} // end erase 


    /************************************************************************
     * Inner class CanvasPane - the actual canvas component contained in the
     * Canvas frame. This is essentially a JPanel with added capability to
     * refresh the image drawn on it.
     */
    private class CanvasPane extends JPanel
    {
        public void paint(Graphics g)
        {
            g.drawImage(canvasImage, 0, 0, null);
        }
	} // end CanvasPane inner class
    
     /************************************************************************
     * Inner class CanvasPane - the actual canvas component contained in the
     * Canvas frame. This is essentially a JPanel with added capability to
     * refresh the image drawn on it.
     */
    private class ShapeDescription
    {
        private LinkedList<ShapePacket> shapeList;

		/* 
		* constructs this with specified parameter. 
		*/ 
    public ShapeDescription( LinkedList<ShapePacket> shapeList)
    {
        this.shapeList = shapeList;
		} // end constructor 

		/* 
		* draws this with specified graphic. 
		*/ 
    public void draw(Graphics2D graphic)
    {
        for ( ShapePacket sP: shapeList )
			  {
				    setForegroundColor( sP.getColour());
			      graphic.fill( sP.getShape()); 
	     }
		} // end draw 
	} // end ShapeDescription inner class 
 
}  // end ControllerCanvas class 
