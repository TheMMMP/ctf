package src.View; 

/* 
* ShapePacket 
* @author Phil Sheridan 
* @version 12/09/09 
*/ 



import java.awt.*;
import java.awt.geom.*;
import java.util.LinkedList; 

public class ShapePacket 
{ 
	private Shape shape; 
	private String colour; 
	
	public ShapePacket( Shape shape, String colour ) 
	{ 
		this.shape = shape; 
		this.colour = colour; 
	} 
	
	public Shape getShape() 
	{ 
		return shape; 
	} 
	
	public String getColour() 
	{ 
		return colour; 
	} 
} // end ShapePacket class 