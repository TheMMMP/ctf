package src.View; 

/* 
* DrawableBloodBankController.java 
* @author Phil Sheridan 
* @version 3/3/15 
*/ 

import java.util.LinkedList; 
import java.util.Iterator; 
import java.util.Map; 
import java.util.HashMap; 
import src.Model.*; 
import src.Model.BloodBank.*; 
 	
public class DrawableBloodBankController extends BloodBankController 
{  
	
	protected BoardComponentList baseCity; 
	private DrawableBoardComponentList dBCL;			
	private  ControllerCanvas canvas; 	
    private BoardComponentList fieldBorder; 
	private int moveCount; 
	/* 
	 ** constructs this. 
	 **/ 
	public DrawableBloodBankController( int totalTime, String[] filename, boolean randomise ) 
	{ 
		super(totalTime, filename ); 
	 	dBCL = new DrawableBoardComponentList( agentBCList[0] );
		fieldBorder = new BoardComponentList(); 
		BoardComponent bC; 
		String colour = "black"; 
		// now do the borders 
		bC= new BoardComponent( ((MAX_X)), (MAX_Y), 2, MAX_Y, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now add right border 
		//bC= new BoardComponent( (MAX_X-1), (MAX_Y/2), 2, MAX_Y, true ); 
		//bC.setColour( "red"); 
		//fieldBorder.add( bC ); 
		// now add upper boreder 
		bC= new BoardComponent( (MAX_X-(MAX_X/4)), 2, (MAX_X/2), 2, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now add the lower border 
		bC= new BoardComponent( (MAX_X-(MAX_X/4)), (MAX_Y-2), (MAX_X/2), 2, true ); 
		bC.setColour( "red"); 
		fieldBorder.add( bC ); 
		// now do the same for the blue 
		// now do mid field marker 
		bC= new BoardComponent( (MAX_X/2-1), (MAX_Y/2), 2, MAX_Y, true ); 
		//bC.setColour( "blue"); 
		//fieldBorder.add( bC ); 
		// now add left border 
		bC= new BoardComponent( 2, (MAX_Y/2), 2, MAX_Y, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
		// now add upper boreder 
		bC= new BoardComponent( (MAX_X/4), 2, (MAX_X/2), 2, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
		// now add the lower border 
		bC= new BoardComponent( (MAX_X/4), (MAX_Y-2), (MAX_X/2), 2, true ); 
		bC.setColour( "blue"); 
		fieldBorder.add( bC ); 
			     
	} // end constructor 

	/* 
	 ** runs this. 
	 */ 
	public void runDrawableController() 
	{
        System.out.println("DBBC-rC-start");  
	    canvas = ControllerCanvas.getCanvas();  	       
		for ( int repetition=0; repetition< numberOfRepetitions; repetition++ ) 
		{ 
			sizeOfTeam = agentBCList[repetition].size(); 
		    for ( int round = 0; round < numberOfRounds; round++ ) 
			{  
			    for ( int game=0; game< numberOfGames; game++ ) 
		        { 
				    //canvas.updateCityLabel(); 
					runNextDrawableGame( repetition,  round, game ); 
					//canvas.updateCityLabel(); 
					//System.out.println("DCTFC-rDC-after runNextRGame");  
			        canvas.wait( 2000 ); 
				} // end for game 
			} // end for round 
		} // end for repitition  
		setTournamentResultsLabel(); 
		canvas.updateCityLabel(); 
		resultsToFile(); 
	 	
		//printAllScores(); 
	} // end run drawable controller 
	
	/* 
	** runs next drawable game. 
	**/	
    public void runNextDrawableGame( int repNo, int round, int game ) 
	{ 
	    //System.out.println("DBBC-rNDG-start, numberOfTeams= " + numberOfTeams); 
	    int[] contestantId = new int[numberOfTeams];
	    for ( int t=0; t< numberOfTeams; t++ ) 
	    {	        
			contestantId[t] = schedule[round][game][t]; 
			//System.out.println("CTF-rNG-contestantId= " + contestantId[t] ); 
			//System.out.println("DBBC-rNDG-t=" + t + ", contestantId= " + contestantId[t] );
			player[t] = teamList[contestantId[t]]; 
			//for ( int j=0; j< player[t].length; j++ ) 
			  //System.out.println("DBBC-rNDG-player " + j + " is " + ((BoardComponent) player[t][j]).getShape()); 
			//System.out.println("DBBC-c-exiting");  
		} // end for t 
		//System.out.println("DBBC-rNG-before initialise"); 
		initialiseGame( repNo, round, game, contestantId ); 
        timeRemaining = totalTime; 
		canvas.updateCityLabel(); ; 
		boolean finished = false; 
		while ( !finished ) 
		{	
		    playGame(); 
			dBCL.makeInvisible();	
			updateDBCL( round );
			dBCL.makeVisible(); 
			finished = isGameFinished();  
		} // end while	
		//System.out.println("CTFC-rNG-finished while");
		updateCurrentContestants( repNo, round, game, contestantId ); 
		canvas.updateRoundScoreBoard();    
		canvas.updateCityLabel(); 
	    for ( int blink=0; blink< 9; blink++ ) 
		{ 
		    canvas.toggleCityLabel(blink); 
	        canvas.wait( 1000 ); 
	       } // end for blink
       } // end run next drawable game 
		
    private void updateDBCL( int round ) 
    {
	    BoardComponent bC, smallBC; 
	    dBCL.clear(); 
		for ( int i=0; i< currentDonorList.size(); i++ ) 
		    dBCL.add( currentDonorList.retrieve(i) ); 
		for (int i=0; i< currentBloodBankList.size(); i++ ) 
		{    
		    BloodBank bloodBank = (BloodBank) currentBloodBankList.retrieve(i); 
			dBCL.add( bloodBank ); 
			int load = bloodBank.getBloodLoad(); 
			if ( load > 0 ) 
			{ 
			    int[] xP = bloodBank.getXPoints(); 
				int[] yP = bloodBank.getYPoints(); 
				double proportion = ((double) bloodBank.getBloodLoad())/bloodBank.getCapacity(); 
				//System.out.println("load= " + bloodBank.getBloodLoad() + ", cap= " + bloodBank.getCapacity() + ", proportion= " + proportion ); 
				int width = 1 + (int) (proportion*xP[1]); 
				int x = xP[0] + (width/2); 
				int y = yP[0]+ (yP[1]/2); 
				smallBC = new BoardComponent( x, y, width, yP[1], true ); 
				smallBC.setColour( "red");  
				//System.out.println("DBBC-uDC-bloodBank-smallBC= " + smallBC.toString() );  
				dBCL.add( smallBC ); 
			} // end if blood load > 0  
		} // end for i 
		// now add the agents 
		for ( int i=0; i< sizeOfTeam; i++ ) 
		{ 
			bC = player[0][i]; 	   
			//System.out.println("DBBC-uDBL-agent= " + bC.toString() ); 
			dBCL.add( bC ); 
			if ( currentBloodLoadList[i] > 0 ) 
			{ 
			    double[] xY = bC.getCentrePoint(); 
				smallBC = new BoardComponent( xY[0], xY[1], 10, 10, false ); 
				smallBC.setColour("red"); 
				//System.out.println("DBBC-uDC-smallBC= " + smallBC.toString() ); 
				dBCL.add( smallBC ); 
			}
		} 
		//moveCount++; 
		//if ( moveCount > 3 ) 
		//{ 
			//System.out.println("DBBc-uDBCL-exiting"); 
			//System.exit(0); 
		//}
		//else 
			//System.out.println("DBBC-uDBL-----------------"); 
	} // end update dbcl  

    static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // keep false when testing, otherwise randomises red and blue contestants
	    int gameDuration = 400; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "BloodBankContestantNames.txt"; 
	    filename[1] = "BloodBankCityNames.txt"; 
		DrawableBloodBankController dC = new DrawableBloodBankController( gameDuration, filename, randomise );  
	    ControllerCanvas.createController( dC ); 
	    dC.runDrawableController(); 
	} // end main 

} // end DrawableBloodBankController class 
