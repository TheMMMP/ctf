package src.View; 

/* 
* DrawableTagController.java 
* @author Phil Sheridan 
* @version 23/2/15 
*/ 

import java.util.LinkedList; 
import java.util.Iterator; 
import java.util.Map; 
import java.util.HashMap; 
import src.Model.Tag.*; 
 
public class DrawableTagController extends TagController 
{ 
    private Iterator<Contestant> compItr; 
    private int currentRound; 
	private Contestant currentCompetitor;
	private Contestant secondCompetitor;
	private long timeConstraint; 
	protected  BoardComponentList currentCity; 
    private Map<String, BoardComponentList> algorithmMap; 
    private LinkedList<Contestant> competitorQueue; 
	protected Contestant bestContestant; 
    public LinkedList<String> cityList; 
	protected BoardComponentList baseCity; 

	
	public DrawableTagController( int totalTime, String agentBCFilename, String contestantListFilename ) //String contestantFilename, String cityFilename, long timeConstraint, contestantListFilename ) 
	{ 
		super( totalTime, agentBCFilename, contestantListFilename ); //contestantFilename, cityFilename, timeConstraint, contestantListFilename ); 
	} // end constructor 
 
	
	public void runController() 
	{
      //System.out.println("DC-rC-start"); 
		  int currentRound =0; 
	    while ( !isFinished() ) 
		  {	
		      //System.out.println("DC-rC-inside while, not finished"); 
			    startNextRound();
					while ( hasCompetitor() ) 
			    {
			        //System.out.println("Dc-rC-inside while has next competitor"); 
				      runNextCompetitor(); 
		          ControllerCanvas canvas = ControllerCanvas.getCanvas(); 
				      canvas.updateRoundScoreBoard(); 
			        canvas.wait( 2000 );
			    } // end while has contestant 
      } // end while not finished 
	} // end run controller 
	
		/* 
	* determines whether this has a competitor or not. 
	* @return true if competitor queue is not empty, false otherwise .
	*/ 
	public boolean hasCompetitor() 
	{ 
		return compItr.hasNext();  
	} // end has competitor 
/* 
	* determines whether this tournament is finished or not. 
	* @return true if finished, false otherwise. 
	*/
	public boolean isFinished() 
	{ 
	   return ( currentRound >= numberOfRounds ); 
	} // end is finished 	
	
	/* 
	* runs the next competitor's allocation algorithm on current city.  
	* @precondition: assumes method hasCompetitor has been called and returned a true. 
	*/ 
	public void runNextCompetitor() 
	{ 
	    int count = 0; 
		  currentCompetitor = compItr.next(); 
	    secondCompetitor = compItr.next();
			DrawableBoardComponentList dBCL = new DrawableBoardComponentList( agentBCList ); 
			dBCL.makeVisible(); 
			boolean finished = false;
			while ( !finished ) 
			{ 
			    dBCL.makeInvisible();	
					playTag(); 
          dBCL.makeVisible(); 
				  finished = (getCurrentTime()>  getTotalTime());
		  } // end for
	} // end run next contestant  
	
	private void printStatus() 
	{ 
	    int[] cR = new int[2]; 
	    double[] xY;
			BoardComponent bC0 = agentBCList.retrieve( 0 ); 
			BoardComponent bC1 = agentBCList.retrieve( 1 ); 
			//Agent a0 = new S123456TagAgent( bC0 ); 
			 //Agent a1 = new S123456TagAgent( bC1 ); 
			xY =  bC0.getCentrePoint(); 
			cR[0] = (int) xY[0]; 
			cR[1] = (int) xY[1]; 
			int orA0 = player[0].getOrientationDegrees(); 
			System.out.println("bC0 (x,y)=  " + cR[0] + ", " + cR[1] + ", or= " + orA0 ); 
			xY =  bC1.getCentrePoint(); 
			cR[0] = (int) xY[0]; 
			cR[1] = (int) xY[1]; 
			int orA1 = player[1].getOrientationDegrees(); 
			System.out.println("bC1 (x,y)= " + cR[0] + ", " + cR[1]+ ",  or= " + orA1 ); 
		  boolean a0IsIt = ((TagAgent) player[0]).isIt(); 
			//int it;
			double[] v;
			//System.out.println("a0IsIt = " + a0IsIt);
			if ( a0IsIt ) 
			{    
			    v = player[0].getLocationOf( bC1 );
					//it = 0; 
			}
			else 
			{	  
			    v = player[1].getLocationOf( bC0 );
					//it = 1; 
			}
	
		 int distance = (int) v[1]; //bC0.distance( bC1 ); 
			int angle = (int) (v[0]*180/Math.PI); //(a0.angleTo( bC1 )*180/Math.PI);
			System.out.println(distance + ", is distance. it = " + it + ", angle= " + angle); 
			System.out.println(); 
			//+ ", distance/angle from bC0 to bC1 is " + distance + ", " + angle);
	} // end print status 
 
	public void startNextRound() 
	{
      ControllerCanvas canvas = ControllerCanvas.getCanvas(); 
			currentRound++; 
		 competitorQueue = reversedCopy( contestantList ); 
		 compItr = competitorQueue.iterator(); 
		 bestContestant = new Contestant( "a", "b", "c" ); 
		 bestContestant.updateNumberOfDinosaurs( Integer.MAX_VALUE ); 
		 bestContestant.updateElapsedTime( Integer.MAX_VALUE );	
	} // end start next round 

	/* 
	* @return numberOfRounds 
	*/ 
	public int getNumberOfRounds() 
	{ 
		return numberOfRounds; 
	}
	
	/* 
	* @return a ListIterator for the contestantList. 
	*/ 
	public Iterator<Contestant> getCurrentListItr() 
	{
		return contestantList.iterator(); 
	} 

	/* 
	* @return size of the contestantList
	*/ 
	public int getNumberOfContestants() 
	{ 
		return contestantList.size(); 
	} 

	/* 
	* @return contestantList 
	*/ 
	public LinkedList<Contestant> getCurrentRoundList() 
	{ 	
		return contestantList;
	} 

	/* 
	* makes a shallow copy of current contestantList in reverse order. 
	*/ 
	static public LinkedList<Contestant> reversedCopy( LinkedList<Contestant> list ) 
	{ 
		// lab exercise, write the body of this method. 
		LinkedList<Contestant> rList = new LinkedList<Contestant>(); 
		//int inc = 0;
		for ( int i= (list.size()-1); i>=0; i-- ) 
			  rList.add( list.get( i ) ); 
		return rList; 
	} // end reverse copy

/* 
	* gets the name of the current round. 
	* @return round number concatenated with current city name minus the last four chars.
	*/ 
	public String getCurrentRoundStr() 
	{ 
		if ( currentRound < cityList.size() ) 
		{
	        String filename = cityList.get( currentRound ); 
          filename = filename.substring(  0, (filename.length()-4) ); 
		      String str = "Round " + (currentRound+1) + " " + filename + " City";
		      return str; 
		}
		else 
		{ 
			  Contestant c = contestantList.get( 0 ); 
			  String str = "" + c.getFirstName() + " " + c.getLastName(); 
			  str += " Winner!"; 
			  return str; 
		}
  } // end get current round 
		
	
} // end DrawableTagController class 
