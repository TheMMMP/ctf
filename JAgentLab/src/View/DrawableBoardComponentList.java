package src.View; 

/* 
* DrawableBoardComponentList.java extends to add functionality required to use with Canvas. 
* @author Phil Sheridan 
* @version 7/3/14 
*/ 


import java.awt.*;
import java.awt.geom.*;
import java.util.LinkedList; 
import src.Model.*; 

public class DrawableBoardComponentList extends BoardComponentList 
{ 
	
	private String aColour; //
	private boolean isVisible; 
	private double direction;  
	private int count;
	
	/* 
	* construct this with specified parameter. 
	* @param road, the Road object to be passed to super class. 
	* @postcondition: all the colours of the road are set. 
	*/
	public DrawableBoardComponentList( BoardComponentList bCL) 
	{
     super( bCL); 	
	   aColour = "redw"; 
			direction = 0.0;
			count = 0;
	} // end constructor 
	
    /**
    * Make this  visible. If it was already visible, do nothing.
    */
    public void makeVisible()
    {
        isVisible = true;
        draw();
	} // end make visible 
	
	/*
    * Make this invisible. If it was already invisible, do nothing.
    */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
	} // end make invisible 

	/**
    * Draw the BoardComponents with current specifications on screen.
    */
    private void draw()
    {
        if(isVisible) 
		    {
            ControllerCanvas canvas = ControllerCanvas.getCanvas();
		        LinkedList<ShapePacket> sPList = new LinkedList<ShapePacket>(); 
					  Shape shape; 
			      ShapePacket sP; 
					  BoardComponent bC;
					  int w; 
					  int[] xP; 
						int[] yP;
						for ( int i = 0; i< size; i++ ) 
					  {
						   
					      bC = list[i]; //.get(i);
					      aColour = bC.getColour(); 
								//System.out.println("DBCL-d- bC is " + bC.toString() );
								xP = bC.getXPoints(); 
								yP = bC.getYPoints(); 
								//System.out.println("DBCL-d"); 
						    //if ( true ) //count < 2 ) 
						    //{
						      //  for ( int k=0; k< xP.length; k++ )
						       //     System.out.print("(" + xP[k] + ", " + yP[k] + ")"); 
								   // System.out.println(); 
						        //count++; 
						    //} 
				
								if ( bC.getShape().equals("ellipse") ) 
								{
										shape =  new Ellipse2D.Double(xP[0], yP[0], xP[1], yP[1] ); 
					          sP = new ShapePacket( shape, aColour ); 
			              //System.out.println("DBCL-d-added an ellipse= " + bC.toString()); 
										sPList.add( sP );
										//System.out.println("DBCL-d-we have an ellipse");
							} // end if is ellipse 
							else if ( bC.getShape().equals("rectangle") ) 
							{
							    //System.out.println("DBCL-d-a rectangle- " + xP[0] + ", " + yP[0] + ", " + xP[1] + ", " + yP[1] );  
										shape =  new Rectangle2D.Double(xP[0], yP[0], xP[1], yP[1] ); 
					          sP = new ShapePacket( shape, aColour ); 
			              sPList.add( sP ); 
							} // end else is rectangle 
							else if ( bC.getShape().equals("triangle") )
							{    
				          shape =  new Polygon( xP, yP, 3) ; 
			            sP = new ShapePacket( shape, aColour ); //bC.getColour() ); 
			            sPList.add( sP );
								}	
							
			    } // end for i                 
					canvas.draw(this,sPList ); 
			    canvas.wait(10);
		  } // end if visible 
	}  // end draw  

	/**
     * Erase the BoardComponent objects  on screen.
     */
    private void erase()
    {
        if(isVisible) 
		{
            ControllerCanvas canvas = ControllerCanvas.getCanvas();
            canvas.erase(this);
        }
	} // end erase 
	
} // end DrawableBoardComponentList class
