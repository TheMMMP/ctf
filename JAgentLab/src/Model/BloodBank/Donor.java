package src.Model.BloodBank;
/*
* Donor.java  extends BoardComponent class
* @author Phil Sheridan 
* @version 18/3/14
*/ 
import src.Model.*;

public class Donor extends BoardComponent
{
    // instance variables 
		
	/* 
	* constructs this from specified parameter
	*/ 
    public Donor( BoardComponent bC) 
    { 
	    super( bC); 
    } // end constructor 

    /* 
    * constructs default Donor 
    */ 
    public Donor() 
    { 
        super();
    } // end constructor  

    /* 
    * decrements this blood by one unit if current > 1 unit
    */
    public void decrementBloodLoad() 
    { 
        if ( xPoints[1] > 1 ) 
		{ 
		    xPoints[1]--; 
		    xPoints[0] = -xPoints[1]/2; 
		    yPoints[1]--; 
		    yPoints[0] = -yPoints[1]/2;
		} // end if has donatable blood 
    } // end decrement blood load 

    /* 
    * returns the level of the current blood load 
    */ 
    public int getBloodLoad() 
    { 
        return xPoints[1];
    } // end get blood load 	

    public int getRadius() 
    { 
        return (int) Math.floor((xPoints[1]/((double) 2.0))+0.5);
    } 

    public String toString() 
    { 
        String str = "load is " + getBloodLoad() + ", xY is (" + ((int) xY[0]) + ", " + ((int) xY[1]) + ")";
		return str;
    }
} // end Donor class

