package src.Model.BloodBank;
/*
* BloodBank.java  extends BoardComponent class
* @author Phil Sheridan 
* @version 18/3/14
*/ 
import src.Model.*;

public class BloodBank extends BoardComponent
{
    // instance variables 
	private int capacity; 
	private int myBloodLoad; 
		
	/* 
	* constructs this from specified parameter
	* @param bC, the BoardComponent object that determines the location of this. 
	* @param capacity, an int that determines the maximum blood load of this. 
	*/ 
    public BloodBank( BoardComponent bC, int capacity) 
	{ 
	    super( bC);
		this.capacity = capacity;
		myBloodLoad = 0;
	} // end constructor 

    public BloodBank( BloodBank bB ) 
    { 
        super( bB ); 
		capacity = bB.capacity;
		myBloodLoad = bB.myBloodLoad; 
    } // end constructor 

    /* 
    * constructs default BloodBank 
    */ 
    public BloodBank() 
    { 
        super(); 
		capacity = 0;
		myBloodLoad = 0;
    } // end constructor  

    public int getCapacity() 
    { 
        return capacity;
    } 

    /* 
    * gets the spare capacity of this 
    * @return int representing spare capacity 
    */ 
    public int getSpareCapacity() 
    { 
        return (capacity-myBloodLoad);
    } 

    /* 
    * increases this blood byspecified parametere unit if has capacity
    * @param load, the amount by which to increase this load
    * @precondition: myBloodLoad+ load <= capacity
    */
    public void increaseBloodLoad( int load) 
    { 
        if ( (load >= 0) &&(( load + myBloodLoad ) <= capacity )) 
		{ 
		    myBloodLoad += load;
		} 
    } // end increase blood load 

    /* 
    * returns the level of the current blood load 
    */ 
    public int getBloodLoad() 
    { 
        return myBloodLoad;
    } // end get blood load 	

    public int getRadius() 
    { 
        return (xPoints[1]/2);
    } 

    public String toString() 
    { 
        String str = "blood load= " + myBloodLoad + ", capacity= " + capacity; 
		return str;
    }
} // end BloodBank class

