package src.Model.BloodBank;
/*
* BloodBankAgent.java  extends Agent class
* @author Phil Sheridan 
* @version 5/2/15
*/ 
import src.Model.*;

public abstract class BloodBankAgent extends Agent
{
    // static constants 
	static public final int MY_CAPACITY = 10; 
		
    // instance variables 
	protected int myBloodLoad; 		
	protected BoardComponentList visibleAgents; 
	protected BoardComponentList visibleDonors; 
	protected BoardComponentList visibleBloodBanks; 
	
	/* 
	* constructs this from specified parameter
	*/ 
    public BloodBankAgent( BoardComponent bC) 
	{ 
	    super( bC);
		myBloodLoad = 0;
		visibleAgents = new BoardComponentList(); 
		visibleDonors = new BoardComponentList(); 
		visibleBloodBanks = new BoardComponentList(); 		
	} // end constructor 

    /* 
    * constructs default BloodBankAgent 
    */ 
    public BloodBankAgent() 
    {     
        super();
		myBloodLoad = 0;
		visibleAgents = new BoardComponentList(); 
		visibleDonors = new BoardComponentList(); 
		visibleBloodBanks = new BoardComponentList(); 	
    } // end constructor  
		
    public void incrementBloodLoad() 
    { 
        if ( myBloodLoad < MY_CAPACITY ) 
		    myBloodLoad++;
    } // end increment blood load 

    /* 
    * reduces my blood load by specified amount
    * @param amount, the amount by which myBloodLoad is reduced 
    * @precondition: amount > myBloodLoad and zero
    */ 
    public void reduceBloodLoad( int amount) 
    { 
        if ((amount>=0) && (amount<= myBloodLoad) ) 
		    myBloodLoad -= amount; 
    } // end reduce blood load  

    public int getBloodLoad() 
    { 
        return myBloodLoad;
    } 

    /* 
    * set this blood load to the specified parameter
    * @param load, the amount of the level to be set
    * @precondition: (load < mY_CAPACITY)
    */ 
    public void setBloodLoad( int load ) 
    { 
        if (( load >= 0 ) && ( load<= MY_CAPACITY))
		    myBloodLoad = load;
    } // end set blood load 

	/* 
	* set the current list of visible agents from specified parameter 
	* @param vAL, the BoardComponentList containing the visible agents
	*/ 
	public void setVisibleAgents( BoardComponentList vA) 
	{ 
	    visibleAgents = null; 
		visibleAgents = vA; 
	} // end set visible agents 
		
	/* 
	* set the current list of visible donors from specified parameter 
	* @param vBBL, the BoardComponentList containing the visible blood blobs
	*/ 
	public void setVisibleDonors( BoardComponentList vBBL) 
	{ 
	    visibleDonors = null; 
		visibleDonors = vBBL; 
	} // end set visible blood blobs
		
	/* 
	* set the current list of visible blood banks from specified parameter 
	* @param vBBL, the BoardComponentList containing the visible blood banks
	*/ 
	public void setVisibleBloodBanks( BoardComponentList vA) 
	{ 
	    visibleBloodBanks = null; 
		visibleBloodBanks = vA; 
	} // end set visible blood banks
		
	public String toString() 
	{ 
	    //   String str = super.toString() + 
		String str = "blood Load= " + myBloodLoad + ", xY is (" + ((int) xY[0]) + ", " + ((int) xY[1]) + ")"; 
		return str; 
    } // end to string 
		
} // end BloodBankAgent class

