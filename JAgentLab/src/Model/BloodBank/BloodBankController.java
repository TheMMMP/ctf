package src.Model.BloodBank;
/*
* BloodBankController.java 
* @author Phil Sheridan 
* @version 18b/3/15 modified method initialiseGame, blood bank capacity  
*/

import java.io.*; 
import java.util.Scanner; 
import java.util.HashMap; 
import java.util.LinkedList; 
import java.util.Iterator; 
import src.Model.*;
import src.Model.BloodBank.*;

public class BloodBankController extends Controller
{ 
    static private double visibleRange = 50; 
	static private double tagRange = 20;
   	
   	/* 
   	 ** gets the time remaining in this game 
   	 * @return int represents number of remaining simulated seconds 
   	 **/ 
   	static public int getTimeRemaining() 
	{ 
	    return timeRemaining; 
	}  

    // instance variables 
	protected BoardComponentList[] donorBCList; 
	protected BoardComponentList[] bloodBankBCList;  
	protected BoardComponentList currentDonorList; 
	protected BoardComponentList currentBloodBankList;  
	protected int[] currentBloodLoadList;  
	protected boolean allBloodBanksFull; 
		
	/* 
	* constructs this 
	*/ 
	public BloodBankController( int totalTime, String[] filename ) 
	{ 
		this( totalTime, filename, false );  
	} // end constructor 
	
    public BloodBankController( int totalTime, String[] filename, boolean isAssessorMode )  
	{ 
	    super( totalTime, filename[0], filename[1], "BloodBankAgent" , 1, false, isAssessorMode ); 
	    title = "Blood Bank";
		resetContestantList(); 
		String cityName, agentFilename, donorFilename, bloodBankFilename; 
		agentBCList = new BoardComponentList[cityList.size()]; 
		donorBCList = new BoardComponentList[cityList.size()]; 
	    currentDonorList = new BoardComponentList(); 
		currentBloodBankList = new BoardComponentList(); 	
		bloodBankBCList = new BoardComponentList[cityList.size()]; 
		for ( int i =0; i< cityList.size(); i++ ) 
		{
		    cityName = cityList.get(i); 
			agentFilename = basePathInputCity + cityName + "AgentBC.txt"; 
			//System.out.println("BBC-c-agentFile= " + agentFilename ); 
			agentBCList[i] = readBoardComponents( agentFilename, "triangle" );  
			//System.out.println("BBC-c-size of agentBCList for " + i + " is " + agentBCList[i].size()); 
			// now set the colour of all agents 
			for ( int j=0; j<agentBCList[i].size(); j++ ) 
			{ 
			    agentBCList[i].retrieve(j).setColour("blue"); 
			} // ??
			donorFilename = basePathInputCity + cityName + "DonorBC.txt"; 
			donorBCList[i] = readBoardComponents( donorFilename, "ellipse" ); 
			// now set the colour of all donors 
			for ( int j=0; j<donorBCList[i].size(); j++ ) 
			{ 
			    donorBCList[i].retrieve(j).setColour("red"); 
			}
			bloodBankFilename = basePathInputCity + cityName + "BloodBankBC.txt"; 
			bloodBankBCList[i] = readBoardComponents( bloodBankFilename, "rectangle" );
			// now set the colour of all blood banks 
			for ( int j=0; j<bloodBankBCList[i].size(); j++ ) 
			{ 
			    bloodBankBCList[i].retrieve(j).setColour("black"); 
			}
			// next3  lines are tests of file content at standardoutput 
			printBCList( agentBCList[i], agentFilename);  
			printBCList( donorBCList[i], donorFilename);  
			printBCList( bloodBankBCList[i], bloodBankFilename);  
		} // end for i 
		setAlgorithmList();
		gamePlayer = new String[numberOfRepetitions][numberOfRounds][numberOfGames][2]; 
		gameResult = new int[numberOfRepetitions][numberOfRounds][numberOfGames]; 
		individualGameList = new LinkedList<String>(); 
		System.out.println("BBC-c-finished constructing ");  	
	} // end constructor	
		
		private BoardComponentList readBoardComponents( String filename, String shape ) 
		{ 
		    BoardComponentList list; 
			try 
			{ 
			    list = boardComponentsFromFile( filename, shape );
			} 
			catch( FileNotFoundException e ) 
			{
			    System.out.println("BBC-rC- " + e.toString()); 
				list = new BoardComponentList(); 
			}
		    return list; 
		} // end read components 
	/* 
	 ** implements abstract method,. 
	 ** It allows this to override the variable speed and orientation made in the contestant. 
	 ** But nothing to do in the BloodBank game. 
	 **/ 
	 public void imposeGameRestrictions() 
	 { 
	 	// nothing to do here
	 }	
	/* 
	* Implements the abstract method to update all player's BoardComponent objects
	*/   
	public void updateAllBoardComponents() 
	{
	    //int rNum = currentRound;
	    // now update the blood loads of donors and blood banks
		updateBloodLoads(); 			
		//  now update each player's visibility
	    BoardComponent bC, nextBC; //player.length]; 
		Donor donor; 
		BloodBank bloodBank;
		BoardComponentList[]  vA = new BoardComponentList[sizeOfTeam]; 
		BoardComponentList[]  vD = new BoardComponentList[sizeOfTeam]; 
		BoardComponentList[]  vB = new BoardComponentList[sizeOfTeam]; 
		for ( int i=0; i< sizeOfTeam; i++ ) 
		{    
	        vA[i] = new BoardComponentList();
			vD[i] = new BoardComponentList();
			vB[i] = new BoardComponentList(); 
		} // end for i 
		for (int  i=0; i< (sizeOfTeam-1); i++ ) 
		{ 
		    bC = player[0][i]; 
			// now check the distances between each agent and it successor
			for ( int j=i+1; j<sizeOfTeam; j++ ) 
			{ 
			    nextBC = player[0][j]; 
			    if(bC.distance( nextBC )< visibleRange )
				{ 
				    //System.out.println("BBC-uA-i= " + i + ", currenttime= " + currentTime); 
			        vA[i].add( new BoardComponent(nextBC) ); 	
					vA[j].add( new BoardComponent(bC) ); 
					//System.out.println("BBC-ua-fini adding");
				}
		    } // end for j 
		} // end for i 
		// now do the donor lists 
		for (int  i=0; i< sizeOfTeam; i++ ) 
		{ 
		    bC = player[0][i]; 
			// now check the distances between agent i and each donor
			for ( int j=0; j< currentDonorList.size(); j++ ) 
			{ 
			    donor = (Donor) currentDonorList.retrieve(j); 
				//System.out.println("BBC-uA-donor is " + donor.toString()); 
				if ( bC.distance( donor ) < visibleRange ) 
				{    
					vD[i].add( new Donor(donor) ); 
					//System.out.println("    BBC-uABC- yes, donor " + j + " is in range"); 
				}   
	       } // end for j 
           // now do the blood banks 
		   for ( int j=0; j< currentBloodBankList.size(); j++ ) 
		   { 
		       bloodBank = (BloodBank) currentBloodBankList.retrieve(j); 
			   //System.out.println("BBC-uA blood bank is " + bloodBank.toString() ); 
		       if ( bC.distance( bloodBank ) < visibleRange ) 
			       vB[i].add( new BloodBank(bloodBank));
		   } // end for j
       } // end for i
	   // now place the time remaining, visible list, locations and blood loads  in their respective players
	  for ( int i=0; i<sizeOfTeam; i++) 
	  {    
	      ((BloodBankAgent) player[0][i]).setAgentTimeRemaining( timeRemaining );
		  ((BloodBankAgent) player[0][i]).setVisibleAgents( vA[i] );
	      ((BloodBankAgent) player[0][i]).setVisibleDonors( vD[i] );
		  ((BloodBankAgent) player[0][i]).setVisibleBloodBanks( vB[i] );	  
		  ((BloodBankAgent) player[0][i]).setBloodLoad( currentBloodLoadList[i]  );
	  } // end for i 
  } // end update all board components
 	
 	/* 
 	** implements abstract method
 	**/
 	public String[] getCurrentRoundStr()
    {
        return roundStringLabel; 
    } // end get current round str   
	 
	
	protected void setGameLabel( int repetition, int round, int game, int[] contestantId )
    {
	    // still need to write the body of this. 
	    
	    gameStr = null; 
		gameStr = " " + repetition + ", " + round + ", " + game + " "; 
		roundStringLabel = null;
	    roundStringLabel = new String[9]; 
		roundStringLabel[0] = "" + "Repetition " + (repetition+1); 
	    //+1) + ", round " + (round+1) + ", Game " + (game+1); 
		roundStringLabel[1] = "Round " + (round+1); 
		//+ ", Game " + (game+1); 
		roundStringLabel[2] = "Game " + (game+1); 
		Contestant c = contestantList[contestantId[0]]; 
		roundStringLabel[3] = c.getLastName();  
		roundStringLabel[4] = " "; 
		//c = contestantList[contestantId[1]]; 
		roundStringLabel[5] = " "; //c.getLastName();  
		roundStringLabel[6] = " "; 
		roundStringLabel[7] = " "; 
		roundStringLabel[8] = " "; 
		//	System.out.println("CTFc-sGL-" + roundStringLabel[0]); 
		//System.out.println("         " + roundStringLabel[1]); 
		//System.out.println("         " + roundStringLabel[2]); 
		//System.out.println("         " + roundStringLabel[3]); 
		//System.out.println("         " + roundStringLabel[4]); 
	} // end set game label 
			
    /* 
    ** implements abstract method 
    **/ 
    public String getScore1()
	{ 
		String str = "Collected"; 
		return str; 
	} // end get score 1 
	
	/* 
	** implements abstract method 
	**/ 
	public String getScore2()
	{ 
		String str = "Time "; 
		return str; 
	} // end get score 2 
	
    /* 
    ** implements abstract method 
    **/ 
	public String getScore3()
	{  
		String str = "Possible "; 
		return str; 
	} // end get score 3 
	
	/* 
	 ** implements abstract method 
	 **/ 	
	public LinkedList<String> getIndividualGameList() 
	{ 
	    return individualGameList;
	} 

   /* 
   ** implements abstract method
   ** But, nothing to do in bloodBank 
   **/ 	
   public void setClockParameters()
   { 
     // nothing to do 
    } // end set clock parameters 
     
 
 /* 
   ** implements abstract method
   ** But, nothing to do in bloodBank 
   **/ 	
    public void setShiftFactors( int repNum )
    { 
    	
    } // end set shift factors 
 
	private void updateBloodLoads() 
	{
	   	// update agents in range of donors 
		BoardComponent agentBC;
		Donor donor;
		int radius;
		for ( int i=0; i< sizeOfTeam; i++ ) 
		{ 
		    agentBC =  player[0][i];  	
			// update the donors 
			for ( int j=0; j< currentDonorList.size(); j++ ) 
			{ 
			    donor = (Donor) currentDonorList.retrieve(j); 
				//System.out.println("BBC-uB- agent= " + agentBC.toString() ); 
		        //System.out.println("BBC-uB-donor= " + donor.toString() ); 
	            //System.out.println();
				radius = donor.getBloodLoad(); //getRadius(); 
				if ( (radius>1) && (agentBC.distance( donor ) < radius)) 
				{ 
				    if ( currentBloodLoadList[i] < BloodBankAgent.MY_CAPACITY ) 
				    { 
					    //System.out.println("BBC-uBL-got a donor's blood"); 
						donor.decrementBloodLoad(); 
						currentBloodLoadList[i]++; 
					} // end if has capacity 
				} // end in donor range and has blood
			} // end for j
			// now update blood banks 
			BloodBank bloodBank; 
			int sC; 
			allBloodBanksFull = true; 
			for ( int j=0; j< currentBloodBankList.size(); j++ ) 
			{  
				bloodBank = (BloodBank) currentBloodBankList.retrieve(j); 
				radius = bloodBank.getRadius(); 
				if ((bloodBank.getBloodLoad() < bloodBank.getCapacity() ) && (agentBC.distance( bloodBank ) < 5 )) //radius)) 
				{ 
				    sC = bloodBank.getSpareCapacity(); 
					//System.out.println("BBC-uB- a blood bank is " + bloodBank.toString() + ", spare capacity= " + sC); 
					if ( sC < currentBloodLoadList[i] ) 
					{ 
					    currentBloodLoadList[i] -= sC; 
						bloodBank.increaseBloodLoad( sC ); 
					}
					else 
					{
					    bloodBank.increaseBloodLoad( currentBloodLoadList[i] ); 
						currentBloodLoadList[i] = 0; 
					}
					//System.out.println("BBC-uBL-got to a blood bank, load= " + bloodBank.toString() ); //getBloodLoad() ); 
				} // end if blood bank has aspair capacity and in range			/if ( currentBloodLoadList[i] < BloodBankagent.MY_CAPACITY )
				allBloodBanksFull = (allBloodBanksFull && (bloodBank.getBloodLoad() == bloodBank.getCapacity())); 
		    } // end for j
		} // end for i 
		//System.out.println("BBC-uBL- finished call to update blood loads");
	} // end updateblood loads  
	 
	/* 
	** provides access to method in super class from subclass
	**/ 
	protected void playGame() 
	{
		super.playGame(); 
	} // end play game 
    
    /* 
    ** implements abstract method 
    **/   
    public void initialiseGame( int repNo, int round, int game, int[] contestantId ) 
    { 
        //System.out.println("BBC-iABCL- start"); 
		setGameLabel( repNo, round, game, contestantId ); 
		BoardComponent bC; 
		double[] xY, xYBase; 
		for (int i=0; i< sizeOfTeam; i++ ) 
		{
		    bC = agentBCList[repNo].retrieve(i);  
			//System.out.println("BBC-ig-bC= " + bC.toString() ); 
			xYBase = bC.getCentrePoint();  
			xY = null; 
		    xY = new double[2]; 
			xY[0] = xYBase[0];  
			xY[1] = xYBase[1];  
			int orientationIndex = ((int) (Math.random()*1000))%BoardComponent.NUMBER_OF_INC;
			player[0][i].initialise( xY[0], xY[1], "triangle", orientationIndex ); ; 
			player[0][i].setColour( "blue" ); 
			//System.out.println("BBC-iG-agentShape= " + player[0][i].getShape() ); ; 
		} // end for i 
		//System.out.println("BBC-iG-exiting"); 
		int rNum = repNo; 
		//System.out.println("BBC-iG-repNo = " + repNo + ", sizeOfTeam= " + sizeOfTeam ); 
        // now reset the donors 
        currentBloodLoadList = null; 
	    currentBloodLoadList = new int[sizeOfTeam]; 
	    currentDonorList.clear(); 
		//System.out.println("BBC-iG-after clear, currentDonor size is " + currentDonorList.size() ); 
		int totalLoad = 0;
		for ( int i=0; i< donorBCList[rNum].size(); i++ ) 
		{
		    currentDonorList.add( new Donor(donorBCList[rNum].get(i)) );
			totalLoad += ((Donor) currentDonorList.retrieve(i)).getBloodLoad(); 
		} 
		//System.out.println("BBC-iG-after add currentDonorList size is " + currentDonorList.size() ); 
		// now reset the current blood bank list 
		totalLoad -= currentDonorList.size(); 
		int modCount = totalLoad % bloodBankBCList[rNum].size();
		totalLoad /= bloodBankBCList[rNum].size();
		currentBloodBankList.clear(); 
		//System.out.println("BBC-iG-length bloodBankBCList= " + bloodBankBCList.length + ", rNum= " + rNum ); 
		for ( int i=0; i< bloodBankBCList[rNum].size(); i++ ) 
		    if ( i < modCount ) 
		    	currentBloodBankList.add( new BloodBank(bloodBankBCList[rNum].get(i), (totalLoad+1) ));
		    else 
		        currentBloodBankList.add( new BloodBank(bloodBankBCList[rNum].get(i), totalLoad ));
		    
		    allBloodBanksFull = false; 
		    //System.out.println("BBC-iABCL-gt =t the blood banks");
	    } // end initialise game 

    /* 
    ** implements abstract method 
    **/ 
    public void updateCurrentContestants( int repNum, int round, int game, int[] id)  
   	{  
   	    // first collect data on contestant's performance on this game
   	    int actual = 0; 
		int time = 0; 
		int possible = 0;
		for ( int i=0; i< currentBloodBankList.size(); i++ ) 
		{    
		    actual += ((BloodBank) currentBloodBankList.retrieve(i)).getBloodLoad();
			time += (totalTime-timeRemaining); 
			possible += ((BloodBank) currentBloodBankList.retrieve(i)).getCapacity();
		}
	    //System.out.println("BBC-uCC-actual= " + actual + ", time= " + time + ", possible= " + possible ); 
   	    //System.out.println("BBC-uCC-id= " + id[0]  + ", round= " + round + ", repNum= " + repNum); 
   	    ((BloodBankContestant) contestantList[id[0]]).updateRoundScores( repNum, round, game, actual, time, possible); 
		// aaa 
		 //System.out.println("BBC-uCC-id= " + id[0] ); 
   	  
		//System.out.println("BBC-uCC contestant= " + ((BloodBankContestant) contestantList[id[0]]).toString() ); 
		//int[][] sol = ((BloodBankContestant) contestantList[id[0]]).getCityScores(); 
		// System.out.println("BBC-uCC-id= " + id[0] ); 
   	  
		// bbb
		insertInOrder( contestantList[id[0]], orderedContestantList );	
		finishGameLabel(id );
		String str = " "; 
		str += contestantList[id[0]].getLastName();   
        str += " " + ((BloodBankContestant) contestantList[id[0]]).toString(); 
        individualGameList.add( str ); 
        //System.out.println("BBC-uCC-this individual game list is " + str);  
	} // end update curre	
	
	// aaa 
	
    public void extendGameLabel( int[] contestantId ) 
    {	
	    Contestant c = contestantList[contestantId[0]]; 
		roundStringLabel[3] = c.getLastName();  
		roundStringLabel[4] = " ";  
		roundStringLabel[5] = " ";   
	} // end extend game label 
	
	protected void finishGameLabel( int[] contestantId )
    {
    	//System.out.println("BBC-fCC-exiting");  
    	roundStringLabel[6] = null;  
		roundStringLabel[7] = null;  
		roundStringLabel[8] = null; 
		Contestant c = contestantList[contestantId[0]]; 
		roundStringLabel[6] =  c.getLastName(); 
		roundStringLabel[7] = "Collected= " + c.getFirstComponent(); 
		roundStringLabel[8] = "Time= " +c.getSecondComponent();  
		// now set the game str for the individual  game list  
		gameStr =  c.getLastName(); 
		gameStr += gameDescription; 
		// print the roundStringlabel to ouput 
		//for ( int i=0; i< roundStringLabel.length; i++ ) 
		//	System.out.println("  i= " + i + ", " + roundStringLabel[i] ); 
		//System.out.println(" BBC-sRSL-exiting"); 
	} // end finish game  label 
	
	/* 
	** implements abstract method 
	**/
	public boolean isGameFinished()
	{ 
	   boolean finished = (( timeRemaining <= 0)||(allBloodBanksFull));
	    return finished; 
	} // end is game finished 
	
	/* 
	** implements abstract method 
	**/ 		
	public void printAllScores() 
	{
	   	BloodBankContestant c; 
		for ( int i=0; i<orderedContestantList.size(); i++ )  
		{ 
			c= (BloodBankContestant) orderedContestantList.get(i); 
			System.out.println(c.toString() ); 	
			System.out.println(); 
		}  // end for i 
	} // end print all scores  
 
	/* 
	** implements abstract method 
	**/ 
	public void resultsToFile() 
	{ 
	    String str;
	    BloodBankContestant c; 
	    try 
		{ 
		    PrintWriter writer = new PrintWriter(basePathOutput+"tournamentResults.txt");
			writer.println("Rank    Sn     BloodTotal    timeTotal      PossibleTotal");
		    int rank = 1;
			for ( int i=0; i< orderedContestantList.size(); i++ ) 
			{    
			    str = rank++ + "      "; 
			    c =(BloodBankContestant) orderedContestantList.get(i); 
			    str += c.getIdNo(); 
			    str += "   " + c.getTotalActual(); 
			    str += "            " + c.getTotalTime(); 
			    str += "           " + c.getTotalPossible(); 			
				writer.println( str );
			    //System.out.println( bBC.orderedContestantList.get(i).toString() ); 
			}  // end for i 
			// now write the individual city results for each contestant in order 
			writer.println(  );
			str = null; 
			str = "   Individual Results for Each City"; 
			writer.println( str );
			writer.println(  );
			int[][][] rS; 
			for ( int i=0; i< orderedContestantList.size(); i++ ) 
			{    
			    str = null;
			    //str = rank++ + "      "; 
			    c =(BloodBankContestant) orderedContestantList.get(i); 
			    str = c.getIdNo(); 
			    writer.println( str );
			    rS = c.getRoundScores(); 
			    for ( int j=0; j< rS.length; j++ ) 
			    {
			        str = null; 
			        str = "    " + cityList.get(j);  
			        str += "   " + rS[j][0][0];  
			        str += "            " + rS[j][0][1];  
			        str += "           " + rS[j][0][2];  			
				    writer.println( str ); 
			    } // end for j 
			    writer.println(  );
			 
			} // end for i 
			
			writer.close();
		} 
		catch( IOException e ) {}
		
	} // end results to file 
   
	private void resetContestantList() 
	{
		orderedContestantList = null;  
		orderedContestantList = new LinkedList<Contestant>(); 
        for ( int i=0;i< contestantList.length; i++ ) 
        {    
        	contestantList[i] = new BloodBankContestant( contestantList[i]);
		    orderedContestantList.add( contestantList[i] ); 
        } // end for i 
    } // end re 
    
    protected void setTournamentResultsLabel() 
	{ 
		setOverallResultsLabel(); 
	} // end set tournament results label 
	

	static public void main( String[] args ) 
	{ 
	    String[] filename = new String[2]; 
	    filename[0]= "BloodBankContestantNames.txt"; 
	   	filename[1] = "BloodBankCityNames.txt";
		BloodBankController bBC = new BloodBankController( 999, filename ); //agentBCFilename, contestantListFilename ); //"ContestantNames.txt");   
		bBC.runController();
		//bBC.resultsToFile(); 
	} // end main   	

} // end BloodBankController class

