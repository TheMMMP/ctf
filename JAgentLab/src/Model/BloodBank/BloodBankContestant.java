package src.Model.BloodBank;

/* 
* BloodBankContestant a class to extend the Contestant class for an agent in Blood Bank tournament. 
* @author Phil Sheridan
* @version  2/3/15
*/ 

import java.util.LinkedList; 
import java.util.Iterator;  
import src.Model.*; 

public class BloodBankContestant extends Contestant 
{ 
    private int totalPossible; 
	private int totalActual; 
	private int totalTime; 
		
	/* 
	* construct this with personal details of specified parameter
	* @param c, Contestant to be copied. 
	* @postcondition: idNo, firstN and lastN are that of c. 
	*/
	public BloodBankContestant( Contestant c ) 
	{ 
	    super( c ); 
	    totalActual = 0; 
	    totalPossible = 0; 
	    totalTime = 0;  
    } // end constructor 
	   
	  /* 
	  ** implements the compareTo method from Contestant class 
	  ** @param other, Object to be compared with this. 
	  ** @return 1 if this totalPoints > other, -1 if <, 0 otherwise. 
	  *@precondition specified parameter is a BloodBankContestant. 
	  **/  
	  public int compareTo( Object other ) 
	  { 
	      BloodBankContestant c = (BloodBankContestant) other; 
		  if ( this.totalActual < c.totalActual ) 
		      return -1; 
		   else if ( this.totalActual > c.totalActual ) 
		      return 1; 
		   else if ( this.totalTime < c.totalTime ) 
		      return 1; 
		   else if ( this.totalTime > c.totalTime ) 
		      return -1; 
	      else // it is a draw  
	          return 0; 
    } // end compare to 
    		
	/* 
	* updates a round score with specified ones 
	* @param repetition, int that represents the repetition in question
	* @paran round, int representing the round number in question 
	* @param actual, the actual number of units achieved. 
	* @param time, the amount of time taken to complete the round. 
	* @paran possible, the possible number of units achievable 
 
	*/ 
	public void updateRoundScores( int repetition, int round, int game, int actual, int time, int possible ) 
	{ 
	    super.updateRoundScores( repetition, round, actual, time, possible ); 
	    //System.out.println("BBCon-uRS-after call to super"); 
	    //int[][] sk = getCityScores(); 
	    //System.out.println("BBContest- xxx");
	    if ( ( repetition >=0) && (repetition<roundScores.length) && (round>=0) && ( round< roundScores[0].length)) 
	    {
	    	totalActual = 0; 
	        totalTime = 0; 
	        totalPossible = 0; 
	        for ( int i= 0; i< roundScores.length; i++ ) 
			    for ( int j=0; j< roundScores[0].length; j++) 
			    {    
			    	totalActual += roundScores[i][j][0]; 
			    	totalTime += roundScores[i][j][1]; 
			    	totalPossible += roundScores[i][j][2]; 
			      } // end for j    
	    } // end if 
	    else 
		    System.out.println("BBContestant-uR- Problem-repetition= " + repetition + ", round " + round + " is out of range");
	} // end update round scores 
	
	public String toString() 
	{ 
	    String str = super.toString(); 	
		str += ", Collected= " + totalActual; 
	    str += ", Time= " + totalTime; 
		str += ", Possible= " + totalPossible; 
		return str;
	} // end to string 
	

    
	
	public int getTotalActual() 
	{ 
	    return totalActual;  
	} 

    public int getTotalTime() 
    { 
    	return totalTime; 
    }
    
    public int getTotalPossible() 
    { 
    	return totalPossible; 
    }
    
    public String getCurrentState() 
    { 
        String  str = "actual= " + totalActual; 
        str += ", time= " + totalTime; 
        str += ", possible= " + totalPossible ; 
        return str; 	
    } // end get current state 
    
} // end BloodBankContestant class
