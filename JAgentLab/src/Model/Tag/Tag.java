/*
* Taga.java, a class to run the the game of tag. 
* @author Phil Sheridan 
* @version 27/2/14
*/
public class Tag
{
    static public void main( String[] args ) 
	  { 
	      String agentBCFilename = "tagBoardComponent.txt"; 
		    String contestantListFilename = "TagContestantNames.txt"; 
		    DrawableTagController dC = new DrawableTagController( 4000, agentBCFilename, contestantListFilename ); //"ContestantNames.txt"); //, "CityNames.txt", 100 ); 
	      ControllerCanvas.createController( dC ); 
		    dC.runController(); 
	  } // end main 
} // end Tag class

