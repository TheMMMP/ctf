package src.Model.Tag; 
/*
* TagAgent.java  extends Agent class
* @author Phil Sheridan 
* @version 26/2/14
*/ 

import src.Model.*; 

public abstract class TagAgent extends Agent
{
    // instance variables 
	protected boolean iAmIt; 
	protected BoardComponentList visibleAgents; 
	
	/* 
	* constructs this from specified parameter
	*/ 
    public TagAgent( BoardComponent bC) 
	{ 
	    super( bC);
		visibleAgents = new BoardComponentList(); 
		iAmIt = false;
	} // end constructor 

    /* 
    * constructs default TagAgent 
    */ 
    public TagAgent() 
    { 
        super();
		visibleAgents = new BoardComponentList(); 
		iAmIt = false; 
    } // end constructor  
		
	/* 
	* set the current list of visible agents from specified parameter 
	* @param vAL, the BoardComponentList containing the visible agents
	*/ 
	public void setVisibleAgents( BoardComponentList vA) 
	{ 
	    visibleAgents = null; 
		visibleAgents = vA; 
	} // end set visible agents 
		
	/* 
	* set the value of vvariable iAmIt to specified one
	*/ 
	public void setIAmIt( boolean iAmIt ) 
	{ 
	    this.iAmIt = iAmIt; 
	} 
		
	/* 
	* determines whether this is it or not 
	*/ 
	public boolean isIt() 
	{ 
	    return iAmIt; 
	}
		
} // end TagAgent class

