package src.Model.Tag; 

/*
* TagController.java 
* @author Phil Sheridan 
* @version 18/2/15
*/

import java.io.*; 
import java.util.Scanner; 
import java.util.HashMap; 
import java.util.LinkedList; 
import java.util.Iterator; 
import src.Model.*;

public class TagController extends Controller
{ 
    //static private double visibleRange = 50; 
	//static final public double TAG_RANGE = 2;
  	static final int HEAD_START = 50; 
	static private double visibleRange = 60; 
	static private double tagRange = 20;
  			
    // instance variables 
	protected int it; 
	protected int getAwayTime; 
    private int prevCatchTime; 
	private int catchTime; 
	protected double[][] shiftAgent; 	
	protected String[] colour; 
	private int[] tab; 
		
	/* 
	* constructs this 
	*/ 
	public TagController( int totalTime, String[] filename, boolean randomise ) 
	{ 
	    super( totalTime, filename[0], filename[1], "TagAgent", 2, randomise, false ); 
	    title = "Tag";
		resetContestantList(); 
		String cityName, agentFilename; 
		agentBCList = new BoardComponentList[cityList.size()]; 
		for ( int i =0; i< cityList.size(); i++ ) 
		{
		    cityName = null;
			cityName = cityList.get(i); 
			//System.out.println("CTFC-c- cityName = " + cityName ); 
			agentFilename = cityName + "AgentBC.txt"; 
			agentBCList[i] = readBoardComponents( agentFilename, "triangle" );  
			//System.out.println("TC-c-size= " + agentBCList[i].size() ); 
			// next line prints agent location to standard output, validation only  
		    printBCList( agentBCList[i], agentFilename);
		} // end for i   
		setAlgorithmList(); 
		
		tab = new int[2]; 
		tab[0] = 0;        // tab for team 0
		tab[1] = (MAX_X/2); // tab for team 1
		shiftAgent = new double[numberOfTeams][2]; 
		colour = new String[2]; 
		colour[0] = "blue"; 
		colour[1] = "red"; 
		roundStringLabel = new String[3]; 
		gamePlayer = new String[numberOfRepetitions][numberOfRounds][numberOfGames][2]; 
		gameResult = new int[numberOfRepetitions][numberOfRounds][numberOfGames]; 
		individualGameList = new LinkedList<String>(); 
		getAwayTime = HEAD_START; //GET_AWAY_THRESHOLD;
		it = 0; //change this to a random allocation 
		prevCatchTime = 0; 
		catchTime = 0; 
	}// end constructor
 
	
	private BoardComponentList readBoardComponents( String filename, String shape ) 
	{ 
	    BoardComponentList list; 
		try 
		{ 
		    list = boardComponentsFromFile( filename, shape );
		} 
		catch( FileNotFoundException e ) 
		{
		    //System.out.println("BBC-rC- " + e.toString()); 
			list = new BoardComponentList(); 
		}
		return list; 
	} // end read components 

   /* 
   ** implements abstract method that imposes the restriction for Tag: 
    ** the it agent speed is 1.0 and the non-it agent is 0.9. 
    * @postconditions: speeds of each agent is set to .9 for it and 1.0 for other. 
    **/ 
 	public void imposeGameRestrictions() 
 	{ 
 	    player[(it+1)%2][0].setAgentSpeedTo( 0.9); 
 		if ( getAwayTime <= HEAD_START ) 
 		    player[it][0].setAgentSpeedTo( 0.0 ); 
 		else 
 		    player[it][0].setAgentSpeedTo( 1.0 ); 
 	}	 // end impose game restrictions 
	
	/* 
	* updates all player's BoardComponent objects
	*/
	public void updateAllBoardComponents() 
	{ 
		// now update whose it
		updateIt();  			
		//  now update each player's visibility
	    BoardComponent bC, nextBC; 
		BoardComponentList[][]  vOtherA = new BoardComponentList[numberOfTeams][sizeOfTeam]; 		
       	double[] xYIt, xYOt; 
       	double speed; 
       	for ( int t=0; t< numberOfTeams; t++ ) 
		{    
		    for ( int i=0; i< sizeOfTeam; i++ ) 
			{  
			    vOtherA[t][i] = new BoardComponentList();	
			} // end for i
	    } // end for t 
		// now collect visible objects 
		int tOther; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    int i;
			for ( i=0; i< (sizeOfTeam); i++ ) 
			{ 
			    bC = player[t][i]; 
				// now check the distances between each agent and it successor	
				// now do the opponents 
				if ( t==0) 
				{ 
				    for ( int j=0; j< sizeOfTeam; j++ ) 
			        { 
					    nextBC = player[1][j]; 
			            if(bC.distance( nextBC )< visibleRange )
					    { 
						    vOtherA[0][i].add( new BoardComponent(nextBC) ); 
							vOtherA[1][j].add( new BoardComponent(bC) ); 
				        } // end if 
		            } // end for js
				} // end if t is 0 	 	  
			} // end for i 
	    } //  end for t 
		// now set the visible agents to players
		for ( int t=0; t< numberOfTeams; t++ ) 
		{    
		    for ( int i=0; i<sizeOfTeam; i++ ) 
		    {    
			    ((TagAgent) player[t][i]).setVisibleAgents( vOtherA[t][i] );      
		    } // end for i 
		} // end for t 
    } // end update all board components
   	
   	private void updateIt() 
	{ 
	    boolean itHasChanged = false;
	    //System.out.println("TC-uI- getAwayTime = "  + getAwayTime +  ", headStart= " + HEAD_START ); 
	    if ( getAwayTime > HEAD_START ) 
	    { 
		    for ( int t=0; t<player.length; t++ ) 
		    { 
		        if ( t != it )
			    { 
				    if (((TagAgent) player[t][0]).distance( player[it][0] ) < tagRange ) 
				    { 
					    itHasChanged = true; 
					    getAwayTime = 0;
						prevCatchTime = catchTime; 
						catchTime = timeRemaining; 
						//System.out.println("TC-uI- it has changed to " + it +", in " + (catchTime-prevCatchTime));    
					} // end in tag range 
				} // end if i is not it 
			} // end for t 
			if ( itHasChanged ) 
			{ 
			    int[] id = new int[2]; 
			    id[1] = 1; 
			    updateCurrentContestants( 0, 0, 0, id ); 
				((TagAgent) player[it][0]).setIAmIt( false ); 
			    it = (it+1)%2; 
				((TagAgent) player[it][0]).setIAmIt( true ); 
			} // end if has changed  
	    } // end if get away time . head start time 
		getAwayTime++; 
	} // end updateIt   
	  
	  	
	public void setClockParameters() 
	{ 
        // abstract method but nothing to do in Tag  
	} // end set clock parameters 
	
    /* 
     ** implements abstract method 
     **/ 
    public void updateCurrentContestants( int repNum, int round, int game, int[] id) 
	{ 
		 gamePlayer[repNum][round][game][0] = contestantList[id[0]].getLastName(); 
		 gamePlayer[repNum][round][game][1] = contestantList[id[1]].getLastName(); 
		 String str = " "; 
		 str += contestantList[id[0]].getLastName(); 
		 str += " vs "; 
		 str += contestantList[id[1]].getLastName(); 	      	
		((GenericContestant) contestantList[id[it]]).incrementRoundScores(  repNum, round, 0, 1, 0 ); //a tag 
		((GenericContestant) contestantList[id[(it+1)%2]]).incrementRoundScores( repNum, round, catchTime, 0, 0 ); //amount of time in tag 
		int comp = ((GenericContestant) contestantList[id[it]]).compareTo( ((GenericContestant) contestantList[id[(it+1)%2]]) ); 
	    int winner = -1;  
	    if ( comp > 0 ) 
	    	winner = it; 
	    else if ( comp < 0 ) 
	        winner = (it+1)%2; 
		finishGameLabel( repNum, round, game, id , winner);
  	
		if ( winner > -1 ) 
		{	    
			str += ": Winner is " + contestantList[id[winner]].getLastName(); 
		    if ( winner == 0 ) 
			    str += " (Blue) "; 
			else 
			    str += " (Red) "; 
		}
		else // game is a draw 
		{
		    	str += ": result is draw"; 
		}
		gameResult[repNum][round][game] = winner; 
		str += gameDescription;  
        individualGameList.add( str ); 
		//insertionSort(); 
		String[] l = getCurrentRoundStr(); 
		//System.out.println("TC-uCC- "  + l[0] + ", " + l[4] );
	} // end update current contestants 
	
	// aaa 
	
	protected void finishGameLabel( int repetition, int round, int game, int[] contestantId , int winner)
    {
	    roundStringLabel[6] = "Result"; 
		if ( winner > -1 ) 
		{ 
		    //+ c.getLastName(); 
			roundStringLabel[7] = null;
			roundStringLabel[7] = "Winner is ";  
			Contestant c = contestantList[contestantId[winner]]; 
			roundStringLabel[8] = null;
			roundStringLabel[8] =  c.getLastName(); 
			
			gameStr += "win: " ;
			gameStr =  c.getLastName(); 
			c = contestantList[contestantId[(winner+1)%2]]; 
			gameStr += ", loss: ";
			gameStr  +=  c.getLastName(); 
		} 
		else 
		{    
		    roundStringLabel[7] = null; 
			roundStringLabel[8] = null; 
			roundStringLabel[7] = "No winner"; 
			roundStringLabel[8] = "Draw"; 
			Contestant c = contestantList[contestantId[0]]; 
			gameStr =  c.getLastName(); 
			c = contestantList[contestantId[1]]; 
			gameStr += "vs "; 
			gameStr  +=  c.getLastName(); 
		}
		gameStr += gameDescription; 
	} // end finish game  label 

   
	protected void setTournamentResultsLabel() 
	{ 
		setOverallResultsLabel(); 
	} // end set tournament results label 
	

	/* 
	 ** implements abstract method 
	 **/ 
	public void initialiseGame( int repNum, int round, int game, int[] contestantId ) 
	{ 
	    setGameLabel( repNum, round, game, contestantId ); 
	    String[] l = getCurrentRoundStr(); 
	    BoardComponent bC; 
		double[] xY, xYBase; 
		int orientationIndex; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    // now set agent centre points and colours
			//System.out.println("T-iT t= " + t);
			for (int i=0; i< sizeOfTeam; i++ ) 
			{
			    bC = agentBCList[repNum].retrieve(i);  
			    xYBase = bC.getCentrePoint();  
				xY = null; 
			    xY = new double[2]; 
				if ( t==0) 
				{ 
				    xY[0] = xYBase[0]; 
					xY[1] = xYBase[1]; 
				}
				else 
				{ 
				    xY[0] = shiftAgent[t][0] - xYBase[0];  
					xY[1] = shiftAgent[t][1] - xYBase[1];
					//System.out.println("TC-iG" + xY[0] + ", " + xY[1]); 
					//System.out.println("TC-ig shift= " + shiftAgent[t][0] + ", base= " +  xYBase[0]) ;  		
				 }
				 //System.out.println("CTF-iT-t= " + t + ", i= " + i); 
				 //orientationIndex = 0; 
				 orientationIndex = ((int) (Math.random()*1000))%BoardComponent.NUMBER_OF_INC;
				 player[t][i].initialise( xY[0], xY[1], "triangle", orientationIndex ); ; 
				 player[t][i].setColour( colour[t] ); 
				 //System.out.println("TC-iG-agent= " + player[t][i].toString() ); 		
			 } // end for i 
		 } // end for t  
		 timeRemaining = totalTime; 
		 catchTime = timeRemaining; 
		 getAwayTime = (HEAD_START+1);
		 it = 0; 
		 ((TagAgent) player[it][0]).setIAmIt( true ); 
		 	//.initialise( xY[0], xY[1], "triangle", orientationIndex ); ; 
			  
	 } // end initialise game 

   public boolean isGameFinished() 
	{ 
	    boolean finished = (timeRemaining <= 0);     
	    return finished; 
	} 

	public void setShiftFactors( int repNum ) 
	{ 
	    shiftAgent[1][0] = MAX_X; 
	    shiftAgent[1][1] = MAX_Y;
	} // end set shift factors 

	public void printAllScores() 
	{
	    int w, l, d;
		String lastName;
		int[][] scores; 
		for ( int i=0; i< contestantList.length; i++ ) 
	    { 
		    //System.out.println(" contestant is " + i ); 
			lastName = contestantList[i].getLastName(); 
			//System.out.println("----------Final result--------"); 
		} // end for i  
	} // end print all scores  
    
    public void extendGameLabel( int[] contestantId ) 
    {	
	    Contestant c = contestantList[contestantId[0]]; 
		roundStringLabel[3] = c.getLastName();  
		roundStringLabel[4] = "vs"; 
		c = contestantList[contestantId[1]]; 
		roundStringLabel[5] = c.getLastName();  
	} // end extend  game label 
 
	
	public void resultsToFile() 
	{  
	   // abstract method, but nothing to do in tag 
	} // end results to file 
	
	public String[] getCurrentRoundStr() 
	{ 
		return roundStringLabel; 
    } // end get current round 

    public String getScore1() 
		{ 
		    return "Time";
		} 
		
		public String getScore2() 
		{ 
		    return "Tags"; 
		} 
		
		public String getScore3() 
		{ 
		    return " ";
		} 
		
		 
		private void insertionSort() 
		{ 
		    /* 
		    CTFContestant[] list = new CTFContestant[orderedContestantList.size()]; 
			CTFContestant current, hold; 	
			// now place all contestants into list 
			for ( int i=0; i< orderedContestantList.size(); i++ ) 
			{ 
			    current = (CTFContestant) orderedContestantList.get(i); 
				list[i] = current; 
			} // end for i 
			// now order list 
			boolean swapped;
			for ( int i=1; i< list.length; i++ ) 
			{ 
			    hold = list[i]; 
				int j= (i-1); 
				swapped = false; 
				while ((j>= 0) && ( hold.getNumberOfPoints() > list[j].getNumberOfPoints() )) 
				{    
				    list[j+1] = list[j]; 
					swapped = true; 
					j--; 
				} // end while 
				if ( swapped ) 
				    list[j+1] = hold; 					
			} // end for i 
			// now put the ordered contestents back into ordered contestant lis 
			orderedContestantList = null; 
			orderedContestantList = new LinkedList<Contestant>(); 
			for ( int i=0; i< list.length; i++ ) 
			    orderedContestantList.add( list[i] ); 
			*/	
		} // end insertion sort 
		 
			
		public LinkedList<String> getIndividualGameList() 
		{ 
		    return individualGameList;
		} 
		
 
	private void resetContestantList() 
	{
		orderedContestantList = null;  
		orderedContestantList = new LinkedList<Contestant>(); 
        for ( int i=0;i< contestantList.length; i++ ) 
        {    
        	contestantList[i] = new GenericContestant( contestantList[i]);
		    orderedContestantList.add( contestantList[i] ); 
        } // end for i 
    } // end reset contestantList

		
		
	static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // should be kept to false when testing, otherwise randomises red and blue 
	    int gameDuration = 499; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "TagContestantNames.txt";  
	    filename[1] = "TagCityNames.txt"; 
		TagController bBC = new TagController( gameDuration, filename, randomise );   
		bBC.runController(); 
	} // end main   	

} // end TagController class

