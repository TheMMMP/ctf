package src.Model.CTF; 

/*
* CTFAgent.java  extends Agent class
* and encapsulates an agent of Capture The Flag 
* @author Phil Sheridan 
* @version 9/5/14
*/ 
import src.Model.*; 

public abstract class CTFAgent extends Agent
{
   		
    // instance variables 
	private BoardComponentList visibleTeamAgents; 
	private BoardComponentList visibleOtherAgents; 
	private BoardComponent visibleTeamFlag; 
    private BoardComponent visibleOtherFlag; 
	private BoardComponent visibleTeamJail; 
	private BoardComponent visibleOtherJail; 
	private boolean isInJail; 
	private boolean iAmHoldingFlag; 
		
	/* 
	* constructs this from specified parameter
	* @param bC, a BoardComponent object that specifies agent's location
	*/ 
    public CTFAgent( BoardComponent bC) 
	{ 
	    super( bC);
	    visibleTeamAgents = new BoardComponentList(); 
		visibleOtherAgents = new BoardComponentList(); 
		isInJail = false;  		
	} // end constructor 

    /* 
    * constructs default CTFAgent 
    */ 
    public CTFAgent() 
    { 
        super(); 
		visibleTeamAgents = new BoardComponentList();  
	    visibleOtherAgents = new BoardComponentList(); 
		isInJail = false; 
    } // end constructor  

    public void setIsInJail( boolean bool ) 
    { 
        isInJail = bool;
    } 

    public boolean isInJail() 
    { 
        return isInJail;
    } 

	/* 
	* set the current list of visible team agents from specified parameter 
	* @param vAL, the BoardComponentList containing the visible agents
	*/ 
	public void setMyVisibleAgents( BoardComponentList vA) 
	{ 
	    visibleTeamAgents = null; 
		visibleTeamAgents = vA; 
	} // end set visible team agents 
	
	/* 
	* set the current list of visible other agents from specified parameter 
	* @param vAL, the BoardComponentList containing the visible agents
	*/ 
	public void setOtherVisibleAgents( BoardComponentList vA) 
	{ 
	    visibleOtherAgents = null; 
		visibleOtherAgents = vA; 
	} // end set visible other agents 

    /* 
    * sets visible team flag from specified parameter 
	* @param bC the BoardComponent object associated with team flag 
	*/ 
	public void setMyVisibleFlag( BoardComponentList bCL ) 
	{ 
	    if ( bCL.size() > 0 ) 
		    visibleTeamFlag = bCL.retrieve(0); 
	} 
 
	/* 
    * sets visible other flag from specified parameter 
	* @param bC the BoardComponent object associated with other flag 
	*/ 
	public void setOtherVisibleFlag( BoardComponentList bCL ) 
	{ 
	    if ( bCL.size() > 0 ) 
		    visibleOtherFlag = bCL.retrieve(0); 
	} 
 
	/* 
    * sets visible team jail from specified parameter 
	* @param bC the BoardComponent object associated with team jail 
	*/ 
	public void setMyVisibleJail( BoardComponentList bCL ) 
	{ 
        if( bCL.size() > 0 ) 
		    visibleTeamJail = bCL.retrieve(0); 
	} 
    
    /* 
    * sets visible other jail from specified parameter 
	* @param bC the BoardComponent object associated with other jail 
	*/ 
	public void setOtherVisibleJail( BoardComponentList bCL ) 
	{ 
        if ( bCL.size() > 0 ) 
		    visibleOtherJail = bCL.retrieve(0); 
	} 

    protected BoardComponentList senseTeamAgents() 
	{ 
	    return visibleTeamAgents; 
	} 
		
	protected BoardComponentList senseOtherAgents() 
	{ 
	    return visibleOtherAgents; 
	} 
		
	protected BoardComponent senseTeamFlag() 
	{ 
	    return visibleTeamFlag; 
	} 
		
	protected BoardComponent senseOtherFlag() 
	{ 
	    return visibleOtherFlag; 
	} 

    protected BoardComponent senseTeamJail() 
	{ 
	    return visibleTeamJail;
	} 
		
	protected BoardComponent senseOtherJail() 
	{ 
	    return visibleOtherJail;
    } 

    protected double[] getMyHomeLocation() 
	{ 
	    double[] v = new double[2]; 
		v[0] = 0.0; 
		v[1] = 0.0; 
		double midX = Controller.MAX_X/2; 
		BoardComponent bC = new BoardComponent( midX, xY[1], 10 ); 
		if ( colour.equals("blue") && (xY[0] > midX) ) 
	        v = this.getLocationOf( bC ); 
		else if ( colour.equals("red") && (xY[0] < midX) )
		    v = this.getLocationOf( bC ); 
        return v;
	} // end get my home location 
		
    public void setIAmHoldingFlag( boolean bool ) 
	{ 
	    iAmHoldingFlag = bool; 
    } 
		
    protected boolean iAmHoldingFlag() 
	{ 
	    return iAmHoldingFlag; 
	} 
		
	protected int getTimeRemainingInJailBufferZone() 
	{ 
	    return CTFController.getTimeRemainingInJailBufferZOne( myIdNumber ); 
	} // end get time remaining in jail buffer zone 
		
	public String toString() 
	{ 
	    String str = super.toString();  
		return str; 
	} // end to string 
		
	/*
	static public void main( String[] args ) 
	{ 
	    double x = 20; 
		double y = 10; 
		BoardComponent bC = new BoardComponent( x, y, 10 ); 
		bC.setColour("blue"); 
		S111111CTFAgent a = new S111111CTFAgent( bC );  
		double[] v = a.getMyHomeLocation(); 
		System.out.println("a= " + a.toString() );
		System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] );
		System.out.println("now set agent colour to red"); 
		bC.setColour("red"); 
	    a = new S111111CTFAgent( bC );  
		v = a.getMyHomeLocation(); 
		System.out.println("a= " + a.toString() );
		System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] ); 
		System.out.println("now set agent's x cord to red side"); 
		x = 190; 
		y = 10; 
		bC = new BoardComponent( x, y, 10 ); 
		bC.setColour("red"); 
		a = new S111111CTFAgent( bC );  
		v = a.getMyHomeLocation(); 
		System.out.println("a= " + a.toString() );
		System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] ); 
	    System.out.println("now set agent's colour to blue"); 
		x = 190; 
		y = 10; 
		bC = new BoardComponent( x, y, 10 ); 
		bC.setColour("blue"); 
		a = new S111111CTFAgent( bC );  
		v = a.getMyHomeLocation(); 
		System.out.println("a= " + a.toString() );
		System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] ); 
	
	} // end main
			*/ 	
} // end CTFAgent class

