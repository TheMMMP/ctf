package src.Model.CTF; 

/*
* Flag.java  extends BoardComponent class
* @author Phil Sheridan 
* @version 25/4/14
*/ 

import src.Model.*;

public class Flag extends BoardComponent
{
    // instance variables 
		
	/* 
	* constructs this from specified parameter
	*/ 
    public Flag( BoardComponent bC) 
	  { 
		    super( bC); 
		} // end constructor 

/* 
* constructs default Flag 
*/ 
public Flag() 
{ 
    super();
} // end constructor  

/* 
* decrements this blood by one unit if current > 1 unit
* deprecated 
public void decrementBloodLoad() 
{ 
    if ( xPoints[1] > 1 ) 
		{ 
		    xPoints[1]--; 
		    xPoints[0] = -xPoints[1]/2; 
		    yPoints[1]--; 
		    yPoints[0] = -yPoints[1]/2;
		} // end if has donatable blood 
} // end decrement blood load 
*/ 

/* 
* returns the level of the current blood load 
* deprecagted  
public int getBloodLoad() 
{ 
    return xPoints[1];
} // end get blood load 	
*/ 

/* deprecated 
public int getRadius() 
{ 
    return (int) Math.floor((xPoints[1]/((double) 2.0))+0.5);
} 
*/ 

    public String toString() 
    { 
        String str = "xY is (" + ((int) xY[0]) + ", " + ((int) xY[1]) + ")";
				return str;
    }
} // end Flag class

