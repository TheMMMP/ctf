package src.Model.CTF; 

/*
* CTFController.java 
* @author Phil Sheridan 
* @version 2/3/15
*/

import java.io.*; 
import java.util.Scanner; 
import java.util.HashMap; 
import java.util.LinkedList; 
import java.util.Iterator; 
import src.Model.*;

public class CTFController extends Controller
{ 
    static private double visibleRange = 50; 
	static final public double TAG_RANGE = 2;
  	static final public double BUFFER_ZONE = 10; 
	static final public int MAX_TIME_IN_BUFFER_ZONE = 1 + 2*((int) BUFFER_ZONE); 
	static final double[] outOfBounds = {-100, -100};
	static final public int SIZE_OF_TEAM = 10;  
	static private int jailedRedCount; 
	static private int jailedBlueCount; 
    static private boolean[][][] inJailBufferZone; 
	static private int[][] jailBufferZoneClock; 		
	
		
	// static methods  
	static public int getTimeRemaining() 
	{ 
	    return timeRemaining; 
	}  
		
	/*
	* get the time remaining for the agent associated with the specified parameter
	*  to leave the jail buffer zone before it is sent to jail. 
	* @param agentId, an int representing the unique agent identification number. 
	* @return value between 0 and BUFFER_ZONE
	* @precondition: agentId is valid, otherwise a value of 0 is returned 
	*/ 
	static public int getTimeRemainingInJailBufferZOne( int agentId ) 
	{ 
	    try 
		{ 
		    return jailBufferZoneClock[agentId/SIZE_OF_TEAM][agentId%SIZE_OF_TEAM]; 
		} 
		catch( Exception e ) 
		{
		    return 0; 
		}
	} // end get time remaing in Jail buffer zone 
		
	static public int getJailedRedCount() 
	{ 
	    return jailedRedCount; 
	} 
		
	static public int getJailedBlueCount() 
	{ 
	    return jailedBlueCount; 
    } 
		
				
		
    // instance variables 
	protected BoardComponentList[] flagBCList; 
	protected BoardComponentList[] jailBCList;  
	protected Jail[] jail; 
	protected Flag[] flag; 
	protected double[][] shiftAgent; 
	protected double[][] shiftFlag; 
	protected double[][] shiftJail; 
	protected String[] colour; 
	protected int[] flagPossession; 
	private int[] tab; 
	//private String[] roundStringLabel; 
	//private String gameStr; 
    private boolean[] isPossessedFlag; 
	private int[] freeWalkClock; 
		
	/* 
	* constructs this 
	*/ 
	public CTFController( int totalTime, String[] filename, boolean randomise ) 
	{ 
	    super( totalTime, filename[0], filename[1], "CTFAgent", 2, randomise, false ); 
	    title = "Capture The Flag";
		resetContestantList(); 
		String cityName, agentFilename; 
		agentBCList = new BoardComponentList[cityList.size()]; 
		flagBCList = new BoardComponentList[cityList.size()];  
		jailBCList = new BoardComponentList[cityList.size()]; 
		for ( int i =0; i< cityList.size(); i++ ) 
		{
		    cityName = null;
			cityName = cityList.get(i); 
			//System.out.println("CTFC-c- cityName = " + cityName ); 
			agentFilename = basePathInputCity + cityName + "AgentBC.txt"; 
			agentBCList[i] = readBoardComponents( agentFilename, "triangle" );  
			//System.out.println("size= " + agentBCList[i].size() );
			String flagFilename = basePathInputCity + cityName + "FlagBC.txt"; 
			//System.out.println("CtTFC-c-flagFilename= " + flagFilename); 
			flagBCList[i] = readBoardComponents( flagFilename, "ellipse" ); 
			//System.out.println("CTFC-c-flagBCList " + i + " size is " ); 
			//System.out.println(flagBCList[i].size() ); 
			String jailFilename = basePathInputCity + cityName + "JailBC.txt"; 
			//System.out.println("CTFC-c-jailFilename= " + jailFilename);
		    jailBCList[i] = readBoardComponents( jailFilename, "rectangle" ); 
			// next3  lines are tests of file content at standardoutput 
			//printBCList( agentBCList[i], agentFilename);  
			//printBCList( donorBCList[i], donorFilename);  
			//printBCList( bloodBankBCList[i], bloodBankFilename);  
		} // end for i 
		setAlgorithmList(); 
		flag = new Flag[2]; 
		isPossessedFlag = new boolean[2]; 
		for ( int t=0; t<2; t++ ) 
		    isPossessedFlag[t] = false; 
		jail = new Jail[2]; 
		tab = new int[2]; 
		tab[0] = 0;        // tab for team 0
		tab[1] = (MAX_X/2); // tab for team 1
		shiftAgent = new double[numberOfTeams][2]; 
		shiftFlag = new double[numberOfTeams][2]; 
		shiftJail = new double[numberOfTeams][2]; 
	    colour = new String[2]; 
		colour[0] = "blue"; 
		colour[1] = "red"; 
		flagPossession = new int[2]; 
		roundStringLabel = new String[3]; 
		gamePlayer = new String[numberOfRepetitions][numberOfRounds][numberOfGames][2]; 
		gameResult = new int[numberOfRepetitions][numberOfRounds][numberOfGames]; 
		inJailBufferZone = new boolean[2][MAX_X][MAX_Y]; 
		jailBufferZoneClock = new int[2][sizeOfTeam]; 
		individualGameList = new LinkedList<String>(); 
		freeWalkClock = new int[numberOfTeams]; 
		//System.out.println("CTFC-c-length= " + sizeOfTeam); 
		//System.out.println("CTFC-c- construction finished"); 
	} // end constructor
	
	private BoardComponentList readBoardComponents( String filename, String shape ) 
	{ 
	    BoardComponentList list; 
		try 
		{ 
		    list = boardComponentsFromFile( filename, shape ); 
		    //System.out.println("CTFC-rBC-list size is " + list.size() ); 
		} 
		catch( FileNotFoundException e ) 
		{
		    System.out.println("CTFC-rBC- " + e.toString() + ", exiting");
		    System.exit(0); 
			list = new BoardComponentList(); 
		}
		return list; 
	} // end read components 
	
	/* 
	 ** implements abstract method: 
	 ** but nothing to do in CTF
	 **/ 
	 public void imposeGameRestrictions() 
	 { 
	 	// nothing to do 
	 }	
	/* 
	* implements abstract method: 
	** updates all player's BoardComponent objects
	*/ 
	public void updateAllBoardComponents() 
	{
	    updateCurrentStatus(); 			
		//  now update each player's visibility
	    BoardComponent bC, nextBC; 
		BoardComponentList[][]  vMyA = new BoardComponentList[numberOfTeams][sizeOfTeam]; 		
		BoardComponentList[][]  vOtherA = new BoardComponentList[numberOfTeams][sizeOfTeam]; 		
        BoardComponentList[][]  vMyF = new BoardComponentList[numberOfTeams][sizeOfTeam];  
		BoardComponentList[][]  vOtherF = new BoardComponentList[numberOfTeams][sizeOfTeam];  
	    BoardComponentList[][]  vMyJ = new BoardComponentList[numberOfTeams][sizeOfTeam]; 
        BoardComponentList[][]  vOtherJ = new BoardComponentList[numberOfTeams][sizeOfTeam];  
		for ( int t=0; t< numberOfTeams; t++ ) 
		{    
		    for ( int i=0; i< sizeOfTeam; i++ ) 
			{  
			    vMyA[t][i] = new BoardComponentList();
				vOtherA[t][i] = new BoardComponentList();
				vMyF[t][i] = new BoardComponentList(); 
			    vOtherF[t][i] = new BoardComponentList();
				vMyJ[t][i] = new BoardComponentList();
				vOtherJ[t][i] = new BoardComponentList(); 
			} // end for i
	    } // end for t 
		// now collect visible objects 
		int tOther; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    int i;
			for ( i=0; i< (sizeOfTeam-1); i++ ) 
			{ 
			    bC = player[t][i]; 
				// now check the distances between each agent and it successor
			    for ( int j=i+1; j< sizeOfTeam; j++ ) 
			    { 
				    nextBC = player[t][j]; 
			        if(bC.distance( nextBC )< visibleRange )
					{     
					    vMyA[t][i].add( new BoardComponent(nextBC) ); 
						vMyA[t][j].add( new BoardComponent(bC) ); 
				    }    
		        } // end for j 
				// now do the opponents 
				if ( t==0) 
				{ 
				    for ( int j=0; j< sizeOfTeam; j++ ) 
			        { 
					    nextBC = player[1][j]; 
			            if(bC.distance( nextBC )< visibleRange )
					    { 
						    vOtherA[0][i].add( new BoardComponent(nextBC) ); 
							vOtherA[1][j].add( new BoardComponent(bC) ); 
				        }
		            } // end for j
				} // end if t is 0 
				// now do the flags and jails  
				if ( bC.distance( flag[t] ) < visibleRange ) 
				    vMyF[t][i].add( new Flag( flag[t]) ); 
			    if ( bC.distance( jail[t] ) < visibleRange ) 
			        vMyJ[t][i].add( new Jail( jail[t]) ); 
				tOther = (t+1)%2; 
			    if ( bC.distance( flag[tOther] ) < visibleRange ) 
				    vOtherF[t][i].add( new Flag( flag[tOther]) ); 
				if ( bC.distance( jail[tOther] ) < visibleRange ) 
				    vOtherJ[t][i].add( new Jail( jail[tOther]) ); 
			} // end for i 
		    // now do the last i 
		    bC = player[t][i]; 
		    if ( t==0) 
			{ 
			    for ( int j=0; j< sizeOfTeam; j++ ) 
			    { 
				    nextBC = player[1][j]; 
			        if(bC.distance( nextBC )< visibleRange )
					{ 
					    vOtherA[0][i].add( new BoardComponent(nextBC) ); 
						vOtherA[1][j].add( new BoardComponent(bC) ); 
				    }
				} // end for j
			} // end if t is 0 
			// now do the flags and jails  
			if ( bC.distance( flag[t] ) < visibleRange ) 
			    vMyF[t][i].add( new Flag( flag[t]) ); 
			if ( bC.distance( jail[t] ) < visibleRange ) 
			    vMyJ[t][i].add( new Jail( jail[t]) ); 
			tOther = (t+1)%2; 
		    if ( bC.distance( flag[tOther] ) < visibleRange ) 
			    vOtherF[t][i].add( new Flag( flag[tOther]) ); 
			if ( bC.distance( jail[tOther] ) < visibleRange ) 
			    vOtherJ[t][i].add( new Jail( jail[tOther]) ); 
		} // end for t 
        // now place the time remaining, visible list, locations and blood loads  in their respective players
	    //int timeRemaining = totalTime-currentTime; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    for ( int i=0; i<sizeOfTeam; i++ ) 
		    {    
			    //((CTFAgent) player[t][i]).setAgentTimeRemaining( timeRemaining );
				((CTFAgent) player[t][i]).setMyVisibleAgents( vMyA[t][i] );
				((CTFAgent) player[t][i]).setOtherVisibleAgents( vOtherA[t][i] ); 
				((CTFAgent) player[t][i]).setMyVisibleFlag( vMyF[t][i] );
			    ((CTFAgent) player[t][i]).setOtherVisibleFlag( vOtherF[t][i] ); 
				((CTFAgent) player[t][i]).setMyVisibleJail( vMyJ[t][i] );
				((CTFAgent) player[t][i]).setOtherVisibleJail( vOtherJ[t][i] );
				//System.out.println("BBC-uAB-time= " + currentTime + ", " + currentAgentBCList.retrieve(i).toString() );    
		  } // end for i
	  } // end for t
  } // end update all board components
	
	private void updateCurrentStatus() 
	{ 
		CTFAgent cTFAgent, opponent, agentBC, opponentBC; 
		Jail opponentJailBC, agentJailBC; 
		Flag opponentFlagBC;  
		// now check flag buffer zone for campers 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    if (( !isPossessedFlag[t] ) && ( freeWalkClock[t] <= 0 ))  
			    if ( opponentInFlagBufferZone( t ) ) 
				    for ( int i=0; i< sizeOfTeam; i++ ) 
					    if ( player[t][i].distance( flag[t] ) < BUFFER_ZONE ) 
						    sendToJail( t, i ); 
			freeWalkClock[t]--; 
		} // end for t 
		// now check jail buffer zone for campers
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    for ( int i=0; i< sizeOfTeam; i++ ) 
		    { 
			    if ( isInJailBufferZone( t, i ) ) 
		        { 
				    jailBufferZoneClock[t][i]--; 
					if ( jailBufferZoneClock[t][i] <= 0 )  
					    sendToJail( t, i ); 
				} // end if 
				else // out of zone , so reset clock 
				    jailBufferZoneClock[t][i] = MAX_TIME_IN_BUFFER_ZONE; 
			} // end for i
		} // end for t 
		// now check for tags 
		for ( int i=0; i< sizeOfTeam; i++ ) 
		{ 
		    agentBC =  ((CTFAgent) player[0][i]); 
			for ( int j=0; j< sizeOfTeam; j++ ) 
			{ 
			    opponentBC = ((CTFAgent) player[1][j]); 
				if (  ((int) agentBC.distance( opponentBC )) < TAG_RANGE )  
				{ 
				    if ( inHomeTurf( agentBC, 0 )) 
					{ 
					    if ( ((CTFAgent) player[1][j]).iAmHoldingFlag() ) 
					        freeWalkClock[0] =  (int) (BUFFER_ZONE+TAG_RANGE); 
						sendToJail( 1, j ); 
					}
					else // agent must go to jail 
					{    
					    if ( ((CTFAgent) player[0][i]).iAmHoldingFlag() ) 
						    freeWalkClock[1] = (int) (BUFFER_ZONE+TAG_RANGE); 
							    sendToJail( 0, i ); 
					}
				} // end if two aposing agents in tag range 
			} // end for j
		} // end for i 
		// now check if a jail break or flag capture 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    opponentJailBC = jail[(t+1)%2]; 
			opponentFlagBC = flag[(t+1)%2]; 	
			agentJailBC = jail[t]; 
			for ( int i=0; i< sizeOfTeam; i++ ) 
			{ 
			    agentBC =  ((CTFAgent) player[t][i]);  
				// now check jail break
				if (  ((int) agentBC.distance( opponentJailBC )) < TAG_RANGE )  
			    {    
				    freeAllInmates( t ); 
					} // end if in tag range of opponent jail 
					// now check opponent flag 
					if (  ((int) agentBC.distance( opponentFlagBC )) < TAG_RANGE )  
					{    
					    grabOpponentFlag( agentBC, opponentFlagBC, t ); 
					} // end if in tag range of opponent flag
				} // end for i 
			} // end for t 
	    } // end update current status  
	 
		private boolean opponentInFlagBufferZone( int t )
		{ 
		    int otherT = (t+1)%2; 
			for ( int i=0; i< sizeOfTeam; i++ ) 
			    if ( player[otherT][i].distance( flag[t] ) < BUFFER_ZONE ) 
				    return true;
		return false; 
	} // end opponent in flag buffer zone 
						
	private boolean isInJailBufferZone( int teamNum, int agentNum ) 
	{ 
	    boolean isIn = false; 
		try 
		{ 
		    double[] xY =((CTFAgent) player[teamNum][agentNum]).getCentrePoint(); 
			if ( inJailBufferZone[teamNum][(int) xY[0]][(int) xY[1]] ) 
			    isIn = true;
		} 
		catch( Exception e ) 
		{ 
		} 
		return isIn; 
	} // end is in jail buffer zone 
		
	private boolean inHomeTurf( BoardComponent bC, int homeNum ) 
	{ 
	    boolean isAtHome = false; 
		double[] xY = bC.getCentrePoint(); 
		if ( homeNum == 0 ) 
		    if ( xY[0] < (MAX_X/2) ) 
			    isAtHome = true; 
			else 
			    isAtHome = false; 
	    else // home number must be 1 
	        if ( xY[0] > (MAX_X/2) ) 
			    isAtHome = true; 
			else 
			    isAtHome = false; 
		return isAtHome; 
	} // end in home turf 
	
	private void sendToJail( int teamNum, int memberNum ) 
	{  
	    if ( ! ((CTFAgent) player[teamNum][memberNum]).isInJail() ) 
		{ 	
		    jailBufferZoneClock[teamNum][memberNum] = MAX_TIME_IN_BUFFER_ZONE; 
	        ((CTFAgent) player[teamNum][memberNum]).setCentrePoint( outOfBounds ); 
			((CTFAgent) player[teamNum][memberNum]).setIsInJail( true );  
			if ( ((CTFAgent) player[teamNum][memberNum]).iAmHoldingFlag() ) 
			{
			    isPossessedFlag[(teamNum+1)%2] = false; 
		        ((CTFAgent) player[teamNum][memberNum]).setIAmHoldingFlag( false); 
			}
			if ( teamNum == 0 ) 
			    jailedBlueCount++; 
			else 
			    jailedRedCount++; 		
		    jail[(teamNum+1)%2].incrementInmateCount(); 
		}
		
	} // end send to jail 
	
	private void freeAllInmates( int teamNum ) 
	{ 
	    double[] xY; 
	    if ( teamNum == 0 ) 
		    jailedBlueCount = 0; 
		else
		    jailedRedCount = 0; 		
		jail[(teamNum+1)%2].freeAllInmates(); 
		for ( int i=0; i< sizeOfTeam; i++ ) 
		{ 
		    if ( ((CTFAgent) player[teamNum][i]).isInJail() ) 
			{ 
			    ((CTFAgent) player[teamNum][i]).setIsInJail( false ); 
				xY = new double[2]; 
				xY[0] = tab[teamNum] + (Math.floor( Math.random()*100)%100); 
				xY[1] = 10 + (Math.floor( Math.random()*100)%100); 
				player[teamNum][i].setCentrePoint( xY ); 
			}
	    } // end for i 	
	} // end free all inmates 
	
	private void grabOpponentFlag( CTFAgent agentBC, Flag opponentFlag, int teamNum ) 
	{ 
	    double[] xY = agentBC.getCentrePoint(); 
		opponentFlag.setCentrePoint( xY ); 
		agentBC.setIAmHoldingFlag( true ); 
		isPossessedFlag[(teamNum+1)%2] = true; 
		flagPossession[teamNum]++; 
	} // end grab opponent flag 
	
	protected boolean aFlagHasBeenCaptured() 
	{ 
	    boolean isCaptured = false; 
		if (inHomeTurf( flag[1], 0)  || inHomeTurf( flag[0], 1) )
		{    
		    isCaptured = true; 
			System.out.println("CTF-aFHBC- yes a flag has been captured with time remaining " + getTimeRemaining()); 
		}
		return isCaptured; 
	} // end a flag has been captured 
	
	protected boolean aJailIsFull() 
	{ 
	    boolean isFull = ( jail[0].isJailFull() || jail[1].isJailFull());  
		return isFull;
	} // end a jail is full 
	
	/* 
	** provides access to method in super class from subclass
	**/ 
	protected void playGame() 
	{
		super.playGame(); 
	} // end play game 
	
	
	public void setClockParameters() 
	{ 
	    for ( int t=0; t< numberOfTeams; t++ ) 
		    flagPossession[t] = 0;
	} // end set clock parameters  
	
	/* 
	 ** implements abstract method 
	 **/ 		
    public void updateCurrentContestants( int repNum, int round, int game, int[] id) 
	{ 
	    int winner = getWinner( id ); 
		gamePlayer[repNum][round][game][0] = contestantList[id[0]].getLastName(); 
		gamePlayer[repNum][round][game][1] = contestantList[id[1]].getLastName(); 
		gameResult[repNum][round][game] = winner; 
		finishGameLabel( repNum, round, game, id , winner);
		String str = " "; 
		str += contestantList[id[0]].getLastName(); 
		str += " vs "; 
		str += contestantList[id[1]].getLastName(); 
		if ( winner > -1 ) 
		{ 	
		    ((CTFContestant) contestantList[id[winner]]).updateRoundScores(  repNum, round, 1, 0, 0 ); 
			((CTFContestant) contestantList[id[(winner+1)%2]]).updateRoundScores( repNum, round, 0, 0, 1 ); 
			str += ": Winner is " + contestantList[id[winner]].getLastName(); 
			if ( winner == 0 ) 
			    str += " (Blue) "; 
			else 
			    str += " (Red) "; 
		}
		else // game is a draw 
		{
		    ((CTFContestant) contestantList[id[0]]).updateRoundScores( repNum, round, 0, 1, 0); 
		    ((CTFContestant) contestantList[id[1]]).updateRoundScores( repNum,  round, 0, 1, 0); 
			str += ": result is draw"; 
		}
		str += gameDescription;  
        individualGameList.add( str ); 
		insertionSort(); 
		String[] l = getCurrentRoundStr(); 
		//System.out.println("CTFC-uCC- "  + l[0] + ", " + l[4] );
	} // end update current contestants 
   
	int getWinner( int[] contestantId) 
	{ 
	    int winner = -1; 
		String str = "Game ended by, "; 
		//System.out.println("CTFC-gW-finished game with time remaining " + timeRemaining);
		if ( jail[0].isJailFull() ) 
		{ 
		    winner = 0; 
			//System.out.println("Blue's 
			str += " jail is full"; 
			//System.out.println("Blue's " + str ); 
		} 
		else if (inHomeTurf( flag[1], 0) )
		{ 
		    winner = 0; 
			//System.out.println("Red's 
			str += " flag has been captured";
			//System.out.println("Red's " + str ); 
		} 
		else if ( jail[1].isJailFull() ) 
		{ 
		    winner = 1; 
			//System.out.println("Red's 
			str += " jail is full"; 
			//System.out.println("Red's " + str); 
		}
		else if ( inHomeTurf( flag[0], 1) ) 
		{ 
		    winner = 1; 
			//System.out.println("Blue's 
			str += " flag has been captured"; 
			//System.out.println("Blue's " + str); 
		} 
		else if ( flagPossession[0] > flagPossession[1] ) 
		{    
		    winner = 0; 
			//System.out.println("Blues have 
			str += " possessed opponent's flag for more time"; 
			str += " " + flagPossession[0] + " to " + flagPossession[1]; 
			//System.out.println("Blue have " + str); 
		}
		else if ( flagPossession[0] < flagPossession[1] ) 
		{    
		    winner = 1; 
			//System.out.println("Reds have 
			str += " possessed opponent's flag for more time"; 
			str += " " + flagPossession[1] + " to " + flagPossession[0]; 
			//System.out.println("Reds have " + str); 
		}
		if ( winner > -1 ) 
		    gameDescription = str; 
		else 
            gameDescription = " ";  
	return winner; 
} // end get winner 
	
	/* 
	 ** implements abstract method 
	 **/ 
	public void initialiseGame( int repNum, int round, int game, int[] contestantId ) 
	{ 
	    setGameLabel( repNum, round, game, contestantId ); 
	   	BoardComponent bC; 
		double[] xY, xYBase; 
		int orientationIndex; 
		for ( int t=0; t< numberOfTeams; t++ ) 
		{ 
		    // now set agent centre points and colours
			//System.out.println("CTF-iT t= " + t);
			for (int i=0; i< sizeOfTeam; i++ ) 
			{
			    ((CTFAgent) player[t][i]).setIsInJail( false ); 
				//System.out.println("CTFC-iT-aBCL size is " + agentBCList[repNum].size());
			    bC = agentBCList[repNum].retrieve(i);  
			    xYBase = bC.getCentrePoint();  
				xY = null; 
			    xY = new double[2]; 
				if ( t==0) 
				{ 
				    xY[0] = xYBase[0]; // + shiftAgent[t][0]; 
					xY[1] = xYBase[1]; // + shiftAgent[t][1];
				}
				else 
				{ 
				    xY[0] = shiftAgent[t][0] - xYBase[0]; // + shiftAgent[t][0]; 
					xY[1] = shiftAgent[t][1] - xYBase[1]; // + shiftAgent[t][1];
					//System.out.println("CTFC-it-red centre points are: " + xY[0] + ", " + xY[1]);
				}
				//System.out.println("CTF-iT-t= " + t + ", i= " + i); 
				//orientationIndex = 0; 
				orientationIndex = ((int) (Math.random()*1000))%BoardComponent.NUMBER_OF_INC;
				player[t][i].initialise( xY[0], xY[1], "triangle", orientationIndex ); ; 
				player[t][i].setColour( colour[t] ); 
				//System.out.println("CTFC-iG-agent= " + player[t][i].toString() ); 		
			} // end for i 
			// now set flag  centre points and colour 
			bC = flagBCList[repNum].get(0);
			//System.out.println("CTFC-iT-flag bC= " + bC.toString());
			xYBase = bC.getCentrePoint();  
			xY = null; 
			xY = new double[2]; 
			if ( t==0 ) 
			{ 
			    xY[0] = xYBase[0] + shiftFlag[t][0]; 
				xY[1] = xYBase[1] + shiftFlag[t][1]; 
				//System.out.println("CTFC-it-t= " + t + ", centre points are: " + xY[0] + ", " + xY[1]); 
			} 
			else 
			{ 
			    xY[0] = shiftFlag[t][0] - xYBase[0]; 
				xY[1] = shiftFlag[t][1] -xYBase[1]; 
				//System.out.println("CTFC-it-t= " + t + ", centre points are: " + xY[0] + ", " + xY[1]); 
			}
			//System.out.println("CTF-iT-before flag set");
			flag[t] = null; 
			flag[t] = new Flag( bC );
			flag[t].setCentrePoint( xY ); 
			flag[t].setColour( colour[t] ); 
			//System.out.println("CTFC-iT-finished the flag"); 
			// now set jail centre points and colour 
			bC = jailBCList[repNum].get(0);
			xYBase = bC.getCentrePoint();  
			xY = null; 
			xY = new double[2]; 
			if ( t==0 ) 
			{ 
			    xY[0] = xYBase[0] + shiftJail[t][0]; 
				xY[1] = xYBase[1] + shiftJail[t][1]; 
			} 
			else 
			{ 
			    xY[0] = shiftJail[t][0]-xYBase[0]; 
				xY[1] = shiftJail[t][1] -xYBase[1]; 
			} 
			bC.setCentrePoint( xY );
			jail[t] = null; 
			jail[t] = new Jail( bC, sizeOfTeam, colour[t]  );
			//jail[t].setCentrePoint( xY ); 
			//jail[t].setColour( colour[t] ); 
		} // end for t 
		initialiseJailBufferZone(); 
		timeRemaining = totalTime; 
	} // end initialise game 
	
	/* 
	 ** implements abstract method 
	 **/ 	
     public boolean isGameFinished() 
	 { 
	     boolean finished = (timeRemaining <= 0);  
		 finished = finished || aFlagHasBeenCaptured(); 
		 finished = finished || aJailIsFull(); 
	     return finished; 
	} 
	
	/* 
	 ** implements abstract method 
	 **/ 	
	public void setShiftFactors( int repNum ) 
	{ 
	    shiftAgent[1][0] = MAX_X; //180; 
	    shiftAgent[1][1] = MAX_Y; 
		shiftFlag[1][0] = MAX_X; //180; 
		shiftFlag[1][1] = MAX_Y; 
		shiftJail[1][0] = MAX_X; //180; 
		shiftJail[1][1] = MAX_Y;
	} // end set shift factors 

    private void initialiseJailBufferZone() 
	{ 
	    BoardComponent bC;
		for ( int teamNum=0; teamNum< numberOfTeams; teamNum++ ) 
		{ 
		    for ( int i=0; i< sizeOfTeam; i++ ) 
			    jailBufferZoneClock[teamNum][i] = MAX_TIME_IN_BUFFER_ZONE; 
			for ( int x= 0; x< MAX_X; x++ ) 
			{   
			    for ( int y=0; y< MAX_Y; y++ ) 
				{ 
				    bC = null;  
					bC = new BoardComponent( x, y, 2 ); 
					if ( bC.distance( jail[teamNum] ) <BUFFER_ZONE ) 
					    inJailBufferZone[teamNum][x][y] = true; 
					else 
					    inJailBufferZone[teamNum][x][y] = false; 		
				} // end for y  
			} // end for x 
		} // end for eam number t 
	} // end initialise jail buffer zone 
	
	/* 
	** implements abstract method 
	**/ 	
	public void printAllScores() 
	{
	    int w, l, d;
		String lastName;
		int[][] scores; 
		for ( int i=0; i< contestantList.length; i++ ) 
		{ 
		    //System.out.println(" contestant is " + i ); 
			lastName = contestantList[i].getLastName(); 
			w = ((CTFContestant) contestantList[i]).getNumberOfWins(); 
			l = ((CTFContestant) contestantList[i]).getNumberOfLosses(); 
			d = ((CTFContestant) contestantList[i]).getNumberOfDraws(); 
			//scores = contestantList[i].getRoundScores(); 
			System.out.println(lastName + "    " + w + "    " + l + "    " + d); 
		}  // end for i 
		//System.out.println("----------Final result--------"); 
	} // end print all scores  
 
	/* 
	** implements abstract method 
	**/ 
	public void resultsToFile() 
	{ 
	    String str;
	    try 
		{ 
		    PrintWriter writer = new PrintWriter(basePathOutput+"tournamentResults.txt"); 
		  	writer.println("Rank    Sn     Team    Points");
			int rank = 1;
			for ( int i=0; i< orderedContestantList.size(); i++ ) 
			{    
			    str = rank++ + "      " +orderedContestantList.get(i).toString() ;
				writer.println( str );
			    //System.out.println( bBC.orderedContestantList.get(i).toString() ); 
			}
	        writer.println(); 
	    	writer.println(); 
			writer.println("Individual Game Results are:");
			int count = 0;
			for ( int rep=0; rep< numberOfRepetitions; rep++ ) 
			{    
			    for ( int round=0; round< numberOfRounds; round++ ) 
				{ 
				    writer.println("Repetition " + (rep+1) +", round " + (round+1) ); 
					for ( int g=0; g< numberOfGames; g++ ) 
					    if ( count< individualGameList.size()  ) 
					        writer.println("  " + individualGameList.get(count++) ); 
				} // end for round 
			} // end for rep 
			writer.close();
		} 
		catch( IOException e ) {}	
	} // end results to file 
   
    public void extendGameLabel( int[] contestantId ) 
    {	
	    Contestant c = contestantList[contestantId[0]]; 
		roundStringLabel[3] = c.getLastName();  
		roundStringLabel[4] = "vs"; 
		c = contestantList[contestantId[1]]; 
		roundStringLabel[5] = c.getLastName();  
	} // end extend  game label 
			
	protected void finishGameLabel( int repetition, int round, int game, int[] contestantId , int winner)
    {
	    roundStringLabel[6] = "Result"; 
		if ( winner > -1 ) 
		{ 
		    //+ c.getLastName(); 
			roundStringLabel[7] = null;
			roundStringLabel[7] = "Winner is ";  
			Contestant c = contestantList[contestantId[winner]]; 
			roundStringLabel[8] = null;
			roundStringLabel[8] =  c.getLastName(); 
			
			gameStr += "win: " ;
			gameStr =  c.getLastName(); 
			c = contestantList[contestantId[(winner+1)%2]]; 
			gameStr += ", loss: ";
			gameStr  +=  c.getLastName(); 
		} 
		else 
		{    
		    roundStringLabel[7] = null; 
			roundStringLabel[8] = null; 
			roundStringLabel[7] = "No winner"; 
			roundStringLabel[8] = "Draw"; 
			Contestant c = contestantList[contestantId[0]]; 
			gameStr =  c.getLastName(); 
			c = contestantList[contestantId[1]]; 
			gameStr += "vs "; 
			gameStr  +=  c.getLastName(); 
		}
		gameStr += gameDescription; 
	} // end finish game  label 

	protected void setOverAllResultsLabel() 
	{ 
	    //roundStringLabel = null;
		//roundStringLabel = new String[5]; 
		Contestant c = orderedContestantList.get(0); 
		roundStringLabel[0] = "All Games"; 
	    // need to account for a tie for first place
		roundStringLabel[1] = "Complete"; 
		roundStringLabel[2] = " "; 
		roundStringLabel[3] = " "; 
		roundStringLabel[4] = " "; 
		roundStringLabel[5] = " "; 
		roundStringLabel[8] = "Winner is " + c.getLastName(); 
	} // end set results label 
		
	public String[] getCurrentRoundStr() 
    { 
	    return roundStringLabel;  
    } //  end get current round 

    public String getScore1() 
	{ 
	    return "Wins";
	} 
		
	public String getScore2() 
	{ 
	    return "Draws"; 
	} 
		
	public String getScore3() 
	{ 
	    return "Losses";
    } 
		
	private void insertionSort() 
	{ 
	    CTFContestant[] list = new CTFContestant[orderedContestantList.size()]; 
		CTFContestant current, hold; 	
		// now place all contestants into list 
		for ( int i=0; i< orderedContestantList.size(); i++ ) 
		{ 
		    current = (CTFContestant) orderedContestantList.get(i); 
			list[i] = current; 
		} // end for i 
		// now order list 
		boolean swapped;
		for ( int i=1; i< list.length; i++ ) 
		{ 
		    hold = list[i]; 
			int j= (i-1); 
			swapped = false; 
			while ((j>= 0) && ( hold.getNumberOfPoints() > list[j].getNumberOfPoints() )) 
			{    
			    list[j+1] = list[j]; 
				swapped = true; 
				j--; 
			} // end while 
			if ( swapped ) 
			    list[j+1] = hold; 					
		} // end for i 
		// now put the ordered contestents back into ordered contestant lis 
		orderedContestantList = null; 
		orderedContestantList = new LinkedList<Contestant>(); 
		for ( int i=0; i< list.length; i++ ) 
		    orderedContestantList.add( list[i] ); 	
	} // end insertion sort 
	
	/* 
	 ** implements abstract method 
	 **/ 	
	public LinkedList<String> getIndividualGameList() 
	{ 
	    return individualGameList;
	} 
		

	private void resetContestantList() 
	{
		orderedContestantList = null;  
		orderedContestantList = new LinkedList<Contestant>(); 
        for ( int i=0;i< contestantList.length; i++ ) 
        {    
        	contestantList[i] = new CTFContestant( contestantList[i]);
		    orderedContestantList.add( contestantList[i] ); 
        } // end for i 
    } // end reset contestantList
    
    protected void setTournamentResultsLabel() 
    { 
		setOverallResultsLabel(); 
	 } // end set tournament results label 
	
	static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // should be kept to false when testing, otherwise randomises red and blue 
	    int gameDuration = 499; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "CTFContestantNames.txt";  
	    filename[1] = "CTFCityNames.txt"; 
		CTFController bBC = new CTFController( gameDuration, filename, randomise );   
		bBC.runController(); 
	} // end main   	

} // end CTFController class

