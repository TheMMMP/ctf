package src.Model.CTF;

/* 
* CTFContestant a class to extend the Contestant class for an agent in Capture The Flag tournament. 
* @author Phil Sheridan
* @version  8/2/15
*/ 

import java.util.LinkedList; 
import java.util.Iterator;  
import src.Model.*; 

public class CTFContestant extends Contestant 
{ 
    //private int totalPossible; 
	//private int totalActual; 
	//private int totalTime; 
	
	private int numberOfWins; 
	private int numberOfLosses; 
	private int numberOfDraws; 
	private int totalPoints; 
			
	/* 
	* construct this with specified parameters. 
	* @param className, String holding the agent's class name  of contestant . 
	* @param firstN, String holding the first name of contestant. 
	* @param lastN, String holding the last name of contestant. 
	* @param  nameList, LinkedList<String> containing the names of the cities associated with the repetitions.
	** @param numberOfRounds, an int to represent the number of rounds in the tournament. 
	* don't need this 
	public CTFContestant( String className, String lastN, String firstN, LinkedList<String> nameList, int numberOfRounds ) 
	{ 
        super( className, lastN, firstN, nameList, numberOfRounds ); 
        	
		//totalPossible = 0; 
		//totalActual = 0; 
		//totalTime = 0; 
		
		numberOfWins = 0; 
		numberOfLosses = 0; 
		numberOfDraws = 0; 
	} // end constructor 
	*/ 
		
	/* 
	* construct this with personal details of specified parameter
	* @param c, Contestant to be copied. 
	* @postcondition: idNo, firstN and lastN are that of c. 
	*/
	public CTFContestant( Contestant c ) 
	{ 
	    super( c ); 
	    this.numberOfWins = 0; //;numberOfWins; 
		this.numberOfLosses = 0; //numberOfLosses ; 
		this.numberOfDraws = 0; //numberOfDraws; 
	    //contestantId = c.contestantId; 
	    //this.className = c.className; 
		//this.firstN = c.firstN; 
		//this.lastN = c.lastN;  
	}
	 	
	
	/* 
	public void incrementWin() 
	{ 
	    numberOfWins++;
	} 
    */ 
    /* 	
    public void incrementLoss() 
    { 
        numberOfLosses++; 
    }
    */ 
    	 
    /* 
    public void incrementDraw() 
    { 
        numberOfDraws++;
    }
    */ 
     	 
    public int getNumberOfWins() 
    { 
        return numberOfWins; 
    } 
     
    	  
    public int getNumberOfLosses() 
{     
        return numberOfLosses; 
    } 
     
    	 
    
    public int getNumberOfDraws() 
    { 
        return numberOfDraws;
    } 
     
    	 


	/* 
	 ** implements the compareTo method from Contestant class 
	 ** @param other, Object to be compared with this. 
	 ** @return 1 if this totalPoints > other, -1 if <, 0 otherwise. 
	 *@precondidtion specified parameter is a CTFContestant. 
	 **/  
	public int compareTo( Object other ) 
	{ 
	    CTFContestant c = (CTFContestant) other; 
		if ( this.totalPoints < c.totalPoints ) 
		    return -1; 
		 else if ( this.totalPoints > c.totalPoints ) 
		    return 1; 
		 else
	   {     return 0; 
	   	// must be equal, so try to resolve on totalDinosaursAllocated.
		     // complete the writing of this method 
		//	if ( this.totalTime> c.totalTime ) 
	//		    return -1; 
	//		else if ( this.totalTime < c.totalTime ) 
	 //       return 1; 
	//		else 
	//		return 0;
	} 
} // end compare to 
	 
			 


	/* 
	* updates the three score values from specified parameter. 
	* @param c, Contestant from which scores are taken for update. 
	* @postcondition: each score is incremented by corresponding value of c. 
	*  maybe don't need this 
	*/ 
    /*
	public void update( Contestant c ) 
	{ 
		numberOfSuccesses += c.numberOfSuccesses; 
	    totalDinosaursAllocated += c.totalDinosaursAllocated;  
	    totalTimeTaken += c.totalTimeTaken; 
	} // end update 
	*/ 
		
	/* 
	* updates a round score with specified ones 
	* @paran round, the round number in question 
	* @paran possible, the possible number of units achievable 
	* @param actual, the actual number of units achieved. 
	* @param time, the amount of time taken to complete the round. 
	*/ 
	public void updateRoundScores( int repetition, int round, int firstS, int secondS, int thirdS ) 
	{ 
	    super.updateRoundScores( repetition, round, firstS, secondS, thirdS );
	    if ( ( repetition >=0) && (repetition<roundScores.length) && (round>=0) && ( round< roundScores[0].length)) 
	    {
	    	totalPoints = 0; 
	        for ( int i= 0; i< roundScores.length; i++ ) 
			    for ( int j=0; j< roundScores[0].length; j++) 
			    {    
			    	numberOfWins += roundScores[i][j][0]; 
			    	numberOfDraws += roundScores[i][j][1]; 
			    	numberOfLosses += roundScores[i][j][2]; 
			      } // end for j 
			totalPoints = (2*numberOfWins)  + numberOfDraws;    
	    } // end if 
	    else 
		    System.out.println("CTFC-uR- repetition= " + repetition + ", round " + round + " is out of range");
	} // end update round scores 
	
	public String toString() 
	{ 
	    String str = super.toString();  
		str += " " + numberOfWins; 
	    str += "    " + numberOfLosses; 
		str += "    " + numberOfDraws; 
		str += "    " + totalPoints; 
		return str;
	} // end to string 
	

    
	
	 public int getNumberOfPoints() 
	{ 
	    return totalPoints;  
	} 

} // end CTFContestant class
