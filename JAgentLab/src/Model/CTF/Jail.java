package src.Model.CTF; 

/*
* Jail.java  extends BoardComponent class
* @author Phil Sheridan 
* @version 25/4/14
*/ 

import src.Model.*;

public class Jail extends BoardComponent
{
    // instance variables 
		private int capacity; 
		private int inmateCount; 
	  private BoardComponentList inmateList; 
		private BoardComponent[] inmate; 
		
	/* 
	* constructs this from specified parameter
	* @param bC, the BoardComponent object that determines the location of this.
	*/ 
    public Jail( BoardComponent bC, int capacity, String colour ) 
	 { 
		    super( bC);
				this.capacity = capacity;
				this.colour = colour;
				setInmates();
				inmateCount = 0; 
				inmateList = new BoardComponentList(); 
		} // end constructor 

public Jail( Jail jail ) 
{ 
    super( jail ); 
		capacity = jail.capacity;
		setInmates(); 
		inmateCount = jail.inmateCount; 
		inmateList = new BoardComponentList(); 
		for ( int i=0; i< inmateCount; i++ ) 
		    inmateList.add( inmate[i] ); 
} // end constructor 

/* 
* constructs default Jail 
*/ 
public Jail() 
{ 
    super(); 
		capacity = 0;
		setInmates(); 
		inmateCount = 0; 
		inmateList = new BoardComponentList(); 
} // end constructor  

private void setInmates() 
{ 
    inmate = new BoardComponent[capacity]; 
		int centreX = (int) xY[0]; //xPoints[0]; 
		int width = xPoints[1]; 
		int centreY = (int) xY[1]; //yPoints[0]; 
		//intheight = yPoints[1]; 
		int xInc = (int) (width/capacity); 
		int startX = (int) (xY[0]-(4*xInc)); 
		String apponentColour = "blue"; 
		if ( getColour().equals("blue") ) 
		    apponentColour = "red"; 
		//System.out.println("J-sI-thi Colour = " + getColour() + ", apponentColour= " + apponentColour); 
		for ( int i =0; i< capacity; i++ ) 
		{    
		    inmate[i] = new BoardComponent( (startX+(i*xInc) ), centreY, 4, 4, false ); 
				inmate[i].setColour( apponentColour ); 
		}	
} // end set inmates 

/* deprecated
public int getCapacity() 
{ 
    return capacity;
} 
*/ 

/* 
* gets the spare capacity of this 
* @return int representing spare capacity 
* deprecated
public int getSpareCapacity() 
{ 
    return (capacity-myBloodLoad);
} 
*/ 

public void freeAllInmates() 
{ 
    inmateList.clear(); 
		inmateCount = 0; 
} // end free all inmates 

public boolean isJailFull() 
{ 
    return ( inmateCount == capacity ); 
}  // end is jail full 

/* 
* increments the count of inmates in this. 
*/
public void incrementInmateCount() 
{ 
    inmateList.add( inmate[inmateCount++] );  		
} // end increment inmate count  load / 

/* 
* returns current count of inmates 
*/
public int getInmateCount() 
{ 
    return inmateCount;
} // end get inmate count 	

public BoardComponentList getInmateList() 
{ 
    return inmateList;
} // end get inmate list 

/* deprecated 
public int getRadius() 
{ 
    return (xPoints[1]/2);
} 
*/ 

    public String toString() 
    { 
        String str = super.toString(); 
				return str;
    }
} // end Jail class

