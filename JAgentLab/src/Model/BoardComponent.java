package src.Model;
/*
*  BoardComponent.java a class to encapsulate a component of the Board class 
* @author Phil Sheridan 
* @version 24/10/14  ,17/3/14 fix to equals method
*/ 
public class BoardComponent
{
    // static variables 
	  static public final int NUMBER_OF_INC = 12; 
	  static public final double TURN_INC = 2*Math.PI/NUMBER_OF_INC; 
		static public final double HALF_TURN_INC = TURN_INC/2; 
		static public final double NEGATIVE_BOUND = -Math.PI + TURN_INC; 
		static public final double POSITIVE_BOUND = Math.PI - TURN_INC; 
		static public final double toDegrees = 180/Math.PI; 
    static double[] Orientation_Angles = new double[NUMBER_OF_INC]; 
		static int[] xIP = {-5, 10, -5}; 
		static int[] yIP = {-5, 0, 5}; 
		
		static int[][] Triangle_X_Point = new int[NUMBER_OF_INC][3]; 
		static int[][] Triangle_Y_Point = new int[NUMBER_OF_INC][3];
		static 
		{ 
		    double[] cR; 
		    for (int i=0; i< NUMBER_OF_INC; i++ ) 
				{ 
				    Orientation_Angles[i] = i*TURN_INC; 
						for ( int j=0; j< 3; j++ ) 
						{ 
						    cR = rotate( (int) xIP[j], (int) yIP[j], Orientation_Angles[i] ); 
								Triangle_X_Point[i][j] = (int) Math.floor(cR[0]+0.5); 
								Triangle_Y_Point[i][j] = (int) Math.floor(cR[1]+0.5); 
								
						} // end for j 
				} // end for i 
		} // end static block 
		
    // instance variables 
		protected double[] xY;  
		protected String shape; 
		protected String colour; 
		protected int[] xPoints; 
		protected int[] yPoints; 
		protected int orientation; 
		
		/* 
		* constructs a rectangle or ellipse from specified parameters
		* or
		*/ 
		public BoardComponent( double x, double y, int width, int height, boolean isRectangle ) 
		{ 
		    xY = new double[2]; 
				xY[0] = x; 
				xY[1] = y;  
				if ( isRectangle )
				    shape = "rectangle";
			  else // must be an ellipse 
				    shape = "ellipse";
				setFourPoints( width, height ); 
				colour = "Green"; 
		} // end constructor 

/* 
* constructs a default BoardObject 
*/ 
public BoardComponent() 
{ 
    this( 10, 10, 1, 1, false);
} 


/* 
* constructs a triangle from specified parameters 
*/ 
public BoardComponent( double x, double y, int orientation ) 
{
    setXY( x, y); 
		shape = "triangle";
    colour = "green"; 
		setOrientationIndex( orientation ); 
}  // end constructor of triangle 
		
/* 
* constructs a polygon from specified parameters 
*/ 
public BoardComponent( double x, double y, int[] xP, int[] yP , int orientation) 
{ 
    setXY( x, y ); 
		shape = "polygon";
    colour = "green"; 
		setOrientationIndex( orientation ); 
	  setPolygonPoints( xP, yP ); 				
} // end constructor of polygon 
	
	/* 
	* constructs this from specified parameter 
	*/ 
	public BoardComponent( BoardComponent bC ) 
	{
	    //System.out.println("BC-cfirst Line");
	    setXY( bC.xY[0], bC.xY[1] );   
	    //System.out.println(" second"); 
			colour =bC.colour;	
			//System.out.println("3 "); 
			if ( bC.shape.equals("triangle")) 
		  { 
			    //System.out.println("4 shape is tri"); 
			    shape = "triangle";
         	setOrientationIndex( bC.orientation );
					// don't need the next two lines
					//xPoints = bC.getXPoints(); 
					//yPoints = bC.getYPoints(); 
	    }
		  else if ( !bC.shape.equals( "polygon" ) ) 
	    { 
			    //boolean isRectangle = false; 
					if ( bC.shape.equals( "rectangle" )) 
					    shape = "rectangle"; 
					else 
					    shape = "ellipse"; 
					//isRectangle = true; 
				  //colour = bC.colour;
					xPoints = new int[2]; 
					yPoints = new int[2]; 
					for ( int i=0; i< xPoints.length; i++ ) 
					{
					    xPoints[i] = bC.xPoints[i]; 
					    yPoints[i] = bC.yPoints[i];
					} // end for i 
			} // end if
			else // must be a polygon 
			{  
					// don't need the next line 
					//setPolygonPoints( bC.xPoints, bC.yPoints ); 
					xPoints = bC.xPoints; 
					yPoints = bC.yPoints; 
					setOrientationIndex( bC.orientation );
					colour = bC.colour;
			} // end else
			//System.out.println("BC-c last line");
} // end constructor of clone 

private void setXY( double x, double y ) 
{ 
    xY = new double[2]; 
		xY[0] = x; 
	 xY[1] = y;
} 

private void setFourPoints( int width, int height ) 
{ 
    xPoints = new int[2]; 
		xPoints[0] = -(width/2); 
		xPoints[1] = width; 
		yPoints = new int[2]; 
		yPoints[0] =  -(height/2); 
		yPoints[1] = height;
} // end set four points 

private void setPolygonPoints( int[] xP, int[] yP ) 
{ 
    xPoints = new int[xP.length]; 
	  yPoints = new int[yP.length]; 
		int minLength = Math.min( xP.length, yP.length ); 
		for ( int i=0; i< minLength; i++ ) 
		{
		    xPoints[i] =  xP[i];    
				yPoints[i] = yP[i];
		} // end for i
} // end set polygon points 

public void initialise( double x, double y, String shape, int orientationIndex ) 
{ 
    setXY( x, y ); 
	this.shape = shape; 
	setOrientationIndex( orientationIndex );
} // end initialise 

public void setColour( String c ) 
{ 
    colour = c; 
} 

public String getColour() 
{ 
    return colour;
}

/* 
* set the index of the orientation of this
* @param orientation an int that represents the index of the orientation 
*/ 
public void setOrientationIndex( int orientation ) 
{ 
    this.orientation = orientation%NUMBER_OF_INC;
		if ( shape.equals("triangle")) 
		{ 
		    xPoints = Triangle_X_Point[orientation]; 
				yPoints = Triangle_Y_Point[orientation]; 
		}
} // end set orientation 

/* 
* gets the index of this orientation 
* @return int >=0, < NUMBER)OR_INC 
*/ 
public int getOrientationIndex() 
{ 
    return orientation; 
} 

/* 
* gets the orientation in radians 
*/ 
public double getOrientationRadians() 
{
    return Orientation_Angles[orientation]; 
} 

/* 
* get the orientation in degrees 
*/ 
public int getOrientationDegrees() 
{ 
    return (int) (Orientation_Angles[orientation]*toDegrees); 
} 

/* 
* decrements the index of the orientation 
*/ 
public void decrementOrientation() 
{ 
   orientation -= 1; 
	 if ( orientation < 0 ) 
       orientation += NUMBER_OF_INC; 
} // end decrement 

/* 
* increments the index of the orientation 
*/ 
public void incrementOrientation() 
{ 
    orientation = (orientation+1)%NUMBER_OF_INC;
} 


  /* 
	* gets a safe copy of the coordinates of the centre of this.
	* @return reference to int array containing column/row coordinates of this centre.
	* postcondition: safe opperation, 	*/ 
	public double[] getCentrePoint() 
	{ 
		double[] c = new double[2]; 
		c[0] = xY[0]; 
		c[1] = xY[1];
		return c; 
	} // get centre point

/* 
* set safely the centre point of this from specified parameter. 
* @param cP, the centre point to be set 
*/ 
public void setCentrePoint( double[] cP ) 
{
    xY[0] =  cP[0]; 
		xY[1] = cP[1]; 
} // end set centre point 

  /* 
	* gets the shape of this 
	*/ 
	public String getShape() 
	{ 
	    return shape; 
	} 
	
	/* 
	* gets the column corrdinates of this 
	*/ 
	public int[] getXPoints() 
	{ 
	    //System.out.println("BC-gXP- shape= " + shape); 
	    int[] xP = new int[xPoints.length]; 
			int i; 
			if ( shape.equals( "triangle" ) ) 
		  {  
			    for ( i=0; i<(xP.length); i++ ) 
			        xP[i] = (int) (xPoints[i] + xY[0]);
					//xP[i] = xPoints[i];
			} // end if is triangle 
			else if ( shape.equals("ellipse") || shape.equals("rectangle")) 
			{ 
			    xP[0]= (int) (xY[0]+xPoints[0]); 
					xP[1] = xPoints[1]; 
					//System.out.println("BC-gXP-xp0= " + xP[0] + ", xP1= " + xP[1] ); 
			} // end if ellipse or rectangle 
			else // must be a polygon 
			{ 
			    System.out.println("BC-gXP-in polygon with shape= x" + shape +"z");  
			    for ( i=0; i<xP.length; i++ ) 
			        xP[i] = (int) (xPoints[i] + xY[0]);
			} // end a polygon
	    return xP;
  } // end get xpoints  
	 
	public int[] getYPoints() 
	{ 
	    int[] yP = new int[xPoints.length]; 
			int i; 
			if ( shape.equals( "triangle" ) ) 
		  {  
			    for ( i=0; i<(yP.length); i++ ) 
			        yP[i] = (int) (yPoints[i] + xY[1]);
					//yP[i] = yPoints[i];
			} // end if is triangle 
      else if ( shape.equals("ellipse") || shape.equals("rectangle")) 
			{ 
			    yP[0]= (int) (xY[1]+yPoints[0]); 
					yP[1] = yPoints[1]; 
			} // end if ellipse or rectangle 
			else // must be a polygon 
			{ 
			    for ( i=0; i<yP.length; i++ ) 
			        yP[i] = (int) (yPoints[i] + xY[1]);
			} // end a polygon
	    return yP;
  } // end get ypoints 
	
	/* 
	* gets the row coordinates of this 
	* deprecated  
	public int[]getYPoints() 
	{ 
	    int[] yP = new int[xPoints.length];     
			int i; 
			for ( i=0; i<yP.length; i++ ) 
			    yP[i] = (int) (yPoints[i] + xY[1]);
		if ( !shape.equals( "triangle" ) ) 
		{    
		    i--; 
				yP[i] = yPoints[i];
		}
	 return yP;  
} // end get ypoints 
*/ 

	
/* 
	* @return String containing a discription of this. 
	*/ 
	public String toString() 
	{ 
	  String str = "col/row centre is " + (int) xY[0] + ", " + (int) xY[1];
	  str += ", shape is " + shape;
		str += ", colour is " + colour;
	  for ( int i=0; i<xPoints.length; i++ ) 
		    str += ", (" + xPoints[i] + ", " + yPoints[i] + ")";
	//str += ", radius is " + radius;	
		return str; 
	} 
	
	/* 
	* gets the uclidian distance from this to the specified parameter. 
	* @param bC a BoardComponent object
	*/ 
	public double distance( BoardComponent bC ) 
	{
	    double[] xYA = bC.getCentrePoint(); 
		  double x = xY[0]- xYA[0]; 
		  x *= x; 
		  double y = xY[1] - xYA[1]; 
		  y *= y; 
		  return  Math.sqrt( x + y ); 
	}  // end distance 
	

	static public double[] rotate( int x, int y, double rotationFactor) 
	{ 
		double[] rC = new double[2]; 
	  double r1 = Math.sqrt( Math.pow(y, 2) + Math.pow(x, 2) ); 
		double theta1 = Math.PI/2; 
		if ( x != 0 ) 
			theta1 = Math.atan( y/(double)x ); 
		if (( x< 0.0 ) || ((x==0) && (y<0))) 
			theta1 += Math.PI;
		
		double theta2 = theta1 + rotationFactor; 
		rC[0] =  r1*Math.sin( theta2 );  
		rC[1] =  r1 * Math.cos( theta2 ); 
		double[] xY = new double[2]; 
		xY[0] = rC[1]; 
		xY[1] = rC[0]; 
		return xY; //rC; 
	} // end rotate 
 

public boolean equals( BoardComponent bC ) 
{ 
    boolean bool = ((((int)this.xY[0]) == ((int)bC.xY[0])) && (((int)this.xY[1]) == ((int)bC.xY[1]))); 
		//System.out.println("BC-e- equals= " + bool ); 
		 
		return bool;
} // end equals 

} // end BoardComponent class

