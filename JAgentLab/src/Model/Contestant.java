package src.Model;

/* 
* Contestant an abstract  class to encapsulate a contestant for an agent in JAgentLab tournament. 
* @author Phil Sheridan
* @version  17/3/15 added the method getFirstResult()  
*/ 

import java.util.LinkedList; 
import java.util.Iterator; 

public abstract class Contestant 
{ 
    static int Number_Of_Contestants = 0; 
    static private String[] roundName; 
	static private boolean areRoundNamesSet; 
		
	protected int contestantId; 
	protected String className; // identification numbero of this
	protected String firstN; // first name of this
	protected String lastN;  // last name of this
	protected int[][][] roundScores; 
 	private int firstComponent; 
 	private int secondComponent; 
 	private int thirdComponent; 
 				
	static private void setRoundNames( LinkedList<String> list, int numberOfRounds ) 
	{ 
	    if ( !areRoundNamesSet ) 
		{ 
		    roundName = new String[list.size()]; 
			for ( int i=0; i<list.size(); i++ ) 
			    roundName[i] = list.get(i); 
		} 
		areRoundNamesSet = true; 
	} // end set round names 
	
	/* 
	* construct this with specified parameters. 
	* @param idNo, String holding the identification number of contestant . 
	* @param firstN, String holding the first name of contestant. 
	* @param lastN, String holding the last name of contestant. 
	*/ 
	public Contestant( String className, String lastN, String firstN, LinkedList<String> nameList, int numberOfRounds ) 
	{ 
        contestantId = Number_Of_Contestants++; 
	    this.className = className; 		
		this.firstN = firstN; 
		this.lastN = lastN; 
		setRoundNames( nameList, numberOfRounds ); 
		roundScores = new int[roundName.length][numberOfRounds][3]; 
		//System.out.print("C-c-roundScores.length= " + roundScores.length ); 
		//System.out.print(", roundScores[0].length= " + roundScores[0].length ); 
		//System.out.println(", roundScores[0][0].length= " + roundScores[0][0].length); 
		//System.out.println(" numberOfRounds= " + numberOfRounds );  
	} // end constructor 
	
	/* 
	* construct this with personal details of specified parameter
	* @param c, Contestant to be copied. 
	*/
	public Contestant( Contestant c ) 
	{ 
	    contestantId = c.contestantId; 
	    this.className = c.className; 
		this.firstN = c.firstN; 
		this.lastN = c.lastN; 
		//System.out.println("C--c- rS.length= " + c.roundScores.length + ", rF[0].length= " + c.roundScores[0].length + ", rF[0][0].length= " + c.roundScores[0][0].length); 	
		this.roundScores = new int[c.roundScores.length][c.roundScores[0].length][c.roundScores[0][0].length]; 
		for ( int i=0; i< roundScores.length; i++ ) 
			for ( int j=0; j< roundScores[0].length; j++ ) 
				for ( int k=0; k< roundScores[0][0].length; k++ ) 
					roundScores[i][j][k] = c.roundScores[i][j][k]; 
	} // end constructor 
	
	abstract public int compareTo( Object other ); 
			 
	/* 
	* @return the identification number of this. 
	*/ 
	public String getIdNo() 
	{ 
		return className; //idNo; 
	} 

	/* 
	* @return firstN
	*/ 
	public String getFirstName() 
	{ 
		return firstN; 
	} 

	/* 
	* @return last name of this 
	*/ 
	public String getLastName() 
	{ 
		return lastN; 
	} 

	public int getContestantId() 
	{ 
	    return contestantId;
	}
	
	/* 
	* determines whether this is equal to specified parameter or not. 
	* @param rHS object to be compared. 
	* @return true idNo of this equals idNo of rHs, false otherwise.
	* @precondition: rHS is a Contestant object. 
	*/ 
	public boolean equals( Object rHS ) 
	{ 
		Contestant c = (Contestant) rHS; 
		if ( this.className.equals( c.className) ) 
		    return true; 
		else 
			return false; 
	} // end equals 		
	
	/* 
	* updates a round score with specified ones 
		*/ 
	protected void updateRoundScores( int repetition, int round, int firstS, int secondS, int thirdS ) 
	{ 
	    if ( ( repetition >=0) && (repetition<roundScores.length) && (round>=0) && ( round< roundScores[0].length)) 
		{ 
		    roundScores[repetition][round][0] = firstS; 
			roundScores[repetition][round][1] = secondS; 
			roundScores[repetition][round][2] = thirdS; 
			firstComponent = 0; 
			secondComponent = 0; 
			thirdComponent = 0; 
		    for ( int i=0; i< roundScores.length; i++ ) 
		    	for ( int j=0; j< roundScores[i].length; j++ ) 
		    	{ 
		    	    firstComponent += roundScores[i][j][0]; 
			        secondComponent += roundScores[i][j][1]; 
			        thirdComponent += roundScores[i][j][2]; 
		    	} // end for j 
		    	//for ( int i= 0; i< 3; i++ ) 
		    	//	System.out.println("C-uRs i= " + i + ", roundScore= " + roundScores[repetition][round][i] ); 
		    	//System.out.println("befor ppp"); 
		    	//int[][] pp = getCityScores(); 
		}
		else 
		    System.out.println("C-uR- Problem!!! repetition= " + repetition + ", round " + round + " is out of range");
	} // end update roundScores 
	 
	public void incrementRoundScores( int repetition, int round, int firstS, int secondS, int thirdS ) 
	{ 
	    if ( ( repetition >=0) && (repetition<roundScores.length) && (round>=0) && ( round< roundScores[0].length)) 
		{ 
		    roundScores[repetition][round][0] += firstS; 
			roundScores[repetition][round][1] += secondS; 
			roundScores[repetition][round][2] += thirdS; 
			firstComponent = 0; 
			secondComponent = 0; 
			thirdComponent = 0; 
		    for ( int i=0; i< roundScores.length; i++ ) 
		    	for ( int j=0; j< roundScores[i].length; j++ ) 
		    	{ 
		    	    firstComponent += roundScores[i][j][0]; 
			        secondComponent += roundScores[i][j][1]; 
			        thirdComponent += roundScores[i][j][2]; 
		    	} // end for j 
		}
		else 
		    System.out.println("C-uR- repetition= " + repetition + ", round " + round + " is out of range");
	} // end increment round scores  ro
	
	public int getFirstComponent() 
	{ 
		return firstComponent; 
	}
	
	public int getSecondComponent() 
	{ 
		return secondComponent; 
	}
	
	public int getThirdComponent() 
	{ 
		return thirdComponent; 
	}
	/* 
	* unsafely, gets all round scores for this 
	* @return 2D array,for each row,possible, actual, time 
	*/ 
	public int[][][] getRoundScores() 
	{ 
		int[][][] s= new int[roundScores.length][roundScores[0].length][roundScores[0][0].length]; 
		for ( int i= 0; i< s.length; i++ ) 
			for ( int j=0; j< s[i].length; j++ ) 
				for ( int k=0; k< s[i][j].length; k++ ) 
					s[i][j][k] = roundScores[i][j][k]; 
						
	    return s; //roundScores; 
	} 

  /* 
	* unsafely, get the round names for this 
	* @return array of String object with a city name for each round 
	*/ 
	public String[] getRoundNames() 
	{ 
	    return roundName;
	} 
	
	
		

		
	/* 
	* @return this as a String object. 
	*/ 
	public String toString() 
	{ 
		//String str = firstN + " " + lastN + " " + className;
		String str =  className; 
		return str;
	} 

    
    public int[][] getCityScores() 
    { 
    	//System.out.println("C-gCS-start"); 
        int[][] s = new int[roundScores.length][3]; 
        for ( int i=0; i< s.length; i++ ) 
        	for ( int j=0; j< s[i].length; j++ ) 
        	{	
        		s[i][j] = roundScores[i][0][j]; 
        		//System.out.println("   s= " + s[i][j] + ", roundScores= " + roundScores[i][0][j]); 
        	} 
        return s; 
    } // end get city scores  
		
	public int getFirstResult() 
	{ 
		int score = 0; 
		if ( roundScores != null ) 
		    score = roundScores[0][0][0]; 
		    	return score;  
	}	   
} // end Contestant class
