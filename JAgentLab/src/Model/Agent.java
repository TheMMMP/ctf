package src.Model;

/**
 * Agent.java, encapsulates a Agent 
 * @author Phil Sheridan
 * @version 9/4/14 )
 */

public abstract class Agent extends BoardComponent
{
   //static variables 
		static public int Agent_Count = 0; 
    static public int numberOfAgents() 
		{ 
		    return Agent_Count;
		} 
		// instance variables 
	protected int myIdNumber; //index 0 is due east and clockwise
	protected double mySpeed; // pixel/second
  protected int myTimeRemaining; // time this agent has left to complete task
	
	
	/**
    * Constructor for objects of class Agent     
		*/
    public Agent( BoardComponent bC) 
	  {
				super( bC );
				myIdNumber = Agent_Count++;  
				mySpeed = 1.0;
				myTimeRemaining = Integer.MAX_VALUE; 
		} // end constructor
	
 /* 
	* constructs a default Agent object 
	*/ 
	  public Agent( ) 
	  {
				super(); 
				myIdNumber = Agent_Count++;  
				mySpeed = 1.0;
				myTimeRemaining = Integer.MAX_VALUE; 
		} // end constructor

	  /* 
		* gets the id number of this 
		*/ 
		public int getIdNumber() 
		{ 
		    return myIdNumber; 
		}
		
		/* 
		* gets the speed of this 
		*/ 
		public double getSpeed() 
		{
		    return mySpeed; 
		} 
		
		/* 
		* get the angle to the specified location in radians
		* @param bC, the BoardComponent of the target object.
		* @return angle of direction from this: positive indicates object th right of this, 
		*                             negative number indicates to left of this
		*/ 
		public double getAngleTo( BoardComponent bC ) 
		{ 
		    double[] xYA = bC.getCentrePoint(); 
				double hD = xYA[0] - xY[0]; 
				double vD = xYA[1] - xY[1]; 
				double angle = 0.0; 
				if ( hD != 0.0 ) 
				    angle = Math.atan( vD/hD );
				//System.out.println("xYA centre is " + xYA[0] + ", " + xYA[1] ); 
				//System.out.println("this       is " + xY[0] + ", " + xY[1]);
				//System.out.println("vD= " + vD ); 
				//System.out.println("hD = " + hD ); 
				//System.out.println("angle= " + angle ); 
				
				if (vD > 0.0 ) // targ in 3rd or 4th quad
			      if ( hD >= 0.0 ) 
						    angle  = angle; //Math.PI - angle;
						else // hD must be negative so add
						    angle = Math.PI + angle;
				else // vD must be < 0
				    if ( hD > 0 ) 
						    angle = 2*Math.PI+angle; 
						else // hD must be < 0 
						    angle = Math.PI+angle; 
				 angle = angle-getOrientationRadians(); 	   
				if ( angle > Math.PI ) 
				    angle -= (2*Math.PI);
				return angle;
		} // end get angle to 

		/* 
		* gets the location of the specified object as a vector. 
		* @param bC the BoardComponent object of interest
		* @return two element double array the first element is angle 
		*  and second is distance in number of pixels
		*/ 
		protected double[] getLocationOf( BoardComponent bC ) 
		{
		    double[] v = new double[2]; 
				v[0] = this.getAngleTo( bC ); 
				v[1] = distance( bC ); 
				return v; 
		} // end location of 
		
		
      /* 
		  * sets to speed of this
		  * @param agentSpeed, an int that spcifies the speed. 
		  * @postcondition: this  agent's speed is the specified one.
		  */ 
		  public void setAgentSpeedTo( double agentSpeed ) 
		  { 
	        if ( (agentSpeed >= 0.0) && ( agentSpeed <= 1.0 )) 
					    mySpeed = agentSpeed;
					else 
					{ 
					    if ( agentSpeed > 1.0) 
							    mySpeed = 1.0; 
							else // speed must be < 0 so set to 0
							    mySpeed = 0.0; 
					} // end else 
			} // end set speed 
		
		/* 
		* turns this left by a small amount		
		*/ 
		protected void turnLeft() 
		{ 
		    decrementOrientation(); 
			} // end turn left 

    /* 
		* turns this right by a small amount 
		*/ 
		protected void turnRight() 
		{ 
		    incrementOrientation();   
		} // end turn right 
			
			/* 
			* set the time remaing for this
			* @param tR, the time remaining to be set 
			*/ 
			public void setAgentTimeRemaining( int tR ) 
			{ 
			    myTimeRemaining = tR; 
			} 
			
			public abstract void agentStrategy(); 
		
		
			public String toString() 
			{ 
			    String str = "Time left= " + myTimeRemaining; 
					str += ", Id= " + myIdNumber; 
					str += ", direction= " +getOrientationDegrees(); //((int) (Math.floor(myDirection*100+0.5)/100 ));
				  str += ", speed= " + ((double) (Math.floor(mySpeed*10+0.5)/10 ));
          str += super.toString(); 				
					return str;
			} // end to string
			
} // end Agent class
