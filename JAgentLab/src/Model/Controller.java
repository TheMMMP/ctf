package src.Model;
/*
* Controller.java 
* @author Phil Sheridan 
* @version 25/3/15 implemented basePathOut 
*/
import java.util.Map; 
import java.util.HashMap; 
import java.util.LinkedList; 
import java.util.Iterator; 
import java.util.ListIterator; 
import java.io.*; 
import java.util.Scanner;  
import java.util.LinkedList; 
import java.util.Iterator; 
//import src.Model.MyClasses.*; 

public abstract class Controller
{
    // class constants  
	static protected int timeRemaining; 
	static protected int totalTime; 
	static final public  int MAX_X = 300; //FIELD_SIZE; 
	static final public  int MAX_Y = 200; //FIELD_SIZE; 
	static final boolean[][] field = new boolean[MAX_X][MAX_Y]; 
	static 
	{ 
	    for ( int i=0; i< MAX_X; i++ ) 
			for ( int j=0; j< MAX_Y; j++ ) 
			    if ((i>1) &&(i<(MAX_X-2)) && (j>1) &&(j<(MAX_X-2))) 
				    field[i][j] = true; 
				else 
			        field[i][j] = false;
	} // end static block 

    /* 
     ** determines whether a new game has started 
     ** @return true if timeRemaining has been set to total time 
     **   and false if timeRemaining < total time. 
     **/ 
     static public boolean isNewGame() 
     { 
     	return (timeRemaining == totalTime); 
     } 
     		
	/* 
	*  determines whether specified parameter is inside the playing field or not. 
	* @param xY, a double[] that specifies the column-x cordinate and row-y coordinate of the point in question 
	* @return true if if xY is a valid position on playing field, false otherwise. 
	*/ 
	static public boolean isInBounds( double x, double y ) 
	{ 
	    boolean inBounds = false; 
		try 
		{ 
		    inBounds = field[((int) x)][((int) y)]; 
		} 
		catch( Exception e ) 
		{ 
	       // do nothing	
		} 
		return inBounds; 
	} // end is in bounds 

    static public boolean isInBounds( double[] cP ) 
	{ 
	    return isInBounds( cP[0], cP[1] ); 
    } 
		
   	
    // instance variables 
	protected int numberofRepetitions; 
	protected int numberOfRepetitions; 
	protected int numberOfRounds; 
    protected int numberOfGames;
    protected int numberOfTeams; 
    protected int sizeOfTeam;  
	protected Agent[][] player; 
	protected BoardComponentList[] agentBCList; 
	protected BoardComponentList[] currentAgentBCList; 
	protected Agent[][] algorithmList; 
    public LinkedList<String> cityList;   
	protected  BoardComponentList currentCity; 
   	private String extensionName; 
	protected LinkedList<String> noAlgorithmFoundList; 
	public LinkedList<Contestant> orderedContestantList; 
	protected Agent[][] teamList; 
	protected Contestant[] contestantList; 
	protected String title; 
	protected int[][][] schedule; 
	protected String[][][][] gamePlayer; 
	protected int[][][] gameResult; 
 	protected String gameDescription; 
	protected LinkedList<String> individualGameList; 
	protected String gameStr; 
 	protected String[] roundStringLabel; 
	protected String basePathInput;  
	protected String basePathInputCity;  
	protected String basePathOutput;  
    	
	// abstract methods that must be implemented in derived class
	public abstract void updateAllBoardComponents();
	public abstract String[] getCurrentRoundStr(); 
	public abstract String getScore1(); 
	public abstract String getScore2(); 
	public abstract String getScore3(); 
	public abstract LinkedList<String> getIndividualGameList();	
	public abstract void setClockParameters(); 
	public abstract void setShiftFactors( int repNum ); 	
	public abstract void printAllScores(); 
	public abstract void resultsToFile(); 
	public abstract void initialiseGame( int repNo, int round, int game, int[] contestantId ); 
    public abstract boolean isGameFinished(); 
    public abstract void updateCurrentContestants( int repNum, int round, int game, int[] id); 
    public abstract void imposeGameRestrictions(); 
    public abstract void extendGameLabel( int[] contestantId );  
    			  
	/* 
	* constructs this 
	* @param totalTime, int specifying the duration of a game in simulated seconds 
	* @param contestantListFilename, String specifying the name of contestant file name 
	* @param cityListFilename, String specifying the name of file containing city names
	* @param exN, String specifies the name to which this is extended in subclass 
	* @param numberOfTeams, int specifies number of teams per game-should be 2 
	* @param randomise, boolean specifies whether the blues and red are to be randomised or not. 
	*/  
	public Controller( int tT, String cF, String cLF, String exN, int nOT, boolean randomise, boolean isAssessorMode) 
	{ 
		//System.out.println("C-c-start"); 
        this.totalTime = tT; 
		extensionName = exN; 
		this.numberOfTeams = nOT;
		System.out.println("Contr-c numberOfTeams= " + numberOfTeams ); 

		noAlgorithmFoundList = new LinkedList<String>();  
		String cityListFilename; 
		String contestantListFilename; 
		if ( isAssessorMode ) 
		{ 
	        basePathInputCity = "Assessor\\InputFiles\\Cities\\"; 
			basePathInput = "Assessor\\InputFiles\\"; 
			basePathOutput = "Assessor\\OutputFiles\\"; 
		}
		else 
		{  
			basePathInputCity = "InputFiles\\";
			basePathInput = "InputFiles\\";
	        basePathOutput = "OutputFiles\\"; 
	
		}
		cityListFilename = basePathInputCity + cLF;
		contestantListFilename = basePathInput + cF;
	
		setCityList( cityListFilename ); 
		int numValidContestants = getNumberOfValidContestants( contestantListFilename ); 	
		Scheduler scheduler = new Scheduler( numberOfTeams, numValidContestants, randomise ); 
		schedule = scheduler.getSchedule(); 
		numberOfRounds = scheduler.getNumberOfRounds(); 
		//System.out.println("Contr-c- numberOfrounds = " + numberOfRounds );  
		numberOfGames = scheduler.getNumberOfGames(); 
		//System.out.println("C-c-numberOfRepetitions= " + numberOfRepetitions + ", numberOfRounds= " + numberOfRounds + ", numberOfGames = " + numberOfGames); 
		setContestantList( contestantListFilename );  
		// next two line are for testing 
		printContestantList(); 
		//printCityList();
		//currentRound = 0; 
		//System.out.println("C-c-numberof contestants = " + contestantList.length );  
		player = new Agent[numberOfTeams][]; 	 		
	} // en constructor 
	
	public void runController() 
	{
        //System.out.println("C-rC-start");  
	    for ( int repetition=0; repetition< numberOfRepetitions; repetition++ ) 
		{ 
			//System.out.println("Contr-rC- start repetion= " + repetition ); 
		    // Not sure about origen of next line
		    //startNextRepetition(); 
			setShiftFactors( repetition );  
			sizeOfTeam = agentBCList[repetition].size(); 
			for ( int round = 0; round < numberOfRounds; round++ ) 
			{  
				for ( int game=0; game< numberOfGames; game++ ) 
				{ 
					//System.out.println("C-rC- repetition= " + repetition + ", round= " + round + ", game= " + game ); 
				    runNextGame( repetition,  round, game ); 
				    // temp comment out next line 
				    printRoundStringLabel();  
				} // end for game 
			} // end for round 
		} // end for repitition  
		// aaa temp 
		//Contestant contestant = getContestant( "SBenchMarkSingle" ); //minSolIdNo ); 
		//System.out.println("Contr-rC-2- contestant= " + contestant.toString()); 
		//int[][][] s = contestant.getRoundScores(); 
        //for ( int i=0; i< s.length; i++ ) 
        //	for ( int j=0; j< s[i].length; j++ ) 
        //		for ( int k=0; k < s[i][j].length; k++ ) 
        //			System.out.println("   " + s[i][j][k] ); 
        				
		// bbb 
		setOverallResultsLabel(); 
		printRoundStringLabel(); 
		//printAllScores(); 
		resultsToFile(); 
	} // end run controller 

	public void runNextGame( int repNo, int round, int game ) 
	{ 
	    //System.out.println("CTFC-rNG-started"); 
	    double[] xY; 
		double speed; 
	    int[] contestantId = new int[numberOfTeams];
	    setClockParameters(); 
	    for ( int t=0; t< numberOfTeams; t++ ) 
		{
	        contestantId[t] = schedule[round][game][t]; 
			//System.out.println("C-rNG-round= " + round + ", game= " + game + ", contestantId= " + contestantId[t] ); 
			player[t] = teamList[contestantId[t]]; 
		} // end for t 
		//System.out.println("CTFC-rNG-before initialise"); 
		initialiseGame( repNo, round, game, contestantId );  
		timeRemaining = totalTime; 
		boolean finished = false; 
		while ( !finished ) 
		{	
		    playGame(); 	
		  	finished = isGameFinished(); 
		} // end while	
		//System.out.println("CTFC-rNG-finished while");
		updateCurrentContestants( repNo, round, game, contestantId );  
		//l = getCurrentRoundStr(); 
		//System.out.println("C-rNG- " + l[0] + ": " + l[1]); 
		//System.out.println(); 
	} // end run next game 
 		
 	protected void playGame() 
	{
		invokeStrategy(); 
		imposeGameRestrictions(); 
		move(); 
		updateAllBoardComponents();
		timeRemaining--;
	} // end play game 

	protected void setOverallResultsLabel() 
	{ 
	    // first, erase all the round results 
	    for ( int i= 0; i< roundStringLabel.length; i++ ) 
		{	
		    roundStringLabel[i] = null; 
			roundStringLabel[i] = " "; 
		} // end for i 		
	   	// now determine the number of tied first place 
	   	Contestant c =  orderedContestantList.get(0); 
		int total = c.getFirstComponent(); 
		int count = 0; 
		int i= 1; 
		boolean finished = (i >= orderedContestantList.size()); 
		while ( !finished ) 
		{  
	        if ( c.compareTo( orderedContestantList.get(i)  ) == 0) 
	            count++; 
	        else 
	            finished = true; 
	        i++; 
	        finished = finished || ( i >= orderedContestantList.size() ); 
		} // end while 
		// now set the labels 
		if ( count > 1 ) 
		{ 
			String number; 
			if ( count == 2 ) 
				number = "Two"; 
			else if ( count == 3 ) 
			    number = "Three"; 
			else 
			    number = "Multiple"; 
			roundStringLabel[3] = number; 
			roundStringLabel[4] = "Way"; 
			roundStringLabel[5] = "Tie"; 
			if ( count > 3 ) 
				count = 3; 
			for ( int j=0; j< count; j++ ) 
				roundStringLabel[6+j] = orderedContestantList.get(j).getLastName(); 
					 			
			
		} // end if count > 1  
		else // must be unique winner 
		{ 
		    roundStringLabel[3] = "Champion"; 
		    roundStringLabel[4] = "is"; 
		    roundStringLabel[5] = orderedContestantList.get(0).getLastName(); 
		    if ( orderedContestantList.size() > 1 ) 
		        roundStringLabel[6] = "Second is" + orderedContestantList.get(1).getLastName(); 
		   	if ( orderedContestantList.size() > 2 ) 
		        roundStringLabel[7] = "Third is" + orderedContestantList.get(2).getLastName(); 
			if ( orderedContestantList.size() > 3 ) 
		        roundStringLabel[8] = "Fourth is" + orderedContestantList.get(3).getLastName(); 
		
		} 		 
	} // end set overall results label 
		
	
   		
    private void printRoundStringLabel() 
    { 
        System.out.println(roundStringLabel[0] + " " + roundStringLabel[1] + " " + roundStringLabel[2] ); 
        System.out.println(roundStringLabel[3] + " " + roundStringLabel[4] + " " + roundStringLabel[5] ); 
        System.out.println(roundStringLabel[6] + " " + roundStringLabel[7] + " " + roundStringLabel[8] ); 
    } // end print round string label 
    
    public int getTotalTime() 
    { 
        return totalTime;
    } 

    public String getTitle() 
    { 
        return title; 
    } 


    /* 
    * reads BoardComponent objects from specified filename and stores them in list
    */ 
    protected BoardComponentList boardComponentsFromFile( String filename, String shape ) throws FileNotFoundException 
    { 
        BoardComponentList boardComponentList = new BoardComponentList(); 
		//System.out.println("CM-iFF- start"); 
		String fullFilename = filename; 
		//System.out.println("C-fullFilename= " + fullFilename); 
		Scanner inFile = new Scanner(new FileReader( fullFilename)); 
	    BoardComponent bC = new BoardComponent(); 
		String line; 
		while (inFile.hasNext())  
        {   
		    line = inFile.nextLine(); 
		    //System.out.println("C-fF-line is " + line); 
		    bC = makeBoardComponent( line, shape ); 
			boardComponentList.add( bC ); 
		} // end while 
		return boardComponentList;
    } // end board components from file
 	
 
	/* 
	* make triangle shape objects 
	* need to add code to check the size of lS and then call the appropriate BC constructor 
	*/ 
	private BoardComponent makeBoardComponent( String line, String shape ) 
	{
	    int x, y; 
		int orientation = 0; 
		int width = 0; 
		int height = 0;
 	    BoardComponent bC = new BoardComponent();
		Scanner lS = new Scanner( line ); 
		x = lS.nextInt(); 
		y = lS.nextInt(); 
		if ( shape.equals("triangle") ) 
		{
		    orientation = lS.nextInt(); 
		    bC = new BoardComponent ( x, y, orientation );
		} 
		else if( shape.equals("rectangle") ) 
		{ 
		    width = lS.nextInt(); 
		   	height = lS.nextInt(); 
		    bC = new BoardComponent ( x, y, width, height, true );
		
		} 
		else if ( shape.equals("ellipse") ) 
		{ 
		    width = lS.nextInt(); 
		   	height = lS.nextInt(); 
		    bC = new BoardComponent ( x, y, width, height, false );
		} 
		else // must be a polygon 
		{ 
		    // read pairs of points to the end os lS
				// don't know if this is needed right now 
				System.out.println("C-mBC- apolygon, so exiting"); 
				System.exit(0);
		}
		//System.out.println("C-mBC-bC is " + bC.toString() );
		return bC; 	
	} // end make  board component 



    protected void invokeStrategy() 
    { 
        for ( int t=0; t< numberOfTeams; t++ ) 
            for (int i = 0; i< sizeOfTeam; i++ ) 
		    {    
		        try 
		        { 
			        //System.out.println("C-iS- agent " + i ); 
			        player[t][i].agentStrategy(); 
			    } 
			    catch( Exception e ) 
			    { 
			        player[t][i].mySpeed = 0.0; //setSpeed( 0.0); 
			    } 
		    } // end for i 
    } // end invoke strategy  
 
	protected void move() 
    {
	    boolean isLegalMove = false; 
		double dX, dY, direction, speed, x2, y2; 
        BoardComponent bC; 
		double[] xY; 
		int orientationIndex; 
		int index;
		for ( int t=0; t< numberOfTeams; t++ ) 
		{
		    for ( int i=0; i< sizeOfTeam; i++ ) 
			{ 
			    isLegalMove = false; 
			  	speed =player[t][i].getSpeed(); 
				direction =player[t][i].getOrientationRadians(); 
				orientationIndex =player[t][i].getOrientationIndex(); 
				dX = speed*Math.cos( direction ); 
				dY = speed*Math.sin( direction );  
				bC = player[t][i]; 
				//currentAgentBCList[t].retrieve(i); 
				xY = bC.getCentrePoint(); 
				x2 = xY[0] + dX; 
				y2 = xY[1] + dY; 
				//if ((x2>0)&&(y2>0)&&(x2<MAX_X)&&(y2<MAX_Y)) 
				if ( isInBounds(x2,  y2 ) ) 
			        isLegalMove = true; 
				if ( isLegalMove ) 
				{    
				    xY[0] = x2; 
					xY[1] = y2; 
					player[t][i].setCentrePoint( xY ); 
					player[t][i].setOrientationIndex( orientationIndex ); 
				}
				else 
			    { 
				    index = player[t][i].getOrientationIndex(); 
					player[t][i].setOrientationIndex( (index+6)%12 ); 
					//    System.out.println("C-m-is an illegal move so do nothing"); 
				} 
			} // end for i 
		} // end for t 
	} // end move 
	
		/* 
	* reads the contestants from filename and puts into competitor list. 
	*/
    private void setContestantList( String contestantFilename ) 
	{ 
        try 
		{ 
		    orderedContestantList = new LinkedList<Contestant>(); 
			String idNo = "zyx"; 
			String sNum = "zyx"; 
			String firstN, lastN; 
			//System.out.println("C-sCL- contestantFilename= " + contestantFilename); 
			String filename = contestantFilename;  
			System.out.println("C-sCL-contestantNames= " + filename); 
			Scanner inFile = new Scanner(new FileReader( filename)); 
		    Scanner lS; 
			String line; 
			GenericContestant con; 
			Class c;
			while (inFile.hasNext())  
			{  
			    line = inFile.nextLine(); 
				//System.out.println("C-sCL- contestant= " + line); 
			    lS = new Scanner( line ); 
		        sNum = "S"+lS.next(); 
			    lastN = lS.next(); 
			    firstN = " "; 
				while  (lS.hasNext() ) 
				    firstN += lS.next(); 
			    //System.out.println("C-sCL-contestant line is " + line); 
				try
		        { 
				    idNo = sNum +extensionName;
					// must be able to create a class before creating a contestant 
					c = Class.forName( idNo );  
					con = new GenericContestant( sNum, lastN, firstN, cityList, numberOfRounds );  
				    orderedContestantList.add( con ); 
					//xxx System.out.println("C-sCL con= " + con.toString()); 
					con = null;  
				} 
				catch(Exception e) 
	            { 
				    String str = idNo + " " + lastN + " " + firstN; 
					noAlgorithmFoundList.add( str ); 
				   System.out.println("C-sCL-class for contestant " + str + " not found " + e.toString()); 
			   } 
			} // end while 
			//System.out.println("CsCL-orderedContestantList size= " + orderedContestantList.size() );  
			contestantList = new Contestant[orderedContestantList.size()];
			int id;
			for ( int i=0;i< contestantList.length; i++ ) 
			{ 
			    con = (GenericContestant) orderedContestantList.get(i); 
				id = con.getContestantId(); 
				//System.out.println("C-sCL-id of con= " + id); 
				contestantList[id] = con; 
			} // end for i   
		} 
		catch( FileNotFoundException e ) 
		{ 
			System.out.println( e.toString() ); 
			System.exit(0); 
        }
	} // end set contestant list 

    private int getNumberOfValidContestants( String contestantFilename ) 
	{ 
		int count = 0; 
        try 
		{  
			String idNo = "zyx"; 
			String sNum = "zyx"; 
			String firstN, lastN; 
			//System.out.println("C-sCL- contestantFilename= " + contestantFilename); 
			String filename = contestantFilename;  
			//System.out.println("C-sCL-contestantNames= " + filename); 
			Scanner inFile = new Scanner(new FileReader( filename)); 
		    Scanner lS; 
			String line; 
			Class c;
			while (inFile.hasNext())  
			{  
			    line = inFile.nextLine(); 
				//System.out.println("C-sCL- contestant= " + line); 
			    lS = new Scanner( line ); 
		        sNum = "S"+lS.next(); 
			    lastN = lS.next(); 
			    firstN = " "; 
				while  (lS.hasNext() ) 
				    firstN += lS.next(); 
			    //System.out.println("C-sCL-contestant line is " + line); 
				try
		        { 
				    idNo = sNum +extensionName;
					// must be able to create a class before counting a valid contestant 
					c = Class.forName( idNo );   
				    count++; 
				} 
				catch(Exception e) 
	            { 
				   // continue processing    
			   } 
			} // end while 
		} 
		catch( FileNotFoundException e ) 
		{ 
			System.out.println( "C-gNVC- " + e.toString() ); 
			System.exit(0); 
        }
        return count; 
} // end get number of valid contestants 
    
    public Contestant getContestant( String idNo) 
   	{ 
	    //System.out.println("C-c-Contestants for this are:"); 
	    Contestant contestant = null; 
		String conId;
		for (int i = 0; i< orderedContestantList.size(); i++ ) 
		{      
		    contestant = orderedContestantList.get(i); 
			conId = contestant.getIdNo();
            //System.out.println("C-gC-conId= " + conId + ", idNo= " + idNo); 
			//if ( !idNo.equals("S123456") ) 
			//  System.exit(0); 
			if ( conId.equals( idNo ) ) 
		        return contestant; 
			} 
			return null;  
	} // end get contestant t contestant list 
	
 
	private void printContestantList() 
	{ 
	    System.out.println("C-c-Contestants for this are:"); 
			for (int i = 0; i< contestantList.length; i++ ) 
			    System.out.println("  " + contestantList[i] ); 
			System.out.println(); 
	} // end print contestant list 
 
	
	protected void setAlgorithmList() 
	{ 
		
		sizeOfTeam = 1; 
		// now get the largest team form the various repetitions 
		for ( int k=0; k< agentBCList.length; k++ ) 
		    if ( agentBCList[k].size() > sizeOfTeam ) 
			    sizeOfTeam = agentBCList[k].size(); 
		// sizeOfTeam should now be the largest possible team 
		//System.out.println("C-sAL-max sizeOfTeam = " + sizeOfTeam ); 
		teamList = new Agent[contestantList.length][sizeOfTeam]; 
		String idNo = "zyx"; 
		String sNum = "xyz"; 
		Agent[] team; 
		Class c; 
		for( int cNum=0; cNum< contestantList.length; cNum++ ) 
		{    
		    c = null;
		    try 
		    {
                sNum = contestantList[cNum].getIdNo(); 
				idNo = sNum + extensionName;
			    //System.out.println("C-sAM- idNo= " + idNo );  
				c = Class.forName( idNo ); 
	       		team = null;
			    team = new Agent[sizeOfTeam]; 
				for ( int i=0; i< sizeOfTeam; i++ ) 
				{    
					team[i]  = (Agent) c.newInstance(); 
					//System.out.println("C-sAM-agent= " + team[i].toString() ); 
				}  
				teamList[cNum] = team; 
			}
		    catch (Exception ex) 
		    {
			    String lastName = contestantList[cNum].getLastName(); 
				String firstName = contestantList[cNum].getFirstName(); 
				String str = idNo + " " + lastName + " " + firstName; 
				noAlgorithmFoundList.add( str ); 
			    //System.out.println( ex.toString() ); 
		    }
		} // end for cNum 
		if ( noAlgorithmFoundList.size() < 1 ) 
			System.out.println("C-sAL- All contestants listed in Contestantfile has classes"); 
		else 
		{ 
		    System.out.println("C-sAL- contestants list in Contestant file that do not have classes are:"); 
		    	for ( int i=0; i< noAlgorithmFoundList.size(); i++ ) 
		    		System.out.println("   " + noAlgorithmFoundList.get(i)); 	
		} 
	} // end set algorithm map
 
	protected void setGameLabel( int repetition, int round, int game, int[] contestantId )
    {
	    gameStr = null; 
		gameStr = " " + repetition + ", " + round + ", " + game + " "; 
		roundStringLabel = null;
	    roundStringLabel = new String[9]; 
		roundStringLabel[0] = "" + "Repetition " + (repetition+1);  
		roundStringLabel[1] = "Round " + (round+1);  
		roundStringLabel[2] = "Game " + (game+1); 
		// now initialise the rest of the string with blanks to be set in subclass 	roundStringLabel[3] = " "; 
		roundStringLabel[4] = " "; 
		roundStringLabel[5] = " "; 
		roundStringLabel[6] = " "; 
		roundStringLabel[7] = " "; 
		roundStringLabel[8] = " ";
		extendGameLabel( contestantId );  
	} // end set game label  
	
	/* 
	* reads the city names from filename and puts into cityList 
	*/ 
	private void setCityList( String cityFilename ) 
	{ 
		  try 
		  { 
		      cityList = new LinkedList<String>(); 
			  // kkk modified the filename call 
			  String filename = cityFilename; 
			  //System.out.println("C-sCL-cityFilename= " + filename); 
			  Scanner inFile = new Scanner(new FileReader( filename)); 
		      Scanner lS; 
			  String line; 
			  while (inFile.hasNext())  
			  {  
			      line = inFile.nextLine(); 
				  //System.out.println("C-sCL-line= " + line ); 
				  cityList.add( line );  		
			  } // end while  
			  numberOfRepetitions = cityList.size(); 
		  } 
		  catch( FileNotFoundException e ) 
		  { 
			    System.out.println( e.toString() ); 
			    System.exit(0); 
      }
	} // end set city list 
	
	public String getFirstContestantFilename() 
	{ 
	    return contestantList[0].getIdNo() + extensionName; 			  
	} 
	
	public String getFirstCityName() 
	{ 
	    return cityList.get(0); 
	} 
	 
	public String getFirstContestantResults() 
	{ 
	    Contestant c = contestantList[0]; 
		return ("" + c.getFirstResult()); 
	} 
	 
		
	public LinkedList<String> getNoAlgorithmFoundList() 
	{ 
	    return noAlgorithmFoundList;
	} 
	
	private void printCityList() 
	{ 
	    System.out.println("C-c-Cities for this are:"); 
			for (int i = 0; i< cityList.size(); i++ ) 
			    System.out.println("  " + cityList.get(i) ); 
			System.out.println(); 
	} // end print city list 
	
	protected void printBCList(  BoardComponentList bCList, String filename) 
	{ 
	    String str = filename + ", BoardComponents are:"; 
	    System.out.println( str); 
		for ( int i=0; i< bCList.size(); i++ ) 
		    System.out.println("   " + bCList.retrieve(i) ); 
		System.out.println(); 
	} // end print agent bc list 
	
	/* 
	* @return numberOfRounds 
	*/ 
	public int getNumberOfRounds() 
	{ 
		return cityList.size(); //numberOfRounds; 
	}
	
	/* 
	* @return a ListIterator for the contestantList. 
	*/
	public Iterator<Contestant> getCurrentListItr() 
	{
		return orderedContestantList.iterator(); 
	} 
 

	/* 
	* @return size of the contestantList
	*/ 
	public int getNumberOfContestants() 
	{ 
		return contestantList.length; 
	} 

	/* 
	* @return contestantList 
	*/ 
	public Contestant[] getCurrentRoundList() 
	{ 	
		return contestantList;
	} 

	/* 
	* makes a shallow copy of current contestantList in reverse order. 
	*/ 
	static public LinkedList<Contestant> reversedCopy( LinkedList<Contestant> list ) 
	{ 		LinkedList<Contestant> rList = new LinkedList<Contestant>(); 
		//int inc = 0;
		for ( int i= (list.size()-1); i>=0; i-- ) 
			  rList.add( list.get( i ) ); 
		return rList; 
	} // end reverse copy


	
	
/* 
	* determines whether this tournament is finished or not. 
	* @return true if finished, false otherwise. 
	* maybe deprecate this 
	public boolean isFinished() 
	{ 
	   return ( currentRound >= (getNumberOfRounds()-1) ); 
	} // end is finished 
	*/ 
		
	public void insertInOrder( Contestant c, LinkedList<Contestant> cL ) 
	{ 
		int index = cL.indexOf( c ); 
		if ( index >= 0 ) 
			cL.remove( index ); 
		ListIterator<Contestant> itr = cL.listIterator(); 
		boolean found = false; 
		// now find the correct position where c should be inserted. 
		while (itr.hasNext() && !found ) 
		{ 
		    Contestant currentC = itr.next(); 
		    if ( currentC.compareTo( c ) > 0 ) 
			{
				if ( itr.hasNext() ) 
			        currentC = itr.next(); 
				else 
					found = true;
			}
			else
			{
				found = true;
                // the correct position is now the previous 
				itr.previous();
			}
		}// end while 
		itr.add( c ); 
	} // end insert in order 

} // end Controller class

