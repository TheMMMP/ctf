package src.Model;

/* 
* BoardComponentList, a class to encapsulate a collection of BoardComponent objects. 
* @author Phil Sheridan
* @version 7/3/14
*/ 

import java.io.*;                                         //Line 1
import java.util.*;                                       //Line 2

public class BoardComponentList 
{ 
	protected BoardComponent[] list; 
	protected int size; 
	
	/* 
	* constructs this with a capacity of 10 and size of 0
	*/
	public BoardComponentList() 
	{ 
	    list = new BoardComponent[10]; 
		  size = 0; 
	} // end constructor 
	
	/* 
	* constructs this safely from from specified parameter
	* @param bCL, the BoardComponentList object to be cloned 
	*/ 
	public BoardComponentList( BoardComponentList bCL ) 
	{ 
	    list = new BoardComponent[bCL.size]; 
			size = bCL.size; 
			BoardComponent bC;
			for ( int i=0; i< size; i++ ) 
			{   
			    bC = bCL.get(i);
					//System.out.println("BCL-c- " +  ", i= " + i + ", bC= " + bC.toString()); 
					list[i] = bC; 
			}
					
	} // end constructor 
	
	/* 
	* adds the specified parameter to the list 
	* @param bC, the BoardComponent object to be added 
	* @postcondition: bC has been added to the end of list and size incremented
	*/ 
	public void add( BoardComponent bC ) 
	{ 
	    if ( size == list.length ) 
		  { 
			    BoardComponent[] temp = new BoardComponent[2*list.length]; 
		      for ( int i= 0; i< list.length; i++ ) 
				      temp[i] = list[i]; 
	        list = temp; 
			    temp = null;
		} 
		list[size++] = bC;
	} // end add 
	
	/* 
	* returns a safe copy of the Blob object in the specified postion
	* @param index, and int that identifies the position of the required object
	* @return reference to object at index if it exists, null otherwise
	*/ 
	public BoardComponent get( int index ) 
	{
		if ( index < list.length ) 
		    return new BoardComponent(list[index]); 
		else 
		    return null;
	}

/* 
	* returns an  unsafe copy of the Blob object in the specified postion
	* @param index, and int that identifies the position of the required object
	* @return reference to object at index if it exists, null otherwise
	*/ 
	public BoardComponent retrieve( int index ) 
	{
		if ( index < list.length ) 
		    return list[index]; 
		else 
		    return null;
	}

  /* 
	* replaces the BoardComponent object at the specified position with the specified one
	* @param id, the identification number of the object to be replaced
	* @param bC, the replacement BoardComponent object
  * @postcondition: bC is in position id in list
	*/ 
	public boolean replace( int id, BoardComponent bC ) 
	{ 
	    boolean isReplaced = false; 
			if ( (id>=0) && (id<size)) 
			{ 
			    list[id] = null; 
					list[id] = bC; 
					isReplaced = true; 
			} // end if 
			return isReplaced;
	} // end replace 
	
	
	/* 
	* removes the specified object from the list
	* @param bC, the BoardComponent object to be removed
	*/ 
	public boolean remove( BoardComponent bC ) 
	{ 
	    boolean success = false; 
	    int index = indexOf( bC );
			if ( index >=0 ) 
			{ 
			    list[index] = null; 
					for ( int i= index; i <(size-1); i++ ) 
					    list[i] = list[i+1]; 
					size--; 
					success = true;
			} 
			return success; 
	} // end remove 
 
	
  /* 
	* @return size, the number of  objects in list
	*/ 
	public int size() 
	{ 
		  return size; 
	} 

/* 
* clears this list of all objects and set size to 0
*/ 
	public void clear() 
	{ 
		list = null; 
		list = new BoardComponent[10]; 
		size = 0; 
	} // end clear
	
	/* 
 * searches for the specified object in list
	* @param rHS the BoardComponent object to be searched for 
	* @return index of rHS if it exists, -1 otherwise
	*/ 
	public int indexOf( BoardComponent rHS ) 
	{ 
	    for ( int i=0; i< size; i++ ) 
		  {
			    //System.out.println("BCL-iO-n in contains, " + list[i].toString() + " =? " + rHS.toString() ); 
				
			    if ( list[i].equals( rHS ) ) 
				      return i; 
	    }
		  return -1; 
	} // end index of  
	
	/* * @return String object containing all BoardComponent objects in list
	*/ 
	public String toString() 
	{ 
		String str = "";
		for ( int i=0; i< size; i++ ) 
			  str += list[i].toString() + "\n"; 
		return str; 
	} // end to string 		
} // end BoardComponentList class
