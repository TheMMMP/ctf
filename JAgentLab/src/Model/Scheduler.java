package src.Model; 

/*
* Scheduler.java, a class  to schedule a tournament of Capture The Flag or BloodBank. 
*  @author Phil Sheridan 
* @version 4/2/15 
*/ 

public class Scheduler
{
    // instance variables 
	private int[][][] schedule; 
    private boolean[][] canPlay; 
	private int round; 
	private int[][] gameList; 
	private int numberOfRounds; 
	private int numberOfGames; 
	private int numberOfContestants; 
	private boolean randomise; 
		
	/* 
	* constructs this with specified parameters 
	* @param numberOfTeams an int to specify the number of teams in a game
	**       1 for BloodBank and 2 for Capture The Flag 
	* @param numberOfContestants, an int that specifies the number of contestants in tournament 
	* @param randomise, a boolean  which if true randomises the contestants as blue or red
	*        otherwise smaller contestant identification number is blue and larger is red 
	*/ 
    public Scheduler( int numberOfTeams, int numberOfContestants, boolean randomise ) 
	{ 
	    if ( numberOfTeams == 2 ) 
	    { 
	    	boolean doSearch = true; //false; 
			if ( doSearch ) 
			    schedule = searchScheduler( numberOfContestants, randomise ); 
			else 
			    schedule = insertionScheduler( numberOfContestants, randomise ); 
	    } 
	    else if ( numberOfTeams == 1 ) 
	    {
	        schedule = setSingleTeamGame( numberOfContestants );  
	    }
	    else 
	    {
	        System.out.println("S-c-numberOfTeams= " + numberOfTeams + ", which is illegal"); 
	        	System.exit(0); 	
	    } 
	    System.out.println("Finished scheduling");
		} // end constructor 
		
	 public int[][][] insertionScheduler( int numberOfContestants, boolean randomise ) 
	 { 
	     boolean oddNumberOfContestants = false; 
			 int evenNumber = numberOfContestants; 
			 if ( (evenNumber%2) != 0 ) 
			 {    
			     oddNumberOfContestants = true; 
					 evenNumber++; 
			 }
			 int [][][] schedule = new int[evenNumber-1][evenNumber/2][];
			 numberOfRounds = schedule.length; 
			 numberOfGames = schedule[0].length; 
			 int num = (numberOfRounds*(numberOfRounds+1)/2); 
				int half = (numberOfRounds-numberOfGames); 
				num -= half*(half+1)/2; 
				gameList = new int[num][2]; 
				//evenNumber*(evenNumber-1)/2][2]; 
			 boolean[][]isScheduled = new boolean[numberOfRounds][evenNumber]; 
			 int count = 0; 
			 int startRow = -2; 
			int rowInvert; 
			int[] pair;
			int tab = 2; 
			int game = -1;
			int i =0; 
			 for ( i=0; i< (evenNumber/2); i++ ) 
			 {   
				   startRow += tab; 
			     round = startRow;
					 rowInvert = startRow;
					game++; 
					for ( int j= (i+1); j< evenNumber; j++ ) 
					{  
			        //round++; 
							pair = new int[2]; 
							pair[0] = i; 
							pair[1] = j; 
							//System.out.println("round= " + round + ", pair= " + pair[0] + ", " + pair[1]);
							if ( round< schedule.length ) 
							{    
							    System.out.println("game= " + game + ", round= " + round + ", pair= " + pair[0] + ", " + pair[1]); 
							    schedule[round++][game] = pair; 
							}
							else 
							{
							    schedule[--rowInvert][game] = pair; 
							    System.out.println("game= " + game + ", round= " + rowInvert + ", pair= " + pair[0] + ", " + pair[1]); 
							    //schedule[--rowInvert][game] = pair;
							} // end else 
			    } // end for j
			} // end for i 
			
			 for (; i< (evenNumber-1); i++ ) 
			 {    
			     for ( int j= (i+1); j< evenNumber; j++ ) 
					 {  
					     gameList[count][0] = i; 
							 gameList[count][1] = j; 
							 //System.out.println("Count= " + count + ", " + gameList[count][0] + ", " + gameList[count][1]);
								count++; 
					 } // end for j 
			 } // end for i 
			 // now schedule the games 
			 //int round, game; 
				
				
				boolean found;
				for ( int g=0; g< gameList.length; g++ ) 
			  { 
				    //System.out.println("Try to schedule gmatch " + g); 
			      round = 0; 
					  game = 0; 
					  found = false; 
					  while ( !found && ( game<numberOfGames) ) 
					  { 
					      if ( (schedule[round][game] == null ) &&rule1Ok(  gameList[g], isScheduled[round] ) ) 
							  { 
								    //System.out.println("gL " + gameList[g][0] + ", " + gameList[g][1] + ", round= " + round + ", game= " + game);
							      isScheduled[round][gameList[g][0]] = true; 
									  isScheduled[round][gameList[g][1]] = true; 
									  if ( rule2Ok( isScheduled, round, game ) ) 
									  { 
										    //System.out.println("round= " + round + ", game= " + game); 
									      schedule[round][game] = new int[2]; 
												schedule[round][game][0] = gameList[g][0]; 
											  schedule[round][game][1] = gameList[g][1]; 
											  //System.out.println("round= " + round + ", game= " + game + ", " + schedule[round][game][0] + ", " + schedule[round][game][1]); 
												found = true; 
									  } 
									  else 
									  { 
									      isScheduled[round][gameList[g][0]] = false; 
									      isScheduled[round][gameList[g][1]] = false; 
												//System.out.println("g= " + gameList[g][0] + ", " + gameList[g][1] + ", failed rule2 in round" + round);
									  }
							  } // end if rule 1 ok 
							  if ( !found ) 
							  { 
							      round++; 
									  if ( round >= numberOfRounds ) 
									  { 
									      round = 0; 
											  game++; 
									  } 
							  } 
					  } // end while 
					  if ( !found ) 
					  { 
					      System.out.println("game " + g + " could not be scheduled: " + gameList[g][0] + ", " + gameList[g][1]); 
								System.out.println("exiting"); 
							  System.exit(0); 
					  } 
			  } // end for g 
				return schedule;
		} // end insertion scheduler 
		
		/* 
		* determines whether the contestants have already appeared in round or not 
		* @return true if have not been scheduled, false otherwise 
		*/ 
		private boolean rule1Ok( int[] contestant, boolean[] isScheduled ) 
		{ 
				if ((isScheduled[contestant[0]] ) || (isScheduled[contestant[1]] )) 
				    return false; 
				else 
				    return true; 
		} // end rule 1 
		
		private boolean rule2Ok( boolean[][] isScheduled, int round, int game ) 
		{ 
		    if ( game < (numberOfGames-1) ) 
				{ 
				    for ( int r=0; r< numberOfRounds; r++ ) 
				    { 
				        //System.out.println("round= " + round );
				        if ( (r!=round) && (matches(isScheduled[r], isScheduled[round] ) ) ) 
						        return false;
				    } // end for r  
				} // end if game isnot ehe last game
				return true;
		} // end rule 2 ok 
		
		private boolean matches( boolean[] a, boolean[] b ) 
		{ 
		    for ( int i=0; i< a.length; i++ ) 
				{    
				    //System.out.println("i= " + i + ", a = " + a[i] + ", b= " + b[i] );
						if ( a[i] != b[i] ) 
						    return false; 
				} 
				return true; 
		} // end matches 
		
		
		private int[][][] setSingleTeamGame( int numberOfContestants ) 
		{ 
		    numberOfRounds = 1; 
			int [][][] schedule = new int[numberOfRounds][numberOfContestants][1]; 
			numberOfGames = schedule[0].length; 
			for ( int i=0; i< numberOfContestants; i++ ) 
				schedule[0][i][0] = i; 
			return schedule;  
		} // end set single team game 
		
		public int[][][] searchScheduler( int numberOfContestants, boolean randomise ) 
		{ 
		    System.out.println("S-c-numberOfContestants= " + numberOfContestants );
			//this.numberOfContestants = numberOfContestants;
			//this.randomise = randomise; 
			boolean oddNumberOfContestants = false; 
			int evenNumber = numberOfContestants; 
			if ( (evenNumber%2) != 0 ) 
			{    
			    oddNumberOfContestants = true; 
				evenNumber++; 
			}
			int [][][] schedule = new int[evenNumber-1][evenNumber/2][];
			numberOfRounds = schedule.length; 
			numberOfGames = schedule[0].length; 
			gameList = new int[evenNumber*(evenNumber+1)/2][2]; 
			canPlay = new boolean[evenNumber][evenNumber]; 
			int count = 0; 
			for ( int i=0; i< (evenNumber-1); i++ ) 
			    for ( int j= (i+1); j< evenNumber; j++ ) 
				{    
				    canPlay[i][j] = true; 
					gameList[count][0] = i; 
					gameList[count][1] = j; 
					count++; 
				} 
				int r; 
				int[] pair; 
				 boolean[] scheduled; 
				for ( round=0; round< schedule.length; round++ ) 
				{ 
				    scheduled = null; 
				    scheduled = new boolean[evenNumber]; 
	          int gameId = 0; 
						int game=0; 
						getNextPair( gameId, game, scheduled, schedule ); 
				} // end for round 
				if ( oddNumberOfContestants ) 
				    eliminateLastContestant( schedule); 
						return schedule;
		} // end search scheduler  
	
	  private boolean getNextPair(  int gameId, int game, boolean[] scheduled, int[][][] schedule ) 
		{ 
		    int r, temp;
		    int[] pair = new int[2]; 
				boolean found = false; 
				int c1 = 0; 
				int c2 = 1; 
				if ( gameId < gameList.length ) 
				{ 
				    c1 = gameList[gameId][0]; 
				    c2 = gameList[gameId][1]; 
				}
				while ( !found && (gameId < gameList.length) ) 
				{ 
				    if ( !scheduled[c1] && !scheduled[c2] && canPlay[c1][c2] )
						{ 
						    pair[0] = c1; 
								pair[1] = c2; 
								canPlay[c1][c2] = false; 
								scheduled[c1] = true; 
								scheduled[c2] = true; 
								if ( fullRound( scheduled ) ) 
								    found = true; 
								else if ( getNextPair( gameId+1, game+1, scheduled, schedule ) ) 
								    found = true; 
								else 
								{ 
								    canPlay[c1][c2] = true; 
								    scheduled[c1] = false; 
								    scheduled[c2] = false; 
										gameId++; 
								} 
						} 
						else 
						{ 
						    //System.out.println("c1= " + c1 + ", c2= " + c2);
						    gameId++; 
								if ( gameId < gameList.length ) 
								{ 
								    c1 = gameList[gameId][0]; 
				            c2 = gameList[gameId][1]; 
								}
						} // end else  
				} // end while not found 
				if ( found ) 
				{    
						if ( randomise ) 
						    r = ((int) (Math.random()*10))%2; 
					  else 
					    r = 0; 
					  //System.out.println("r= " + r + ", pair[0]= " + pair[0] + ", pair[1]= " + pair[1]  ); 
						if ( r==1 ) 
						{ 
						    temp =pair[r]; 
						    pair[r] = pair[(r+1)%2]; 
						    pair[(r+1)%2] = temp; 
						} 
						schedule[round][game] = pair; 
						//System.out.println(" new pair "+ ", pair[0]= " + pair[0] + ", pair[1]= " + pair[1]  ); 
				} 
			 return found; 
		}  // end get next pair 
		
	  private boolean fullRound( boolean[] list ) 
		{ 
		    for ( int i=0; i< list.length; i++ ) 
				    if ( !list[i] ) 
						    return false; 
				return true; 
		} 
		
		private void eliminateLastContestant( int[][][] schedule ) 
		{ 
		    int eliminateId = numberOfContestants; 
			  int g, first, second; 
			  int[][][] temp = new int[schedule.length][schedule[0].length-1][]; 
			  for ( int r=0; r< temp.length; r++ ) 
			  { 
			    g = 0; 
					for ( int i=0; i< schedule[0].length; i++ )  
					{ 
					    first = schedule[r][i][0]; 
							second = schedule[r][i][1]; 	
					    if ( (first != eliminateId) && ( second != eliminateId ) ) 
							{ 
							    temp[r][g] = schedule[r][i]; 
									g++; 
							} // end if 
					} // end for i 
			} // end for r  
			numberOfGames--; 
			schedule = null; 
			schedule = temp; 
			temp = null;
		} // end eliminate last contestant 
		
		public int getNumberOfRounds() 
		{ 
		    return numberOfRounds; 
		} 
		
		public int getNumberOfGames() 
		{ 
		    return numberOfGames; 
		} 
		
		
	public int[][][] getSchedule() 
	{ 
	    return schedule; 
	} 
	
	static public void main( String[] arg ) 
	{ 
		int numberOfTeams = 1; 
	    int numberOfContestants = 6;
	    boolean randomise = false;
		Scheduler s = new Scheduler( numberOfTeams, numberOfContestants, randomise ); 
		System.out.println("randomise = " + randomise);
		int[][][] a = s.getSchedule(); 
		for ( int round=0; round < a.length; round++ ) 
		{ 
		    System.out.println("round= " + round ); 
			for ( int game=0; game< a[0].length; game++ ) 
			{    
			    System.out.print("    game= " + game + ", " ); 
				System.out.println(a[round][game][0] + ", " + a[round][game][1] );
			}
		} // end for round 
	} // end main 
} // end Scheduler class

