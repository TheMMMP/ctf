package src.Model;

/* 
* GenericContestant a class to extend the Contestant class for an agent generic contestant. 
* @author Phil Sheridan
* @version  8/2/15
*/ 

import java.util.LinkedList; 
import java.util.Iterator;  
import src.Model.*; 

public class GenericContestant extends Contestant 
{ 
   	
	private int numberOfWins; 
	private int numberOfLosses; 
	private int numberOfDraws; 
	private int totalPoints; 
			
	/* 
	* construct this with specified parameters. 
	* @param className, String holding the agent's class name  of contestant . 
	* @param firstN, String holding the first name of contestant. 
	* @param lastN, String holding the last name of contestant. 
	* @param  nameList, LinkedList<String> containing the names of the cities associated with the repetitions.
	** @param numberOfRounds, an int to represent the number of rounds in the tournament. 
	*/ 
	public GenericContestant( String className, String lastN, String firstN, LinkedList<String> nameList, int numberOfRounds ) 
	{ 
        super( className, lastN, firstN, nameList, numberOfRounds ); 	
		numberOfWins = 0; 
		numberOfLosses = 0; 
		numberOfDraws = 0; 
	} // end constructor 
	
	/* 
	* construct this with personal details of specified parameter
	* @param c, Contestant to be copied. 
	* @postcondition: idNo, firstN and lastN are that of c. 
	*/
	public GenericContestant( Contestant c ) 
	{ 
		super( c ); 
	    //this.numberOfWins = numberOfWins; 
		//this.numberOfLosses = numberOfLosses ; 
		//this.numberOfDraws =numberOfDraws; 
	} // end constructor 
	
	/* 
	 ** implements the compareTo method from Contestant class 
	 ** @param other, Object to be compared with this. 
	 ** @return 1 if this totalPoints > other, -1 if <, 0 otherwise. 
	 *@precondidtion specified parameter is a CTFContestant. 
	 **/  
	public int compareTo( Object other ) 
	{ // must write this for generic 
	    GenericContestant c = (GenericContestant) other; 
	    return 0; 
} // end compare to 
	 
			 


		
	/* 
	* updates a round score with specified ones 
	* @paran round, the round number in question 
	* @paran possible, the possible number of units achievable 
	* @param actual, the actual number of units achieved. 
	* @param time, the amount of time taken to complete the round. 
	*/ 
	public void updateRoundScores( int repetition, int round, int firstS, int secondS, int thirdS ) 
	{ 
	    super.updateRoundScores( repetition, round, firstS, secondS, thirdS );
	    if ( ( repetition >=0) && (repetition<roundScores.length) && (round>=0) && ( round< roundScores[0].length)) 
	    {
	    	totalPoints = 0; 
	        for ( int i= 0; i< roundScores.length; i++ ) 
			    for ( int j=0; j< roundScores[0].length; j++) 
			    {    
			    	numberOfWins += roundScores[i][j][0]; 
			    	numberOfDraws += roundScores[i][j][1]; 
			    	numberOfLosses += roundScores[i][j][2]; 
			      } // end for j 
			totalPoints = (2*numberOfWins)  + numberOfDraws;    
	    } // end if 
	    else 
		    System.out.println("CTFC-uR- repetition= " + repetition + ", round " + round + " is out of range");
	} // end update round scores 
	
	public String toString() 
	{ 
	    String str = super.toString();  
		str += " " + numberOfWins; 
	    str += "    " + numberOfLosses; 
		str += "    " + numberOfDraws; 
		str += "    " + totalPoints; 
		return str;
	} // end to string 
	

    
	
	 public int getNumberOfPoints() 
	{ 
	    return totalPoints;  
	} 

} // end GenericContestant class
