/*
* S1123456TagAgent.java extends TagAgent class 
* similler to S123456TagAgent
* @version 22/2/15
*/ 

import src.Model.*; 
import src.Model.Tag.*; 

public class S123456TagAgent extends TagAgent
{
  public S123456TagAgent() // BoardComponent bC) 
	{ 
	  super(); 
	}

    public S123456TagAgent( BoardComponent bC ) 
    { 
        super( bC ); 
    } 

    /* 
     ** abstract method: only value of instance variable direction can effect outcome. 
     **/ 
    public void agentStrategy() 
	{ 
		int num = (int)(Math.random() *100);
	    if ( visibleAgents.size() > 0 ) 
		{ 
		    BoardComponent bC = visibleAgents.retrieve(0); 
			double angle = this.getAngleTo( bC );  
			if ( iAmIt ) 
			{   // set direction towards  opponent 
			    setDirection( (angle) ); 
			} 
			else // must not be it 
			{ // so set direction away from opponent   
			    setDirection( angle + Math.PI );   
			}   
		} // end if has visible agent 
		// now change direction if this agent is close to a boundry 
		double[] xY = getCentrePoint(); 
		if( xY[1] < 10 ) 
		    turnRight(6); 
		else if( xY[1] > (Controller.MAX_Y-2)) 
		    turnRight(6); 
		else if( xY[0] > (Controller.MAX_X-2)) 
		    turnRight(6); 
		else if( xY[0] < 10) 
		    turnRight(6); 
    }// end agent strategy
	
		private void setDirection( double angle ) 
		{
	        if ( angle > 0 ) 
		    { 
		        while ( angle > BoardComponent.HALF_TURN_INC )
		        {    
			        turnRight();
			        angle-= BoardComponent.TURN_INC; 
			    }
		    }
		    else // must be negative
		    { 
		        while ( angle < (-BoardComponent.HALF_TURN_INC) )
		        { 
			        turnLeft(); 
				    angle += BoardComponent.TURN_INC;
			    }
		    }
	    } // end set direction 
	//}// end agent strategy
	
	private void turnRight( int n ) 
	{ 
		for ( int i=0; i< n; i++ ) 
			turnRight(); 
	} // end turn right n 
	
} // end S111111TagAgent class

