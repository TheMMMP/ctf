  

/*
* S111112CTFAgent.java extends CTFAgent class and 
*  implements the abstract method agentStrategy().
* @author Phil Sheridan
* @version 29/4/14
* THIS IS THE ORIGINAL AGENT SUPPLIED WITH THE ASSIGNMENT
*/ 
import src.Model.*; 
import src.Model.CTF.*; 

public class S111112CTFAgent extends CTFAgent
{
 
	 // instance variables 
		protected static BoardComponentList knownEnemyAgentsList;
		private static BoardComponent lookOutOne;
		private static BoardComponent lookOutTwo;
		private static BoardComponent lookOutThree;
		private static BoardComponent lookOutFour;
		private int count=0;
	    private int checked = 0;
	/* 
	* constructs a default object 
	*/ 
	public S111112CTFAgent() 	
	{ 
	  super(); 
	  lookOutOne = new BoardComponent(Controller.MAX_X/4, Controller.MAX_Y/4, 0);
	  lookOutTwo = new BoardComponent(Controller.MAX_X/4, (Controller.MAX_Y/4)*3, 0);
	  lookOutThree = new BoardComponent((Controller.MAX_X/4)*3, Controller.MAX_Y/4, 0);
	  lookOutFour = new BoardComponent((Controller.MAX_X/4)*3, (Controller.MAX_Y/4)*3, 0);
		//knownDonors = new BoardComponentList(); 
	}

/* 
* constructs this from the specified parameter 
* @param bC the BoardAgent object for the super class. 
*/ 
 public S111112CTFAgent( BoardComponent bC ) 
	{ 
	  super( bC ); 	    
	  lookOutOne = new BoardComponent(Controller.MAX_X/4, Controller.MAX_Y/4, 0);
	  lookOutTwo = new BoardComponent(Controller.MAX_X/4, (Controller.MAX_Y/4)*3, 0);
	  lookOutThree = new BoardComponent((Controller.MAX_X/4)*3, Controller.MAX_Y/4, 0);
	  lookOutFour = new BoardComponent((Controller.MAX_X/4)*3, (Controller.MAX_Y/4)*3, 0);
	} 
	
	/* 
	* implements the abstract method from the super class. 
	* This is a method that specifies this agent's behaviour 
	*  in the Blood Bank Problem.
	*/ 
	public void agentStrategy() 
	{ 
	    setAgentSpeedTo( 0.6);
	    updateKnownEnemyAgentsList();
	    if((getIdNumber()%10) == 3 || (getIdNumber()%10) == 7) 
	    {
    	    if(senseOtherAgents()!=null)
    	    {
    	       setDirectionToClosestEnemyAgent();
    	       setAgentSpeedTo(0.6);
    	    }
    	}
	    //set waypoints according to colour
	    if(getColour().equals("red"))
	    {
	       lookOutOne = lookOutThree;
	       lookOutTwo = lookOutFour;
	    }
	    //Send first Agent to lookout 1 or 3 
	    if ((getIdNumber()%10) == 0 && checked !=1) 
	    {
		   setDirectionTo(lookOutOne);
		   setAgentSpeedTo(1);
		}
		if( (getIdNumber()%10) == 0 && this.distance(lookOutOne)<=1 && getClosestEnemyAgent() != null)
		{
		    setDirectionToClosestEnemyAgent();
		    setAgentSpeedTo(1);
		}
			
		//Send last Agent to lookout 2 or 4
	    if ((getIdNumber()%10) == 5 && checked ==0) 
	    {
		   setDirectionTo(lookOutTwo);
		   setAgentSpeedTo(1);
		}
		if((getIdNumber()%10) == 5  && this.distance(lookOutTwo)<=1 && getClosestEnemyAgent() != null)
		{
		    setDirectionToClosestEnemyAgent();
		    setAgentSpeedTo(1);
		}
	} // end agent strategy 
	
	/* 
	private void setDirectionToClosestKnownCTF() 
	{ 
	    CTF bloodBank = getClosestKnownCTF(); 
			if ( bloodBank != null ) 
			{ 
			    double angle = this.getAngleTo( bloodBank ); 
					setDirection( angle ); 
			}
	} // end set direction to closest blood bank 
	*/ 


	private void setDirectionToClosestEnemyAgent() 
	{
		if ( knownEnemyAgentsList.size() > 0 ) 
		{ // at least one donor is in the field of view of this agent. 
		    BoardComponent  agent = getClosestEnemyAgent(); 
				double angle = getAngleTo( agent );  
				setDirection ( angle ); 
		}
		else 
		{ 
		  // do nothing
		} 
	} // end set direction to closest donor 
 
	
	
    private BoardComponent getClosestEnemyAgent() 
	{ 
	  double minDis = 10000.0; 
	  double dis;
	  BoardComponent closestEnemyAgent = null;
	  BoardComponent bC = null; 
	  for ( int i=0; i< knownEnemyAgentsList.size(); i++ ) 
	  {   
	    bC = knownEnemyAgentsList.retrieve(i);
		dis = this.distance( bC ); 
		if ( dis < minDis ) 
		{ 
		     minDis = dis; 
					closestEnemyAgent = bC; 
		}
	  }
	  return closestEnemyAgent ;	 
	} // end get closest Enemy Agent 
		
		
		
	private void setDirection( double angle ) 
	{
	    if ( angle > 0 ) 
			{ 
			    while ( angle > BoardComponent.HALF_TURN_INC )
					{    
					    turnRight();
							angle-= BoardComponent.TURN_INC; 
					}
			}
			else // must be negative
			{ 
			    while ( angle < (-BoardComponent.HALF_TURN_INC) )
					{ 
					    turnLeft(); 
							angle += BoardComponent.TURN_INC;
					}
			}
	} // end set direction 

	private void setDirectionTo(BoardComponent bC) 
    {  
        if(bC != null)
        {
          double angle = this.getAngleTo(bC); 
          setDirection( angle ); 
        }
    }
	


		
// 		private double distanceToClosestCTF() 
// 		{ 
// 		    double distance = 0.0; //Double.MAX_VALUE; 
// 				//System.out.println("S11-dTCBB-knownCTF size= " + knownCTFs.size() );
// 				if ( knownCTFs.size() > 0 ) 
// 				{ 
// 				    CTF bloodBank = getClosestKnownCTF(); 
// 						distance = this.distance( bloodBank ); 
// 						//System.out.println("s11-dTcBB-dis= " + ((int) distance) + ", BB is " + bloodBank.toString() ); 
// 				} 
// 				return distance; 
// 		} // end distance to closest blood bank 

		
	
	   /* 
		private CTF getClosestKnownCTF() 
		{ 
		  double minDis = 10000.0; 
		  double dis;
			CTF closestCTF = null;
			CTF bC = null; 
			for ( int i=0; i< knownCTFs.size(); i++ ) 
			{ 
			    bC = (CTF) knownCTFs.retrieve(i);
					dis = this.distance( bC ); 
					if ( dis < minDis ) 
					{ 
					     minDis = dis; 
								closestCTF = bC; 
					}
			}
				return closestCTF;
				} // end get closest known blood bank 
				*/ 
	
	private void updateKnownEnemyAgentsList() 
        { 
            knownEnemyAgentsList = new BoardComponentList(senseOtherAgents());
        } // end update known blood banks 
			
    /* 
    static public void main( String[] args ) 
		{ 
		    double x = 20; 
				double y = 10; 
				BoardComponent bC = new BoardComponent( x, y, 10 ); 
				bC.setColour("blue"); 
				S111111CTFAgent a = new S111111CTFAgent( bC ); 
				//a.setColour("blue"); 
				double[] v = a.getMyHomeLocation(); 
				System.out.println("a= " + a.toString() );
				System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] ); 
				
		} // end main 
		*/  
} // end S123456CTFAgent class

