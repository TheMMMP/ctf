package Runnables;

/*
* RunnableCTF.java, a class to run the capture The Flag tournament 
* @author Phil Sheridan 
* @version 23/2/15 
*/ 

import src.View.DrawableCTFController; 
import src.View.ControllerCanvas; 

public class RunnableCTF
{
    static public void main( String[] args ) 
    { 
	    boolean randomise = false; // keep false when testing, otherwise randomises red and blue contestants
	    int gameDuration = 499; // in simulated seconds 
	    String[] filename = new String[2]; 
	    filename[0]= "CTFContestantNames.txt"; 
	    filename[1] = "CTFCityNames.txt"; 
		DrawableCTFController dC = new DrawableCTFController( gameDuration, filename, randomise );  
	    ControllerCanvas.createController( dC ); 
		dC.runDrawableController(); 
	} // end main 
} // end RunnableCTF class

