package Runnables;

/*
* RunnableBloodBank.java, a class to run a tournament of BloodBank 
* @author Phil Sheridan 
* @version 23/2/15 
*/ 

import src.View.DrawableBloodBankController; 
import src.View.ControllerCanvas; 

public class RunnableBloodBank
{
	 static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // keep false when testing, otherwise randomises red and blue contestants
	    int gameDuration = 400; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "BloodBankContestantNames.txt"; 
	    filename[1] = "BloodBankCityNames.txt"; 
		DrawableBloodBankController dC = new DrawableBloodBankController( gameDuration, filename, randomise );  
	    ControllerCanvas.createController( dC ); 
	    dC.runDrawableController(); 
	} // end main  
} // end RunnableBloodBank class

