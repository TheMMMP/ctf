package Runnables;

/*
* RunnableTag.java, a class to run a game of Tag 
* @author Phil Sheridan 
* @version 23/2/15 
*/ 

import src.View.DrawableTagController; 
import src.View.ControllerCanvas; 

public class RunnableTag
{
    static public void main( String[] args ) 
	{ 
	    boolean randomise = false; // keep false when testing, otherwise randomises red and blue contestants
	    int gameDuration = 400; // in simulated seconds 
		String[] filename = new String[2]; 
	    filename[0]= "TagContestantNames.txt"; 
	    filename[1] = "TagCityNames.txt"; 
		DrawableTagController dC = new DrawableTagController( gameDuration, filename, randomise );  
	    ControllerCanvas.createController( dC ); 
	    dC.runDrawableController(); 
	} // end main 


} // end RunnableTag class

