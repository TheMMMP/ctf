/*
* SBenchMarkBloodBankAgent.java extends BloodBankAgent class and 
*  implements the abstract method agentStrategy().
*  the class given to students and the minimal result that must be beaten
* @author Phil Sheridan
* @version 10/3/15
*/ 

import src.Model.*; 
import src.Model.BloodBank.*; 
 
public class SBenchMarkSingleBloodBankAgent extends BloodBankAgent
{
 
	 // instance variables 
	private BoardComponentList knownBloodBanks;
	private BoardComponentList knownDonors; 
	private int previousTime; 
	
	/* 
	* constructs a default object 
	*/ 
	public SBenchMarkSingleBloodBankAgent() 	
	{ 
	     super(); 
	     knownBloodBanks = new BoardComponentList();
		 knownDonors = new BoardComponentList(); 
	} // end constructor     previoustime = 0; 

    /* 
    * constructs this from the specified parameter 
    * @param bC the BoardAgent object for the super class. 
    */ 
    public SBenchMarkSingleBloodBankAgent( BoardComponent bC ) 
	{ 
	    super( bC ); 
	} 
	
	private boolean isNewGame() 
	{ 
	    return ( myTimeRemaining > previousTime );	
	}
	
	/* 
	* implements the abstract method from the super class. 
	* This is a method that specifies this agent's behaviour 
	*  in the Blood Bank Problem.
	*/ 
	public void agentStrategy() 
	{ 
	    //if ( BloodBankController.isNewGame() ) 
	    //{ 
	    //	System.out.println("SBBA-aS-yes, is a new game"); 
	    //}
	    if ( isNewGame() )
	    { 
	    		// reset all private variable of this 
	    		//System.out.println("SBM-aS-prev know donors size is " + knownDonors.size() + ", known prev bb is " + knownBloodBanks.size() ); 
	    		knownDonors.clear(); 
	    		knownBloodBanks.clear(); 
	    		//System.out.println("SBM-aS-current know donors size is " + knownDonors.size() + ", current known bb is " + knownBloodBanks.size() ); 
	    	 
	    }	    		 
	    previousTime = myTimeRemaining; 
	   	
	    setAgentSpeedTo( 1.0); 
		//int num = (int)(Math.random() *100); 
		updateKnownBloodBanks();
		updateKnownDonors(); 
		if ((myTimeRemaining < distanceToClosestBloodBank()) && (myBloodLoad > 0) ) 
			{ // not much time left, so make a run for the closest blood bank
			    
					setDirectionToClosestKnownBloodBank();
					//System.out.println( "S11-aS-" +  toString() ); 
			}
			else if (( myBloodLoad < MY_CAPACITY ) && (knownDonors.size() > 0 )) 
			{	
			    setDirectionToClosestDonor(); 
			} 
			else if (( myBloodLoad  == MY_CAPACITY ) ) 
			//&& (knownDonors.size() > 0 )) 
			{	
			    setDirectionToClosestKnownBloodBank(); 
			}
			else 
			{ 
			    // make no changes 
					// just keep moving and 
					// hope that  a donor is found by luck
			} 
	} // end agent strategy 
	
	private void setDirectionToClosestKnownBloodBank() 
	{ 
	    BloodBank bloodBank = getClosestKnownBloodBank(); 
			if ( bloodBank != null ) 
			{ 
			    double angle = this.getAngleTo( bloodBank ); 
					setDirection( angle ); 
			}
	} // end set direction to closest blood bank 
	
	private void setDirectionToClosestDonor() 
	{
	    //System.out.println("S11-aS- visDo size= " + visibleDonors.size() + ", thi= " + this.toString() ); 
			if ( knownDonors.size() > 0 ) 
			{ // at least one donor is in the field of view of this agent. 
			    Donor donor = getClosestDonor(); 
					double angle = getAngleTo( donor );  
					setDirection ( angle ); 
			}
			else 
			{ 
			  // do nothing
			} 
	} // end set direction to closest donor 
	
	private void setDirection( double angle ) 
	{
	    if ( angle > 0 ) 
			{ 
			    while ( angle > BoardComponent.HALF_TURN_INC )
					{    
					    turnRight();
							angle-= BoardComponent.TURN_INC; 
					}
			}
			else // must be negative
			{ 
			    while ( angle < (-BoardComponent.HALF_TURN_INC) )
					{ 
					    turnLeft(); 
							angle += BoardComponent.TURN_INC;
					}
			}
	} // end set direction 

    private Donor getClosestDonor() 
		{ 
		  double minDis = 10000.0; 
		  double dis;
			Donor closestDonor = null;
			Donor bC = null; 
			for ( int i=0; i< knownDonors.size(); i++ ) 
			{ 
			    bC = (Donor) knownDonors.retrieve(i);
					dis = this.distance( bC ); 
					if ( dis < minDis ) 
					{ 
					     minDis = dis; 
								closestDonor = bC; 
					}
			}
				return closestDonor;	 
		} // end get closest donor 
		
		private void updateKnownBloodBanks() 
		{ 
		    //System.out.println("S1-uKB-time= " + myTimeRemaining + ", vBB.size= " + visibleBloodBanks.size() ); 
		    BloodBank bloodBank, oldBloodBank; 
				int index; 
				//System.out.println("S11-uKBB-visibleBloodBanks size= " + visibleBloodBanks.size() ); 
				for ( int i=0; i< visibleBloodBanks.size(); i++ ) 
				{ 
				    bloodBank = (BloodBank) visibleBloodBanks.retrieve(i); 
						//System.out.println("   visible BB= " + bloodBank.toString() ); 
						index = knownBloodBanks.indexOf( bloodBank ); 
						if ( index> -1 ) 
						{ 
						    oldBloodBank = (BloodBank) knownBloodBanks.retrieve(index); 
								//System.out.println("  S11-uKBB-got a visibleBlood bank= " + oldBloodBank.toString() );
								if ( oldBloodBank.getBloodLoad() != bloodBank.getBloodLoad() )   
								    knownBloodBanks.replace( index, bloodBank ); 
									
						}
						else 
						{    
						    knownBloodBanks.add( bloodBank ); 
								//System.out.println("S1-aS-kBB size= " + knownBloodBanks.size() + ", added is " + bloodBank.toString() ); 
						}
				} // end for i 
				// now print the know blood banks 
				//if ( knownBloodBanks.size() > 1 ) 
				//{
				  //  for ( int i=0; i< knownBloodBanks.size(); i++ ) 
				    //    System.out.println("time= " + myTimeRemaining + ", " + knownBloodBanks.retrieve(i).toString() ); 
			      //System.exit(0); 
				//}
		} // end update known blood banks 
		
		// aa 
		private void updateKnownDonors() 
		{ 
		    //System.out.println("S1-uKB-time= " + myTimeRemaining + ", vBB.size= " + visibleBloodBanks.size() ); 
		    //BloodBank bloodBank, oldBloodBank; 
			Donor donor, oldDonor; 
			int index; 
			//System.out.println("S11-uKBB-visibleBloodBanks size= " + visibleBloodBanks.size() ); 
			for ( int i=0; i< visibleDonors.size(); i++ ) 
			{ 
			    donor = (Donor) visibleDonors.retrieve(i); 
				//System.out.println("   visible BB= " + bloodBank.toString() ); 
				index = knownDonors.indexOf( donor ); 
				if ( index> -1 ) 
				{ 
				    oldDonor = (Donor) knownDonors.retrieve(index); 
					//System.out.println("  S11-uKBB-got a visibleBlood bank= " + oldBloodBank.toString() );
					if ( oldDonor.getBloodLoad() != donor.getBloodLoad() )   
					{ 
					    if ( donor.getBloodLoad() > 1 ) 
						    knownDonors.replace( index, donor ); 
						else 
						{ 
						    //System.out.println("S1-uKD-got the remove method");
							//System.out.println("S1-uKD-befor call size= " + knownDonors.size() ); 
							boolean removed = knownDonors.remove( oldDonor );
							//System.out.println("S1-uKD-after call size= " + knownDonors.size()+ ", removed= " + removed );
						}
					}		
				}
				else 
				{    
				    knownDonors.add( donor ); 
					//System.out.println("S1-aS-kBB size= " + knownBloodBanks.size() + ", added is " + bloodBank.toString() ); 
				}
			} // end for i 
			// now print the know blood bank 
			//System.out.println("S1-uKD-time = " + myTimeRemaining + ", known donors to me ar:"); 
			//for ( int i=0; i< knownDonors.size(); i++ ) 
			//  System.out.println( knownDonors.retrieve(i).toString() ); 
			//System.out.println();
		} // end update know donors  
		
		private double distanceToClosestBloodBank() 
		{ 
		    double distance = 0.0; //Double.MAX_VALUE; 
				//System.out.println("S11-dTCBB-knownBloodBank size= " + knownBloodBanks.size() );
				if ( knownBloodBanks.size() > 0 ) 
				{ 
				    BloodBank bloodBank = getClosestKnownBloodBank(); 
						distance = this.distance( bloodBank ); 
						//System.out.println("s11-dTcBB-dis= " + ((int) distance) + ", BB is " + bloodBank.toString() ); 
				} 
				return distance; 
		} // end distance to closest blood bank 
		
	
		private BloodBank getClosestKnownBloodBank() 
		{ 
		  double minDis = 10000.0; 
		  double dis;
			BloodBank closestBloodBank = null;
			BloodBank bC = null; 
			for ( int i=0; i< knownBloodBanks.size(); i++ ) 
			{ 
			    bC = (BloodBank) knownBloodBanks.retrieve(i);
					dis = this.distance( bC ); 
					if ( dis < minDis ) 
					{ 
					     minDis = dis; 
								closestBloodBank = bC; 
					}
			}
				return closestBloodBank;
				} // end get closest known blood bank 
				
 
} // end S123456BloodBankAgent class

