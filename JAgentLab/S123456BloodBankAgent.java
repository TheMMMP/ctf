/*
* S123456BloodBankAgent.java extends BloodBankAgent class and 
*  implements the abstract method agentStrategy().
* @author Phil Sheridan
* @version 5/2/15
*/ 

import src.Model.*; 
import src.Model.BloodBank.*; 

public class S123456BloodBankAgent extends BloodBankAgent
{
 
	 // instance variables 
	private BoardComponentList knownBloodBanks; 
	
	/* 
	* constructs a default object 
	*/ 
	public S123456BloodBankAgent() 	
	{ 
	  super(); 
	  knownBloodBanks = new BoardComponentList(); 
	}

    /* 
    * constructs this from the specified parameter 
    * @param bC the BoardAgent object for the super class. 
    */ 
    public S123456BloodBankAgent( BoardComponent bC ) 
	{ 
	    super( bC ); 
	} 
	
	/* 
	* implements the abstract method from the super class. 
	* This is a method that specifies this agent's behaviour 
	*  in the Blood Bank Problem.
	*/ 
	public void agentStrategy() 
	{ 
	    setAgentSpeedTo( 1.0); 
		//int num = (int)(Math.random() *100); 
		updateKnownBloodBanks();
		if ((myTimeRemaining < distanceToClosestBloodBank()) && (myBloodLoad > 0) ) 
		{ // not much time left, so make a run for the closest blood bank
	        setDirectionToClosestKnownBloodBank();
		    //System.out.println( "S11-aS-" +  toString() ); 
		}
		else if (( myBloodLoad < MY_CAPACITY ) && (visibleDonors.size() > 0 )) 
		{	
		    setDirectionToClosestDonor(); 
		} 
		else 
		{ 
			//System.out.println("S123456-s- made no changes to direction"); 
		    //System.out.println("  num VisibleDonors= " + visibleDonors.size() ); 
		    // make no changes 
		    // just keep moving and 
			// hope that  a donor is found by luck
		} 
	} // end agent strategy 
	
	private void setDirectionToClosestKnownBloodBank() 
	{ 
	    BloodBank bloodBank = getClosestKnownBloodBank(); 
		if ( bloodBank != null ) 
		{ 
		    double angle = this.getAngleTo( bloodBank ); 
			setDirection( angle ); 
		}
	} // end set direction to closest blood bank 
	
	private void setDirectionToClosestDonor() 
	{
	    //System.out.println("S11-aS- visDo size= " + visibleDonors.size() + ", thi= " + this.toString() ); 
		if ( visibleDonors.size() > 0 ) 
		{ // at least one donor is in the field of view of this agent. 
		    Donor donor = getClosestDonor(); 
		    double angle = getAngleTo( donor );  
			setDirection ( angle ); 
		}
		else 
		{ 
	        // do nothing
		} 
	} // end set direction to closest donor 
	
	private void setDirection( double angle ) 
	{
	    if ( angle > 0 ) 
		{ 
		    while ( angle > BoardComponent.HALF_TURN_INC )
		    {    
			    turnRight();
			    angle-= BoardComponent.TURN_INC; 
			}
		}
		else // must be negative
		{ 
		    while ( angle < (-BoardComponent.HALF_TURN_INC) )
		    { 
			    turnLeft(); 
				angle += BoardComponent.TURN_INC;
			}
		}
	} // end set direction 

    private Donor getClosestDonor() 
	{ 
        double minDis = 10000.0; 
	    double dis;
		Donor closestDonor = null;
		Donor bC = null; 
		for ( int i=0; i< visibleDonors.size(); i++ ) 
		{ 
		    bC = (Donor) visibleDonors.retrieve(i);
			dis = this.distance( bC ); 
			if ( dis < minDis ) 
	        { 
	            minDis = dis; 
			    closestDonor = bC; 
			}
		}
		return closestDonor;	 
	} // end get closest donor 
		
	private void updateKnownBloodBanks() 
	{ 
	    BloodBank bloodBank, oldBloodBank; 
		int index; 
		//System.out.println("S11-uKBB-visibleBloodBanks size= " + visibleBloodBanks.size() ); 
		for ( int i=0; i< visibleBloodBanks.size(); i++ ) 
		{ 
		    bloodBank = (BloodBank) visibleBloodBanks.retrieve(i); 
			index = knownBloodBanks.indexOf( bloodBank ); 
			if ( index> -1 ) 
			{ 
			    oldBloodBank = (BloodBank) knownBloodBanks.retrieve(index); 
				//System.out.println("  S11-uKBB-got a visibleBlood bank= " + oldBloodBank.toString() );
			    if ( oldBloodBank.getBloodLoad() != bloodBank.getBloodLoad() )   
				    knownBloodBanks.replace( index, bloodBank ); 
			}
			else 
			    knownBloodBanks.add( bloodBank ); 
		} // end for i 
	} // end update known blood banks 
		
	private double distanceToClosestBloodBank() 
	{ 
	    double distance = 0.0; //Double.MAX_VALUE; 
		//System.out.println("S11-dTCBB-knownBloodBank size= " + knownBloodBanks.size() );
		if ( knownBloodBanks.size() > 0 ) 
		{ 
		    BloodBank bloodBank = getClosestKnownBloodBank(); 
			distance = this.distance( bloodBank ); 
			//System.out.println("s11-dTcBB-dis= " + ((int) distance) + ", BB is " + bloodBank.toString() ); 
		} 
		return distance; 
	} // end distance to closest blood bank 
		
	private BloodBank getClosestKnownBloodBank() 
	{ 
	    double minDis = 10000.0; 
		double dis;
		BloodBank closestBloodBank = null;
		BloodBank bC = null; 
		for ( int i=0; i< knownBloodBanks.size(); i++ ) 
		{ 
		    bC = (BloodBank) knownBloodBanks.retrieve(i);
			dis = this.distance( bC ); 
			if ( dis < minDis ) 
			{ 
			    minDis = dis; 
				closestBloodBank = bC; 
			}
		}
		return closestBloodBank;
	} // end get closest known blood bank 
				
 
} // end S123456BloodBankAgent class

