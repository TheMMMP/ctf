/*
* SMightyMutantMooseCTFAgent.java extends CTFAgent class and
* implements the abstract method agentStrategy().
* @author The Mighty Mutant Moose People
* @version 07/06/15
*/
import src.Model.BoardComponent;
import src.Model.BoardComponentList;
import src.Model.CTF.CTFAgent;
import src.Model.CTF.CTFController;
import src.Model.Controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class SMightyMutantMooseCTFAgent extends CTFAgent
{

    // static instance variables
    static private HashMap<Integer, SMightyMutantMooseCTFAgent> myTeam;
    static private BoardComponentList knownCTFs;
    static public BoardComponent knownEnemyFlag;
    static public BoardComponent knownEnemyJail;
    static private HashSet<Integer> inJailSet;

    //instance variables
    private BoardComponentList attackWayPoints;
    private int closestToJailId;
    private boolean sideRed = !((getIdNumber()/10)%2 == 0);
    private int firstDefender;
    private int numOfDefenders;
    private int firstAttacker;
    private int numOfAttackers;
    private int firstFlagDef;
    private int numOfFlagDef;
    private int gapBetween;
    private int frontLine = ((Controller.MAX_X/2) + (50 * (sideRed ? 1 : -1)));
    private int midLine = (Controller.MAX_X/2 + (sideRed ? 3 : -1));
    private int safeZone = 15;
    private String agentRole = "none";
    private int totalTime = 999999999;
    private boolean inJail = false;

    /*
    * constructs a default object
    */
    public SMightyMutantMooseCTFAgent()
    {
        super();
        knownCTFs = new BoardComponentList();
        inJailSet = new HashSet<Integer>();
        knownEnemyFlag = null;
        knownEnemyJail = null;
        totalTime = CTFController.getTimeRemaining();
        myTeam = new HashMap<Integer, SMightyMutantMooseCTFAgent>();
        setAttackWayPoints();
    }

/*
* constructs this from the specified parameter
* @param bC the BoardAgent object for the super class.
*/
 public SMightyMutantMooseCTFAgent(BoardComponent bC)
 {
     super(bC);
 }

    private void setAttackWayPoints()
    {
        attackWayPoints = new BoardComponentList();
        attackWayPoints.add(new BoardComponent(((Controller.MAX_X))/2,Controller.MAX_Y,0));
        attackWayPoints.add(new BoardComponent(((Controller.MAX_X))/2,0,0));
        attackWayPoints.add(new BoardComponent(((Controller.MAX_X))*(sideRed ? 0 : 1),Controller.MAX_Y,0));
        attackWayPoints.add(new BoardComponent(((Controller.MAX_X))*(sideRed ? 0 : 1),0,0));
        attackWayPoints.add(new BoardComponent(((Controller.MAX_X))*(sideRed ? 0 : 1),Controller.MAX_Y/2,0));
    }
    /*
    * implements the abstract method from the super class.
    * This is a method that specifies this agent's behaviour.
    */
    public void agentStrategy()
    {
        myTeam.remove(this);
        myTeam.put(getIdNumber(), this);
        if(Controller.isNewGame())
        {
            totalTime = CTFController.getTimeRemaining();
            BoardComponentList l = new BoardComponentList();
            l.add(null);
            setOtherVisibleFlag(l);
            setOtherVisibleJail(l);
        }

        if(Controller.isNewGame() || CTFController.getTimeRemaining() > totalTime - 5)
        {
            knownCTFs = new BoardComponentList();
            knownEnemyFlag = null;
            knownEnemyJail = null;
            inJailSet = new HashSet<Integer>();
            setAttackWayPoints();
            System.out.println(getIdNumber() + " New Game!");
            if(iAmHoldingFlag())
            {
                setIAmHoldingFlag(false);
            }
        }
        else
        {
            updateKnownCTFs();
            updateKnownEnemyFlag();
            updateKnownEnemyJail();
            if (knownEnemyJail != null)
            {
                updateClosestJailAgent();
            }
        }
        setAgentSpeedTo(1.0);

        //Check Jail Behaviour
        if(isInJail() && !inJail)
        {
            //PANIC! Newly in Jail
            inJailSet.add(myIdNumber);
            inJail = true;
        }
        else if(!isInJail() && inJail)
        {
            //Just released
            setAttackWayPoints();
            inJailSet.remove(myIdNumber);
        }


        boolean onside = ((xY[0] >= midLine && colour == "red") || (xY[0] <= midLine && colour == "blue"));

        //Gets the Agents base personality
        determineAgentRole();

        //Determine the agent action.
        if(CTFController.getTimeRemaining() <= 300)
        {
            //changeAgents(0,8,2); //set the number of agents in each role in the format (int defend, int attack, int flagDef);
        }
        else
        {
            changeAgents(5,4,1); //set the number of agents in each role in the format (int defend, int attack, int flagDef);
        }

        if(onside && getClosestEnemy() != null)//if there is an enemy close by on the friendly side, chase that enemy
        {
            catchEnemy();
        }
        else if (!onside && (getClosestEnemy() != null) && distance(getClosestEnemy()) <= 15)
        {
            retreat();
        }
        else if(getIdNumber() == closestToJailId && inJailSet.size() > 0 && knownEnemyJail != null && !agentRole.equals("flagDef") && !iAmHoldingFlag())
        {
            setDirection(getAngleTo(knownEnemyJail));
        }
        else if(agentRole == "defend") //assigns an agent to the defense role
        {
            defend();
        }
        else if(agentRole == "attack") //assigns an agent to the attack role
            attack();
        else if(agentRole == "flagDef") //assigns an agent to the other role
            flagDef();

    } // end agent strategy

    private void changeAgents(int defend, int attack, int flagDef)
    {
        numOfDefenders = defend;
        firstAttacker = firstDefender + numOfDefenders;
        numOfAttackers = attack;
        firstFlagDef = firstDefender + numOfDefenders + numOfAttackers;
        numOfFlagDef = flagDef;
        gapBetween = (Controller.MAX_Y)/(numOfDefenders+1);
    }

    private void determineAgentRole()
    {
        if(getIdNumber()%10 >= firstDefender && getIdNumber()%10 <= (numOfDefenders+(firstDefender-1)) ) //assigns an agent to the defense role
            agentRole = "defend";
        else if(getIdNumber()%10 >= firstAttacker && getIdNumber()%10 <= (numOfAttackers+(firstAttacker-1))) //assigns an agent to the attack role
            agentRole = "attack";
        else if(getIdNumber()%10 >= firstFlagDef && getIdNumber()%10 <= (numOfFlagDef+(firstFlagDef-1))) //assigns an agent to the other role
            agentRole = "flagDef";
    }

    private BoardComponent defensePoint()
    {
        int x = frontLine;
        int y = gapBetween + ((getIdNumber()%10 - (firstDefender))*(gapBetween));
        return new BoardComponent(x,y,0);
    }

    private void defend()
    {
        setDirection(getAngleTo(defensePoint()));
    }

    private void attack()
    {
        if(distance(getClosestWayPoint()) < 5)
        {
            attackWayPoints.remove(getClosestWayPoint());
        }

        if(this.iAmHoldingFlag())
        {
            setDirection(getMyHomeLocation()[0]);
        }
        else if(knownEnemyFlag != null)
        {
            setDirection(getAngleTo(knownEnemyFlag));
        }
        else if (attackWayPoints.size() > 0)
        {
            setDirection(getAngleTo(getClosestWayPoint()));
        }
        else
        {
            setDirection(getAngleTo(new BoardComponent(((Controller.MAX_X))*(sideRed ? 0 : 1),Controller.MAX_Y/2,0)));
        }
    }

    private BoardComponent flagDefPoint()
    {
        int x = (int)(senseTeamFlag().getCentrePoint()[0] + (15 * (sideRed ? -1 : 1)));
        double gap = 40/numOfFlagDef;
        double topMost = ((senseTeamFlag().getCentrePoint()[1]) - ((numOfFlagDef*gap))/2);
        double y =(topMost) + (gap*((getIdNumber()%10)-firstFlagDef)) + (gap/2);
        return new BoardComponent(x,y,0);
    }

    private void flagDef()
    {
        //POST AGENTS BETWEEN FRIENDLY FLAG AND ENEMY SIDE
        setDirection(getAngleTo(flagDefPoint()));
    }

    private void retreat()
    {
        setDirection(-1*getAngleTo(getClosestEnemy()));
    }

    private void catchEnemy()
    {

        if((!(agentRole == "defend")) && ((sideRed && getClosestEnemy().getCentrePoint()[0] >= midLine) || (!sideRed && getClosestEnemy().getCentrePoint()[0] <= midLine))) //Checks if the enemy is on friendly side(so as not to be caught)
        {
            setDirection(getAngleTo(getClosestEnemy()));//chase enemy if on friendly side
        }
        else if ((agentRole == "defend") && ((getClosestEnemy().getCentrePoint()[1] >= defensePoint().getCentrePoint()[1]+gapBetween) || (getClosestEnemy().getCentrePoint()[1] <= defensePoint().getCentrePoint()[1]-gapBetween)))
        {
            defend();
        }
        else if ((agentRole == "defend") && ((getClosestEnemy().getCentrePoint()[0] <= midLine && colour == "red") || (getClosestEnemy().getCentrePoint()[0] >= midLine && colour == "blue"))) //enemy not on friendly side yet
        {
            defend();
        }
        else
        {
            if ((((senseTeamFlag() != null && distance(senseTeamFlag()) > safeZone) && (senseTeamJail() != null && distance(senseTeamJail()) > safeZone)) || (senseTeamFlag() == null || senseTeamJail() == null)) || distance(getClosestEnemy()) > 1) // ensures agent does not follow enemy into friendly flag zone
            {
                if(agentRole == "defend"){
                    setDirection(getAngleTo(new BoardComponent(frontLine,getClosestEnemy().getCentrePoint()[1],0)));
                }
                else {
                    setDirection(getAngleTo(new BoardComponent(midLine,getClosestEnemy().getCentrePoint()[1],0)));
                }
                //System.out.println("safe, chase");
            }
        }
        if(((senseTeamFlag() != null && distance(senseTeamFlag()) <= safeZone) || (senseTeamJail() != null && distance(senseTeamJail()) <= safeZone)) && distance(getClosestEnemy()) <= 1)
        {
            retreat();
        }
    }

    private void setDirection(double angle) {
        if (angle > 0) {
            while (angle > BoardComponent.HALF_TURN_INC) {
                turnRight();
                angle -= BoardComponent.TURN_INC;
            }
        } else // must be negative
        {
            while (angle < (-BoardComponent.HALF_TURN_INC)) {
                turnLeft();
                angle += BoardComponent.TURN_INC;
            }
        }
    } // end set direction

    private void updateKnownCTFs()
    {
        knownCTFs = senseOtherAgents();
    }

    private void updateKnownEnemyFlag()
    {
        if (senseOtherFlag() != null && knownEnemyFlag == null)
        {
            //FIRST FLAG FIND
            knownEnemyFlag = senseOtherFlag();
        }
        else if (senseOtherFlag() != null && knownEnemyFlag != null)
        {
            //UPDATE FLAG
            knownEnemyFlag = senseOtherFlag();
        }
    }

    private void updateKnownEnemyJail()
    {
        if (senseOtherJail() != null) knownEnemyJail = senseOtherJail();
    }

    //Find the closest Enemy Agent
    private BoardComponent getClosestEnemy() {
        double minDis = 10000.0;
        double dis;
        BoardComponent closestEnemyAgent = null;
        BoardComponent bC = null;
        for (int i = 0; i < knownCTFs.size(); i++) {
            bC = knownCTFs.retrieve(i);
            dis = this.distance(bC);
            if (dis < minDis) {
                minDis = dis;
                closestEnemyAgent = bC;
            }
        }
        return closestEnemyAgent;
    } // end get closest Enemy Agent

    //Get the closest Attacking Waypoint for the attacking agents.
    private BoardComponent getClosestWayPoint() {
        double minDis = 10000.0;
        double dis;
        BoardComponent wayPoint = null;
        BoardComponent bC = null;
        for (int i = 0; i < attackWayPoints.size(); i++) {
            bC = attackWayPoints.retrieve(i);
            dis = this.distance(bC);
            if (dis < minDis) {
                minDis = dis;
                wayPoint = bC;
            }
        }
        return wayPoint;
    } // end get closest Enemy Agent

    //Update the agent closest to the jail
    private void updateClosestJailAgent()
    {
        double minDis = 10000.0;
        double dis;
        CTFAgent agent = null;
        CTFAgent a = null;
        Collection<SMightyMutantMooseCTFAgent> agents = myTeam.values();
        for (int i = 0; i < agents.size(); i++) {
            a = (CTFAgent)agents.toArray()[i];
            dis = a.distance(knownEnemyJail);
            if (dis < minDis) {
                minDis = dis;
                agent = a;
            }
        }
        if(agent != null) closestToJailId = agent.getIdNumber();
    }

     static public void main( String[] args )
        { 
            double x = 20; 
            double y = 10;
            BoardComponent bC = new BoardComponent( x, y, 10 );
            bC.setColour("blue");
            SMightyMutantMooseCTFAgent a = new SMightyMutantMooseCTFAgent( bC );
            //a.setColour("blue");
            double[] v = a.getMyHomeLocation();
            System.out.println("a= " + a.toString() );
            System.out.println("angle= " + ((int) (v[0]*180/Math.PI)) + ", distance= " + v[1] );
                
        } // end main 
} // end SMightyMutantMooseCTFAgent class

